"""A setuptools based setup module.

See:
https://packaging.python.org/guides/distributing-packages-using-setuptools/
https://github.com/pypa/sampleproject
"""

# Always prefer setuptools over distutils
from setuptools import setup, find_packages
from os import path

here = path.abspath(path.dirname(__file__))

# Get the long description from the README file
with open(path.join(here, 'README.md'), encoding='utf-8') as f:
    long_description = f.read()

# Arguments marked as "Required" below must be included for upload to PyPI.
# Fields marked as "Optional" may be commented out.

setup(
    # This is the name of your project. The first time you publish this
    # package, this name will be registered for you. It will determine how
    # users can install this project, e.g.:
    #
    # $ pip install sampleproject
    #
    # And where it will live on PyPI: https://pypi.org/project/sampleproject/
    #
    # There are some restrictions on what makes a valid project name
    # specification here:
    # https://packaging.python.org/specifications/core-metadata/#name
    name='SecMOD',  # Required

    # Versions should comply with PEP 440:
    # https://www.python.org/dev/peps/pep-0440/
    #
    # For a discussion on single-sourcing the version across setup.py and the
    # project code, see
    # https://packaging.python.org/en/latest/single_source_version.html
    version='2.0.0',  # Required

    # This is a one-line description or tagline of what your project does. This
    # corresponds to the "Summary" metadata field:
    # https://packaging.python.org/specifications/core-metadata/#summary
    description='An optimization model for nodal multi-product systems',  # Optional

    # This is an optional longer description of your project that represents
    # the body of text which users will see when they visit PyPI.
    #
    # Often, this is the same as your README, so you can just read it in from
    # that file directly (as we have already done above)
    #
    # This field corresponds to the "Description" metadata field:
    # https://packaging.python.org/specifications/core-metadata/#description-optional
    long_description=long_description,  # Optional

    # Denotes that our long_description is in Markdown; valid values are
    # text/plain, text/x-rst, and text/markdown
    #
    # Optional if long_description is written in reStructuredText (rst) but
    # required for plain-text or Markdown; if unspecified, "applications should
    # attempt to render [the long_description] as text/x-rst; charset=UTF-8 and
    # fall back to text/plain if it is not valid rst" (see link below)
    #
    # This field corresponds to the "Description-Content-Type" metadata field:
    # https://packaging.python.org/specifications/core-metadata/#description-content-type-optional
    long_description_content_type='text/markdown',  # Optional (see note above)

    # This should be a valid link to your project's main homepage.
    #
    # This field corresponds to the "Home-Page" metadata field:
    # https://packaging.python.org/specifications/core-metadata/#home-page-optional
    url='https://git.rwth-aachen.de/ltt/secmod/secmod',  # Optional

    # This should be your name or the name of the organization which owns the
    # project.
    author='Institute of Technical Thermodynamics - RWTH Aachen University',  # Optional

    # This should be a valid email address corresponding to the author listed
    # above.
    author_email='sekmod@ltt.rwth-aachen.de',  # Optional

    # Classifiers help users find your project by categorizing it.
    #
    # For a list of valid classifiers, see https://pypi.org/classifiers/
    classifiers=[  # Optional
        # How mature is this project? Common values are
        #   3 - Alpha
        #   4 - Beta
        #   5 - Production/Stable
        'Development Status :: 2 - Pre-Alpha',

        # Indicate who your project is intended for
        'Intended Audience :: Science/Research',
        'Topic :: Scientific/Engineering',

        # Pick your license as you wish
        # TO-DO
        #'License :: OSI Approved :: MIT License',

        # Specify the Python versions you support here. In particular, ensure
        # that you indicate whether you support Python 2, Python 3 or both.
        # These classifiers are *not* checked by 'pip install'. See instead
        # 'python_requires' below.
        'Programming Language :: Python :: 3.7',
    ],

    # This field adds keywords for your project which will appear on the
    # project page. What does your project relate to?
    #
    # Note that this is a string of words separated by whitespace, not a list.
    keywords='sector coupling optimization life cycle assessment milp',  # Optional

    # You can just specify package directories manually here if your project is
    # simple. Or you can use find_packages().
    #
    # Alternatively, if you just want to distribute a single Python file, use
    # the `py_modules` argument instead as follows, which will expect a file
    # called `my_module.py` to exist:
    #
    #   py_modules=["my_module"],
    #
    packages=find_packages(exclude=['data', 'contrib', 'docs', 'tests']),  # Required

    # Specify which Python versions you support. In contrast to the
    # 'Programming Language' classifiers above, 'pip install' will check this
    # and refuse to install the project if the version does not match. If you
    # do not support Python 2, you can simplify this to '>=3.5' or similar, see
    # https://packaging.python.org/guides/distributing-packages-using-setuptools/#python-requires
    python_requires='>=3.5',

    # This field lists other packages that your project depends on to run.
    # Any package you put here will be installed by pip when your project is
    # installed, so they must be valid existing projects.
    #
    # For an analysis of "install_requires" vs pip's requirements files see:
    # https://packaging.python.org/en/latest/requirements.html
    # to further limit which version of a package should be installed either use
    # '<packagename> == x.y.z' 
    # or use e.g. '<packagename> >= 1.1.2, < 2.4' in this case versions between 1.1.2 and 2.4 would be accepted
    install_requires=['appdirs==1.4.3',
                      'args==0.1.0',
                      'attrs==19.3.0',
                      'backcall==0.1.0',
                      'boto3==1.13.20',
                      'botocore==1.16.20',
                      'cached-property==1.5.1',
                      'certifi==2020.4.5.1',
                      'cffi>=1.14.0',
                      'chardet==3.0.4',
                      'click==7.1.2',
                      'clint==0.5.1',
                      'cloudpickle==1.4.1',
                      'colorama==0.4.3',
                      'cryptography==2.9.2',
                      'cycler==0.10.0',
                      'datapackage==1.14.0',
                      'decorator==4.4.2',
                      'docutils==0.15.2',
                      'entrypoints==0.3',
                      'et-xmlfile==1.0.1',
                      'future==0.18.2',
                      'geographiclib==1.50',
                      'geopy==1.22.0',
                      'idna==2.9',
                      'ijson==3.0.4',
                      'importlib-metadata==1.6.0',
                      'ipykernel==5.1.4',
                      'ipython==7.13.0',
                      'ipython-genutils==0.2.0',
                      'isodate==0.6.0',
                      'jdcal==1.4.1',
                      'jedi==0.17.0',
                      'jmespath==0.10.0',
                      'joblib==0.15.1',
                      'jsonlines==1.2.0',
                      'jsonpointer==2.0',
                      'jsonschema==3.2.0',
                      'jupyter-client==6.1.3',
                      'jupyter-core==4.6.3',
                      'kiwisolver==1.2.0',
                      'linear-tsv==1.1.0',
                      'matplotlib==3.2.1',
                      'nose==1.3.7',
                      'numexpr==2.7.1',
                      'numpy==1.18.1',
                      'openpyxl==3.0.3',
                      'pandas==1.3.5',
                      'pandastable==0.12.2.post1',                      
                      'parso==0.7.0',
                      'pathlib==1.0.1',
                      'pickleshare==0.7.5',
                      'Pillow==7.1.2',
                      'Pint==0.9',
                      'ply==3.11',
                      'prompt-toolkit==3.0.4',
                      'pycparser==2.20',
                      'Pygments==2.6.1',
                      'Pyomo==5.6.9',
                      'pyOpenSSL==19.1.0',
                      'pyparsing==2.4.7',
                      'PyQt5==5.12.3',
                      'PyQt5-sip==4.19.18',
                      'PyQtChart==5.12',
                      'PyQtWebEngine==5.12.1',
                      'pyrsistent==0.16.0',
                      'PySocks==1.7.1',
                      'python-dateutil==2.8.1',
                      'pytz==2020.1',
                      'PyUtilib==5.8.0',
                      'pywin32==227',
                      'pyzmq==18.1.1',
                      'requests==2.23.0',
                      'rfc3986==1.4.0',
                      's3transfer==0.3.3',
                      'scikit-learn==0.23.1',
                      'scipy==1.4.1',
                      'six==1.14.0',
                      'sklearn==0.0',
                      'spyder-kernels==1.9.1',
                      'SQLAlchemy==1.3.17',
                      'tableschema==1.18.0',
                      'tabulator==1.50.0',
                      'threadpoolctl==2.1.0',
                      'tikzplotlib==0.9.2',
                      'tornado==6.0.4',
                      'traitlets==4.3.3',
                      'tsam==1.1.0',
                      'unicodecsv==0.14.1',
                      'urllib3==1.25.8',
                      'wcwidth==0.1.9',
                      'win-inet-pton==1.1.0',
                      'wincertstore==0.2',
                      'xlrd==1.2.0',
                      'zipp==3.1.0'],  # Optional
    
    # List additional groups of dependencies here (e.g. development
    # dependencies). Users will be able to install these using the "extras"
    # syntax, for example:
    #
    #   $ pip install sampleproject[dev]
    #
    # Similar to `install_requires` above, these must be valid existing
    # projects.
    # extras_require={  # Optional
    #     'dev': ['check-manifest'],
    #     'test': ['coverage'],
    # },

    # If there are data files included in your packages that need to be
    # installed, specify them here.
    #
    # If using Python 2.6 or earlier, then these have to be included in
    # MANIFEST.in as well.
    # TO-DO
    # package_data={  # Optional
    #     'sample': ['package_data.dat'],
    # },

    # Although 'package_data' is the preferred approach, in some case you may
    # need to place data files outside of your packages. See:
    # http://docs.python.org/3.4/distutils/setupscript.html#installing-additional-files
    #
    # In this case, 'data_file' will be installed into '<sys.prefix>/my_data'
    # TO-DO
    # data_files=[('my_data', ['data/data_file'])],  # Optional

    # To provide executable scripts, use entry points in preference to the
    # "scripts" keyword. Entry points provide cross-platform support and allow
    # `pip` to create the appropriate form of executable for the target
    # platform.
    #
    # For example, the following would provide a command called `sample` which
    # executes the function `main` from this package when invoked:
    entry_points={  # Optional
        'console_scripts': [
            'secmod=secmod:main',
        ],
    },

    # List additional URLs that are relevant to your project as a dict.
    #
    # This field corresponds to the "Project-URL" metadata fields:
    # https://packaging.python.org/specifications/core-metadata/#project-url-multiple-use
    #
    # Examples listed include a pattern for specifying where the package tracks
    # issues, where the source is hosted, where to say thanks to the package
    # maintainers, and where to support the project financially. The key is
    # what's used to render the link text on PyPI.
    project_urls={  # Optional
        'Bug Reports': 'https://git.rwth-aachen.de/ltt/secmod/secmod/issues',
        # 'Funding': 'https://donate.pypi.org',
        # 'Say Thanks!': 'http://saythanks.io/to/example',
        'Source': 'https://git.rwth-aachen.de/ltt/secmod/secmod/',
    },
)
