grid_name = "ONENODE" #definition of the considered grid, predefined in the grid data. Defining multiple grids for seperate analyses is possible.
invest_years_per_optimization = 1 #foresight of the optimization. 1= one defined period (e.g. 5 years), 2 = 2 periods....
invest_years = [2020] #, 2020, 2025 #periods for which the system is optimized to obtain a transition pathway
interest_rate = 0.05 # interest rate for annualization
economic_period = "8 years" # economic annualization horizon

# Choose LCA framework - "ReCiPe Midpoint (H)", "ILCD 1.0.8 2016 midpoint", "cumulative energy demand" or "ILCD 2.0 2018 midpoint"
LCA_framework   = "ILCD 2.0 2018 midpoint" # LCIA method
# Override used impact categories to reduce preparation time - "None" if FRAMEWORK should be used
LCA_manual_impact_categories = ["cost", "ILCD 2.0 2018 midpoint:climate change:climate change total",
                                "ILCD 2.0 2018 midpoint:climate change:climate change biogenic",
                                "ILCD 2.0 2018 midpoint:climate change:climate change fossil",
                                "ILCD 2.0 2018 midpoint:climate change:climate change land use and land use change",
                                # "ILCD 2.0 2018 midpoint:ecosystem quality:freshwater and terrestrial acidification",
                                # "ILCD 2.0 2018 midpoint:ecosystem quality:freshwater ecotoxicity",
                                # "ILCD 2.0 2018 midpoint:ecosystem quality:freshwater eutrophication",
                                # "ILCD 2.0 2018 midpoint:ecosystem quality:marine eutrophication",
                                # "ILCD 2.0 2018 midpoint:ecosystem quality:terrestrial eutrophication",
                                # "ILCD 2.0 2018 midpoint:human health:carcinogenic effects",
                                # "ILCD 2.0 2018 midpoint:human health:ionising radiation",
                                # "ILCD 2.0 2018 midpoint:human health:non_carcinogenic effects",
                                # "ILCD 2.0 2018 midpoint:human health:ozone layer depletion",
                                # "ILCD 2.0 2018 midpoint:human health:photochemical ozone creation",
                                # "ILCD 2.0 2018 midpoint:human health:respiratory effects, inorganics",
                                # "ILCD 2.0 2018 midpoint:resources:dissipated water",
                                # "ILCD 2.0 2018 midpoint:resources:fossils",
                                # "ILCD 2.0 2018 midpoint:resources:land use",
                                "ILCD 2.0 2018 midpoint:resources:minerals and metals"]

# List of optimization result variables to export to a separate csv file
# Make sure to use correct format of names (set in result_processing)
results_list_to_csv = ['non_served_demand', 'new_capacity_production', 'new_capacity_storage', 'demand', 'product_flow', 'product_flow_production',
                'product_flow_storage', 'product_flow_non_served_demand', 'product_flow_add_demand', 'operational_impact', 'invest_impact',
                'operational_impact_production', 'invest_impact_production','operational_impact_storage', 'invest_impact_storage', 
                'operational_impact_non_served_demand', 'invest_impact_non_served_demand','operational_impact_add_demand', 'invest_impact_add_demand',
                'total_impact', 'total_impact_production', 'total_impact_production', 'total_impact_non_served_demand',
                'product_flow_deposit_into_storage','product_flow_withdrawal_from_storage']

# Chose the optimization mode to be MILP. For the LP code, please refer to our other git repository.
optimization_mode = "MILP"

# unit dictionary necessary for all not used products
unit_dictionary = {product: "MW" for product in ['cold', 'electricity', 'heat general', 'natural gas', 'heat high T']}

# State the products that can be purchased (products_importable) or sold (products_exportable)
products_importable = ['natural gas',
                       'electricity']
products_exportable = ['electricity']

# Set parameters for the timeseries aggregation, done by TSAM
# Documentation of all parameters is here https://github.com/FZJ-IEK3-VSA/tsam/blob/master/tsam/timeseriesaggregation.py
# See "class TimeSeriesAggregation"
time_series_aggregation_config = {
    "resolution"                : None,
    "noTypicalPeriods"          : 6,
    "hoursPerPeriod"            : 24,
    "segmentation"              : False,
    "noSegments"                : 1,
    "clusterMethod"             : 'k_medoids', #'k_medoids', default: 'hierarchical'
    "evalSumPeriods"            : False,
    "sortValues"                : False,
    "sameMean"                  : False,
    "rescaleClusterPeriods"     : True,
    "extremePeriodMethod"       : 'None', #'new_cluster_center', 'None'
    "predefClusterOrder"        : None,
    "predefClusterCenterIndices": None,
    "solver_tsa"                : "gurobi",
    "roundOutput"               : None,
    "addPeakMin"                : None,
    "addPeakMax"                : None, #{'demand': ['electricity','cold','heat general']}, # None or nested-dict, e.g. {'demand': ['electricity','natural gas']}
    "addMeanMin"                : None,
    "addMeanMax"                : None,
    "use_time_series_aggregation": True,
    "correct_dateformat"        : '%d.%m.%Y %H:%M', # desired format of timeseries indizes
    "partially_correct_dateformat": '%d.%m.%Y %H:%M:%S', # partially correct format of timeseries indizes that is meant to be changed
    "wrong_dateformat"          : '%d-%b-%Y %H:%M:%S' # current format of timeseries indizes that is meant to be changed
}

solver = "gurobi_persistent" # chosen solver. Please note that Gurobi requires a seperate license.
solver_options = {
            #"ObjScale": 1,
            "LogToConsole": 1,
            "NumericFocus": 0, # default 0
            "ScaleFlag": -1, # default -1
            "ResultFile": "model.ilp", # writes irreducible infeasible model
            "mipgap": 0.01 # set solver gap
            }

debug_optimization = True

load_raw_input = True
load_existing_input_dict = False
load_existing_results = False
new_pint_units = "./SecMOD/00-INPUT/00-RAW-INPUT/00-UNITS-DEFINITIONS.txt"

