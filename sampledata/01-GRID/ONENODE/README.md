# README - GRID DE

This data package contains all the data for the MILP model. To account for spatial resolution, the model consists of nodes and connections. Nodes have an integer
ID and geo coordinates. Connections have an integer ID as well
and are defined by the two nodes they connect. Please note that the location of our nodes was chosen randomly and is not relevant in our case study. 
