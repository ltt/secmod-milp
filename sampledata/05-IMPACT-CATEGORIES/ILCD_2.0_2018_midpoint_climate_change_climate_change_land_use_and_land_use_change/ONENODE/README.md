# README - IMPACT CATEGORY - ILCD 2.0 2018 midpoint:climate change:land use and land use change - ONE NODE

This directory contains all necessary information about this
impact category, including the nodal and total impact limits in the grid ONE NODE.
