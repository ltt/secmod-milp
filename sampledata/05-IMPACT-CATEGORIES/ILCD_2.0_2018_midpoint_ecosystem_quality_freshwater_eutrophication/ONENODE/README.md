# README - IMPACT CATEGORY - ILCD 2.0 2018 midpoint:ecosystem quality:freshwater eutrophication - ONE NODE

This directory contains all necessary information about this
impact category, including the nodal and total impact limits in the grid ONE NODE.
