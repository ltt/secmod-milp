# README - IMPACT CATEGORIES

This directory contains all impact categories used in the optimization and assessment.
An impact category has a unique name and a corresponding name used
in the ecoinvent database. The nodal and total impact limits for operation, invest and total impact and the objective of the optimization can be specified in each respective folder. Please note that further LCIA categories can be added by copying and renaming the folders by the name of the respective category assessed in the LCIA database.
