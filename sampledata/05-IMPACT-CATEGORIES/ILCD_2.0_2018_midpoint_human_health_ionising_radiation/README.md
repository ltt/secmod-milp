# README - IMPACT CATEGORY - ILCD 2.0 2018 midpoint:human health:ionising radiation

This directory contains all necessary information about this
impact category, including the nodal and total impact limits.
