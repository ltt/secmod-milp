# README - IMPACT CATEGORY - ILCD 2.0 2018 midpoint:resources:minerals and metals

This directory contains all necessary information about this
impact category, including the nodal and total impact limits.
