# README - ECOINVENT

This data package contains the life cycle impact assessment. Please note that all proprietary data in the ecoinvent table was replaced by placeholder numbers and needs to be replaced by the actual ecoinvent data to carry out life cycle assessments. Please note that we deleted some values in the subassemblies to avoid potential license conflicts.
The processes propane emission and isobutene emission were added based on ILCD and ReCiPe characterisation factors for the respective materials (process only has one material flow without production chain).
