# SecMOD Data

This project includes the data set based on our publication.
Due to license restrictions, we deleted all proprietary data, e.g., the 04-ECOINVENT/ecoinvent.csv currently does not contain any meaningful values. However, the structure of the database is visible. 
If used for other projects, the ecoinvent file 04-ECOINVENT/ecoinvent.csv should be updated with the given format and original data. 
