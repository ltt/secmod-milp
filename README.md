# SecMOD MILP - A framework for multi-energy system optimization and life-cycle assessment 
The SecMOD MILP framework is a flexible framework for the optimization and life-cycle assessment of industrial multi-energy systems.
The SecMOD MILP framework entails continuous sizing of discrete components including minimal part-load and part-load dependent efficiencies.

## Referencing

If you use our software or any part of it, please cite [Reinert, Nolzen et al. (2023)](https://www.sciencedirect.com/science/article/pii/S0098135423000455). The full information on the publication is also shown below:

Reinert, C., Nolzen, N., Frohmann, J., Tillmanns, D., and Bardow, A. (2023): "Design of low-carbon multi-energy systems in the SecMOD framework by combining 
MILP optimization and life-cycle assessment". Computer & Chemical Engineering. DOI: https://doi.org/10.1016/j.compchemeng.2023.108176.

Since the publication above by Reinert, Nolzen, et al. and this repository only describe the extensions compared to the linear SecMOD, we also refer to for further information to
[Reinert et al. (2022)](https://www.frontiersin.org/articles/10.3389/fenrg.2022.884525/abstract). The full information on the publication is also shown below:

Reinert, C.; Schellhas, L.; Mannhardt, J.; Shu, D.; Kämper, A.; Baumgärtner, N.; Deutz, S., and Bardow, A. (2022): "SecMOD: An open-source modular framework
combining multi-sector system optimization and life-cycle assessment". Frontiers in Energy Research. DOI: https://doi.org/10.3389/fenrg.2022.884525.

## License
This project is licensed under the MIT license, for more information please refer to the license file.

## Documentation and Support 
**Please, find the full documentation of the SecMOD MILP framework [here](SecMOD_MILP_Documentation.pdf).**

In addition, please, find the full documentation of the SecMOD LP framework [here](https://ltt.pages.git-ce.rwth-aachen.de/opt/secmod/secmod/).

You can further find a video about SecMOD and some example applications [here](https://www.youtube.com/watch?v=wXFocTL95hs).

In case you need help using Git, please refer to the git documentation [here](https://git-scm.com/docs).

## Installation
A brief instruction to install SecMOD can also be found below:
Clone a copy of the whole repository to your computer:
```
git clone git@git-ce.rwth-aachen.de:ltt/secmod-milp.git
```

Open a terminal with a python enviroment (e.g. Anaconda promt) and install the secmod package with:
```shell
pip install --user -e '<Path\to\cloned\repository>'
``` 
The path should point to the directory where secmod is saved (repository folder).
Make sure that the setup.py file is located in your repository folder. You can use this installation for several projects in multiple working directories.

For further installation instruction please go [here](https://ltt.pages.git-ce.rwth-aachen.de/opt/secmod/secmod/usage/installation.html). The code was tested with Python 3.7.5. 
## First Steps

Create a working directory for the optimization framework. Next, cd to that directory and enter
```shell
python -m secmod.setup
```
in the terminal. This sets up the right folder structure. 
Start the optimization with double clicking the start.bat file, where you can choose your enviroment or 
alternatively use 

```shell
python -m secmod
```
to start. 

