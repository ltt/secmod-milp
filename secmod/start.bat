call REM Edit this file to configure your startup!
:: if you are using anaconda, add your anaconda path and the wished
:: virtual environment below and uncomment the next two lines
call C:\Anaconda3\Scripts\activate
python -m secmod

:: if you have installed python directly, add its path below
:: and uncomment the next line (make sure to comment the lines above)
:: D:\Niklas\milp2publish_env\python.exe -m secmod

pause