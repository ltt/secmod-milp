import abc
import copy
import logging
import os
import json
from pathlib import Path
from clint.textui import progress

import numpy as np
import pandas as pd
import pint
import importlib.util
import secmod.helpers
from geopy import distance

# Get current working directory
working_directory = Path.cwd()

# Load config.py
spec = importlib.util.spec_from_file_location("config", "SecMOD/00-INPUT/00-RAW-INPUT/config.py")
config = importlib.util.module_from_spec(spec)
spec.loader.exec_module(config)

units = pint.UnitRegistry()
# Load new unit definitions for the pint package, if path to TXT-file is provided
if config.new_pint_units != None:
    logging.info("Load new unit definitions for the pint package")
    units.load_definitions(str(working_directory / config.new_pint_units))
    logging.info("Done")
pint.set_application_registry(units)


class Grid(object):
    """This class is used to manage grids of nodes and connections.

    This class represents a grid, which consists of a name, nodes and connections between these nodes.
    It can be created from scratch by providing at least a unique name and optionally pandas dataframes for
    data about the nodes and connections. It can alternatively be created from a path to a directory of an
    existing grid. The name will then be taken from the name of the grid directory and the node data
    and connection data will be loaded from .csv-files.

    Furthermore the class has a static list of all created instances and a single static variable
    which represents the selected grid for the optimization.

    Args:
            grid_path (Path): A path of the directory of an existing grid definition, which includes a 
                CSV-file with data about nodes and connections in the grid.

            name (str): Name for a new grid, if no grid_path is provided. The name has to be unique,
                since no two grids can have the same name.

            nodes (pd.Dataframe): A pandas dataframe which has all the information about the nodes of
                a new grid, if no grid_path is provided. This dataframe must have the columns 
                ["node","latitude","longitude"].

            connections(pd.Dataframe): A pandas dataframe which has all the information about the 
                connections of a new grid, if no grid_path is provided. This dataframe must have
                the columns ["connection","node1","node2"].

    Attributes:
        grids (list): Static list of all instanciated grids.

        _selected (Grid): Static reference to a single Grid instance which is selected to be 
            used in the optimization. Can be reached via the selected()-method.

    Raises:
        ValueError: If no name is provided, if no path is provided as well.

        FileNotFoundError: If "nodes.csv" or "connections.csv" does not exist in the provided directory.

        NotADirectoryError: If the provided path is no directory.
    """

    CONNECTION_NODES = {None: ["node1", "node2"]}

    grids = []
    _selected = None

    def __init__(self, grid_path: Path = None, name: str = None, nodes: pd.DataFrame = None, connections: pd.DataFrame = None):
        """See class documentation above for more information on the initialization."""

        # If no path to an existing grid is provided, a new grid is created
        if grid_path == None:
            # If a name for the new grid is provided it is set as name
            if name != None:
                self.name = name
                logging.debug(
                    "Initialize a new instance of Grid from scratch - name: {0}...".format(self.name))
            # If no name is provided, an exception is raised. Every grid needs a name.
            else:
                raise ValueError(
                    'A grid without a name is no grid. Please provide a unique name for your very personal grid.')
            # If existing node data is provided it is set as nodes
            if nodes != None:
                self.nodes = nodes
            # If no node data is provided, a new empty dataframe with the needed columns is created
            else:
                self.nodes = pd.DataFrame(
                    columns=["latitude", "longitude", "comments"])
                self.nodes.index.name = "node"
                logging.debug(
                    "=== Empty dataframe for information about nodes created.")
            # If existing connection data is provided it is set as connections
            if connections != None:
                self.connections = connections
            # If no connection data is provided, a new empty dataframe with the needed columns is created
            else:
                self.connections = pd.DataFrame(
                    columns=["node1", "node2", "comments"])
                self.connections.index.name = "connection"
                logging.debug(
                    "=== Empty dataframe for information about connections created.")
        # If a path to an existing grid is provided, the name, nodes and connections are loaded from that directory
        else:
            # checks, if the provided path is a directory
            if grid_path.is_dir():
                self.name = str(grid_path.name).replace("_", " ")
                logging.debug(
                    "Initialize a new instance of Grid from directory - name: {0}...".format(self.name))
                # checks, if the file "nodes.csv" exists, otherwise raises an exception
                if not (grid_path / "nodes.csv").is_file():
                    raise FileNotFoundError(
                        "A grid exists out of nodes and connections. Without nodes a grid can't exist. Please provide a file 'nodes.csv' in your grid directory.")
                # checks, if the file "connections.csv" exists, otherwise raises an exception
                if not (grid_path / "connections.csv").is_file():
                    raise FileNotFoundError(
                        "A grid exists out of nodes and connections. Without connections a grid can't exist. Please provide a file 'connection.csv' in your grid directory.")
                self.nodes = pd.read_csv(
                    grid_path / "nodes.csv", index_col="node", usecols=lambda x: "Unnamed" not in x, float_precision="high", encoding= 'unicode_escape')
                logging.debug("=== Nodal information read.")
                self.connections = pd.read_csv(
                    grid_path / "connections.csv", index_col="connection", usecols=lambda x: "Unnamed" not in x, float_precision="high", encoding= 'unicode_escape')
                logging.debug("=== Connection information read.")
            # if the provided path is no directory, an exception is raised
            else:
                raise NotADirectoryError(
                    "Path {0} is not a directory. Please provide a path to the directory of the grid.".format(str(grid_path)))
        Grid.grids.append(self)
        logging.debug("=== Grid instance appended to the static Grid list.")
        logging.debug("=== Grid instance initialized.")

    @classmethod
    def load_grids_from_directory(cls, grids_category_path: Path):
        """Loads all grids of a specific class into their class."""

        logging.info("Load grids of class '{0}".format(str(cls)))

        # Get a list of all files and folders in the directory
        grid_paths = os.listdir(grids_category_path)

        # Iterate through the members of the grid directory
        for grid in progress.bar(grid_paths):
            # Check if the file ending is "csv"
            if (grids_category_path / grid).is_dir():
                # If the file is csv, read it in as a dataframe
                cls(grids_category_path / grid)

    @property
    def name(self):
        """This property contains a unique name for the grid.

        Getter:
            Gets the value of this property.

            Returns:
                Name of the grid as string.

        Setter:
            Sets the property to the provided name, if it isn't already used by another grid.

            Raises:
                ValueError: If the name is already used by another grid.

        Type:
            str
        """

        return self._name

    @name.setter
    def name(self, name):
        """See property docstring above for setter information."""

        # If the provided name is equal to the current name, do nothing
        if hasattr(self, "_name"):
            if self.name == name:
                return
        # Check if any other defined grid has the provided name
        for grid in Grid.grids:
            if grid.name == name:
                raise ValueError(
                    "There is already a grid with the name '{0}'. Please choose another name.".format(name))
        self._name = name

    @property
    def nodes(self):
        """This property contains all information about the nodes of a grid.

        Getter:
            Gets the value of this property.

            Returns:
                Dataframe with the information about the nodes of the grid.

        Setter:
            Sets the property to the provided dataframe, if the dataframe
            has the columns ["node","latitude","longitude"].

        Type:
            pd.dataframe
        """

        return self._nodes

    @nodes.setter
    def nodes(self, nodes):
        """See property docstring above for setter information."""

        # Checks if the necessary columns exist in the provided dataframe
        if set(["latitude", "longitude"]).issubset(nodes.columns):
            # Sets the provided dataframe as new node information
            self._nodes = nodes
        else:
            # Raises ValueError, if the necessary columns do not exist
            raise ValueError(
                'The file "nodes.csv" does not provide the necessary columns: ["node","latitude","longitude"]')

    @staticmethod
    def get_list_of_node_ids():
        """Return a list of all node ID numbers of the selected grid."""

        return(list(Grid.selected()._nodes.index.values))

    @property
    def connections(self):
        """This property contains all information about the connections between nodes of a grid.

        Getter:
            Gets the value of this property.

            Returns:
                Dataframe with the information about the connections of the grid.

        Setter:
            Sets the property to the provided dataframe, if the dataframe
            has the columns ["connection","node1","node2"].

        Type:
            pd.dataframe
        """

        return self._connections

    @connections.setter
    def connections(self, connections):
        """See property docstring above for setter information."""

        # Checks if the necessary columns exist in the provided dataframe
        if set(["node1", "node2"]).issubset(connections.columns):
            # Sets the provided dataframe as new connection information
            self._connections = connections
        else:
            # Raises ValueError, if the necessary columns do not exist
            raise ValueError(
                'The provided data of connections does not have the necessary columns: ["node1","node2"]')

    @staticmethod
    def get_list_of_connection_ids():
        """Return a list of all node ID numbers of the selected grid."""

        return(list(Grid.selected()._connections.index.values))

    def setup_distances(self):
        """Sets up the distances of all connections, using pint units.

        Uses geodesic distance from the package GeoPy (https://geopy.readthedocs.io/en/stable/#module-geopy.distance)
        to calculate the distance between the two nodes of a connection.
        """

        # Get coordinates of both nodes of all connections of the grid
        coordinates = self.connections.join(self.nodes[["longitude", "latitude"]], on="node1").join(
            self.nodes[["longitude", "latitude"]], on="node2", lsuffix="_1", rsuffix="_2")

        # Determine distance between both nodes of all connections of the grid
        self.connections["distance"] = coordinates.apply(
            lambda connection: Grid.calculate_distances(connection), axis=1)

    @staticmethod
    def calculate_distances(connection):
        """Calculates the distance or takes it from existing data.

        Uses geodesic distance from the package GeoPy (https://geopy.readthedocs.io/en/stable/#module-geopy.distance)
        to calculate the distance between the two nodes of a connection, if no distance is provided in the data.

        Args:
            connection: An itertuple from the connections DataFrame.
        """

        # Check if the column for manual distance exists in the loaded connection data
        if hasattr(connection, "manual_distance"):
            # If a distance value does not exist, calculate it with the geodetic distance
            if pd.isnull(connection.manual_distance):
                return units(str(distance.distance((connection.latitude_1, connection.longitude_1), (connection.latitude_2, connection.longitude_2))))
            else:
                # Check if the column for the unit of manual distances exists in the loaded connection data
                if hasattr(connection, "manual_distance_unit"):
                    # If a unit value does not exist, assume the distance to be in kilometers
                    if pd.isnull(connection.manual_distance_unit):
                        # Check if the manual distance is smaller than the minimal possible geodetic distance and issue a warning, if it is
                        if (units(str(connection.manual_distance) + " km") < units(str(distance.distance((connection.latitude_1, connection.longitude_1), (connection.latitude_2, connection.longitude_2))))):
                            logging.warning(
                                "Manual distance of connection {0} is smaller than minimal geodetic distance!".format(connection.name))
                            logging.warning("=== Manual distance: {0}".format(
                                units(str(connection.manual_distance) + " km")))
                            logging.warning("=== Geodetic distance: {0}".format(units(str(distance.distance(
                                (connection.latitude_1, connection.longitude_1), (connection.latitude_2, connection.longitude_2))))))
                        # Return distance in the assumed unit of kilometers
                        return units(str(connection.manual_distance) + " km")
                    else:
                        # Check if the manual distance is smaller than the minimal possible geodetic distance and issue a warning, if it is
                        if (units(str(connection.manual_distance) + str(connection.manual_distance_unit)) < units(str(distance.distance((connection.latitude_1, connection.longitude_1), (connection.latitude_2, connection.longitude_2))))):
                            logging.warning(
                                "Manual distance of connection {0} is smaller than minimal geodetic distance!".format(connection.name))
                            logging.warning("=== Manual distance: {0}".format(
                                units(str(connection.manual_distance) + " km")))
                            logging.warning("=== Geodetic distance: {0}".format(units(str(distance.distance(
                                (connection.latitude_1, connection.longitude_1), (connection.latitude_2, connection.longitude_2))))))
                        # Return distance in the provided manual distance unit
                        return units(str(connection.manual_distance) + str(connection.manual_distance_unit))
                else:
                    # Assume the unit to be kilometers, if the column of the manual distance unit does not exist
                    logging.warning(
                        "No unit provided for manual distance. Kilometers are assumed.")
                    return units(str(connection.manual_distance) + " km")
        else:
            # Return the geodetic calculated distance
            return units(str(distance.distance((connection.latitude_1, connection.longitude_1), (connection.latitude_2, connection.longitude_2))))

    @staticmethod
    def selected():
        """This methods gets the value of the static variable "selected".

        This method returns the Grid object which is selected to be used
        as grid for the optimization. The static variable "selected" can 
        only contain one Grid object and is set using the method "select()"
        of an instance of the Grid class.

        Returns:
            Value of _selected, which is either None or a Grid instance.
        """

        return Grid._selected

    def select(self):
        """This methods sets the value of the static member "selected".

        The static member "selected" is set to reference the Grid instance
        which called this method.
        """

        # Setup the distances of all connections in the grid, based on node coordinates or manual distances
        self.setup_distances()
        # Set the static variable Grid._selected to the current instance
        Grid._selected = self
        # Return True, if successfull
        return True


class Product(object):
    """This class is used for all product related data, e.g. demand, impacts, etc."""

    # Default value for average yearly costs, if value is missing
    DEFAULT_COST = 3000
    # List of all products
    products = []

    def __init__(self, product_path: Path):
        """See class documentation above for more information on the initialization."""

        logging.debug("Initialize a new instance of Product.")
        
        if product_path == None:
            # Do things if None has been given as path.
            raise NotImplementedError
        else:
            # Check if product directory exists
            if product_path.is_dir():
                # Get name by directory name
                self.name = str(product_path.name).replace("_", " ")
                # Load average yearly product purchase price
                self._costs_yearly_average = pd.read_csv(
                    product_path / "costs_yearly_average.csv", float_precision="high", usecols=(lambda column: column != "comments"), encoding= 'unicode_escape')
                # Fill empty values with class-wide default costs
                self._costs_yearly_average.fillna(Product.DEFAULT_COST, inplace=True)
                # Unitize dataframe
                self._costs_yearly_average = secmod.helpers.unitize_dataframe(self._costs_yearly_average, units)
                # Load relative yearly product purchase price timeseries
                self._costs_yearly_timeseries = secmod.helpers.correct_time_stamp_of_timeseries(pd.read_csv(
                    product_path / "costs_yearly_timeseries.csv", index_col="time slice", float_precision="high", usecols=(lambda column: column != "comments"), encoding= 'unicode_escape'))
                # Fill empty values with 1 as the yearly average should be used then.
                self._costs_yearly_timeseries.fillna(1, inplace=True)
                # Unitize dataframe
                self._costs_yearly_timeseries = secmod.helpers.unitize_dataframe(self._costs_yearly_timeseries, units)
                # Load impact definition for non-served demand
                self._processes_non_served_demand = pd.read_csv(
                    product_path / "ecoinvent_operation.csv", float_precision="high", usecols=(lambda column: column != "comments"), encoding= 'unicode_escape')
                
                if config.optimization_mode == 'MILP':
                    # load selling prices for decentralized energy system
                    
                    # Load average yearly product selling price
                    self._revenues_yearly_average = pd.read_csv(
                        product_path / "revenues_yearly_average.csv", float_precision="high", usecols=(lambda column: column != "comments"), encoding= 'unicode_escape')
                    # Fill empty values with class-wide default costs
                    self._revenues_yearly_average.fillna(Product.DEFAULT_COST, inplace=True)
                    # Unitize dataframe
                    self._revenues_yearly_average = secmod.helpers.unitize_dataframe(self._revenues_yearly_average, units)
                    # Load relative yearly product selling price timeseries
                    self._revenues_yearly_timeseries = secmod.helpers.correct_time_stamp_of_timeseries(pd.read_csv(
                        product_path / "revenues_yearly_timeseries.csv", index_col="time slice", float_precision="high", usecols=(lambda column: column != "comments"), encoding= 'unicode_escape'))
                    # Fill empty values with 1 as the yearly average should be used then.
                    self._revenues_yearly_timeseries.fillna(1, inplace=True)
                    # Unitize dataframe
                    self._revenues_yearly_timeseries = secmod.helpers.unitize_dataframe(self._revenues_yearly_timeseries, units)
                    # Load impact definition for add-served demand
                    self._processes_add_served_demand = pd.read_csv(
                        product_path / "ecoinvent_operation.csv", float_precision="high", usecols=(lambda column: column != "comments"), encoding= 'unicode_escape')
                
                # Check if subdirectory for grid-dependent data exists
                if (product_path / Grid.selected().name).is_dir():
                    # Load average nodal demand
                    self._demand_nodal_average = pd.read_csv(
                        product_path / Grid.selected().name / "demand_nodal_average.csv", float_precision="high", usecols=(lambda column: column != "comments"))
                    # Fill empty values with 0 as there should be no demand then.
                    self._demand_nodal_average.fillna(0, inplace=True)
                    # Unitize dataframe
                    self._demand_nodal_average = secmod.helpers.unitize_dataframe(self._demand_nodal_average, units)
                    # Load relative yearly nodal demand timeseries
                    self._demand_nodal_timeseries = secmod.helpers.correct_time_stamp_of_timeseries(pd.read_csv(
                        product_path / Grid.selected().name / "demand_nodal_timeseries.csv", index_col="time slice", float_precision="high", usecols=(lambda column: column != "comments"), encoding= 'unicode_escape'))
                    # Fill empty values with 1 as the yearly average should be used then.
                    self._demand_nodal_timeseries.fillna(1, inplace=True)
                    # Unitize dataframe
                    self._demand_nodal_timeseries = secmod.helpers.unitize_dataframe(self._demand_nodal_timeseries, units)

                    # Required secured capacity
                    self._required_total_secured_capacity = pd.read_csv(
                        product_path / Grid.selected().name / "required_total_secured_capacity.csv", float_precision="high", usecols=(lambda column: column != "comments"), encoding= 'unicode_escape')
                    # Fill empty values with 0 as there should be no required secured capacity then.
                    self._required_total_secured_capacity.fillna(0, inplace=True)
                    # Unitize dataframe
                    self._required_total_secured_capacity = secmod.helpers.unitize_dataframe(self._required_total_secured_capacity, units)
                    # Required secured capacity
                    self._required_average_nodal_secured_capacity = pd.read_csv(
                        product_path / Grid.selected().name / "required_average_nodal_secured_capacity.csv", float_precision="high", usecols=(lambda column: column != "comments"), encoding= 'unicode_escape')
                    # Fill empty values with 0 as there should be no required secured capacity then.
                    self._required_average_nodal_secured_capacity.fillna(0, inplace=True)
                    # Unitize dataframe
                    self._required_average_nodal_secured_capacity = secmod.helpers.unitize_dataframe(self._required_average_nodal_secured_capacity, units)
                    # Load relative yearly nodal required secured capacity
                    self._required_relative_nodal_secured_capacity = pd.read_csv(
                        product_path / Grid.selected().name / "required_relative_nodal_secured_capacity.csv", index_col="node", float_precision="high", usecols=["node", "unit", "value"], encoding= 'unicode_escape')
                    # Fill empty values with 1 as the yearly average should be used then.
                    self._required_relative_nodal_secured_capacity.fillna(1, inplace=True)
                    # Unitize dataframe
                    self._required_relative_nodal_secured_capacity = secmod.helpers.unitize_dataframe(self._required_relative_nodal_secured_capacity, units)
                else:
                    logging.warning("No demand data found for process '{0}' in grid '{1}'! Existing and potential capacities are assumed.".format(
                        self._name, Grid.selected().name))
                    # Create empty DataFrames, if no grid-dependent data is provided
                    self._demand_nodal_average = pd.DataFrame(index=Grid.selected().nodes.index.tolist(
                    ), data=0, columns=ProcessImpacts.invest_years)
                    self._demand_nodal_timeseries = pd.DataFrame(index=Grid.selected().nodes.index.tolist(
                    ), data=1, columns=Grid.selected().get_list_of_node_ids())
            else:
                raise NotADirectoryError(
                    "Path {0} is not a directory. Please provide a path to the directory of the product.".format(str(product_path)))
        # Append product instance to static list of products
        Product.products.append(self)

    @classmethod
    def load_products_from_directory(cls, products_path: Path):
        """Loads all products of a specific class into their class."""

        logging.info("Load products of class '{0}".format(str(cls)))

        # Get a list of all files and folders in the directory
        product_paths = os.listdir(products_path)

        # Iterate through the members of the process directory
        for process in progress.bar(product_paths):
            # Check if the file ending is "csv"
            if (products_path / process).is_dir():
                # If the file is csv, read it in as a dataframe
                cls(products_path / process)
    
    @property
    def name(self):
        """This property contains a unique name for the grid.

        Getter:
            Gets the value of this property.

            Returns:
                Name of the grid as string.

        Setter:
            Sets the property to the provided name, if it isn't already used by another grid.

            Raises:
                ValueError: If the name is already used by another grid.

        Type:
            str
        """

        return self._name

    @name.setter
    def name(self, name):
        """See property docstring above for setter information."""

        # If the provided name is equal to the current name, do nothing
        if hasattr(self, "_name"):
            if self.name == name:
                return
        # Check if any other defined grid has the provided name
        for product in Product.products:
            if product.name == name:
                raise ValueError(
                    "There is already a product with the name '{0}'. Please choose another name.".format(name))
        self._name = name
    
    def _get_price_time_series(self, year: int):
        """Returns the absolute price time series"""

        df = self._costs_yearly_timeseries.apply((lambda column: column * self._costs_yearly_average[str(year)].values), axis=0)
        return df
    
    def _get_selling_price_time_series(self, year: int):
        """Returns the absolute selling price time series"""

        df = self._revenues_yearly_timeseries.apply((lambda column: column * self._revenues_yearly_average[str(year)].values), axis=0)
        return df
    
    def _get_cost_non_served_demand(self, reference_year: int, invest_year: int):
        """This method gets the costs of non-served demand of a product, i.e. for purchasing a product.

        Calculates the present value of the selected impact year and its investment period
        in the reference year for a product.

        Args:
            reference_year (int): Reference year of the optimized time horizon, e.g. 2020

            invest_year (int): An investment year of the optimized time horizon, e.g. 2025
        """

        # Determine the length of the current investment period
        investment_period_duration = ProcessImpacts._get_investment_period_duration(
            invest_year)
        
        # Calculate annuity present value factor for the current investment period
        investment_period_annuity_present_value_factor = ((1+ProcessImpacts.interest_rate) ** (investment_period_duration / units.year) - 1) / (
            (1+ProcessImpacts.interest_rate) ** (investment_period_duration / units.year) * ProcessImpacts.interest_rate)

        return (
            # Get costs per non-served demand
            self._get_price_time_series(invest_year)
            # multiplied with the annuity present value factor of the current investment period
            * investment_period_annuity_present_value_factor
            # discounted to reference year
            / (1 + ProcessImpacts.interest_rate)**(invest_year - reference_year)
        )
    
    def _get_revenues_add_served_demand(self, reference_year: int, invest_year: int):
        """This method gets the costs of add-served demand of a product, i.e. for selling a product.

        Calculates the present value of the selected impact year and its investment period
        in the reference year for a product.

        Args:
            reference_year (int): Reference year of the optimized time horizon, e.g. 2020

            invest_year (int): An investment year of the optimized time horizon, e.g. 2025
        """

        # Determine the length of the current investment period
        investment_period_duration = ProcessImpacts._get_investment_period_duration(
            invest_year)
        
        # Calculate annuity present value factor for the current investment period
        investment_period_annuity_present_value_factor = ((1+ProcessImpacts.interest_rate) ** (investment_period_duration / units.year) - 1) / (
            (1+ProcessImpacts.interest_rate) ** (investment_period_duration / units.year) * ProcessImpacts.interest_rate)

        return (
            # Get costs per non-served demand
            self._get_selling_price_time_series(invest_year)
            # multiplied with the annuity present value factor of the current investment period
            * investment_period_annuity_present_value_factor
            # discounted to reference year
            / (1 + ProcessImpacts.interest_rate)**(invest_year - reference_year)
        )

    def get_impact_non_served_demand(self, impact_categories: list, reference_year: int, invest_years: list):
        """This methods gets the impact of non-served demand of a product in one or multiple impact categories for a specific year.
        
        If the impact category is "cost", the corresponding method Product._get_cost_non_served_demand is used.
        In all other cases the impacts of all ecoinvent processes defined by the process itself as the
        impacts of non-served demand, are multiplied with their corresponding weight factors and summed up together.
        """

        impact_non_served_demand_list = []
        impact_non_served_demand_combined = pd.DataFrame()
        
        for invest_year in invest_years:
            # Create empty dictionary for the operational impacts
            impact_non_served_demand = pd.DataFrame(data=units("0"), index=self._costs_yearly_timeseries.index, columns=["product", "invest year"] + impact_categories)
            impact_non_served_demand["product"] = self.name
            impact_non_served_demand["invest year"] = invest_year

            # Loop over all requested impact categories
            for impact_category in impact_categories:
                # Check if impact category is "cost", if it is, use special methods from ProcessImpacts
                if impact_category == "cost":
                    impact_non_served_demand[impact_category] = self._get_cost_non_served_demand(
                        reference_year, invest_year)

                # If impact category is not "cost" calculate the sum of the impacts of all invest processes
                else:
                    impact_non_served_demand[impact_category] = EcoinventImpacts._recursively_get_impact(self._processes_non_served_demand, impact_category, invest_year, invest_year)

            impact_non_served_demand.reset_index(inplace=True)
            impact_non_served_demand.set_index(["product", "invest year", "time slice"], inplace=True)
            impact_non_served_demand_list.append(impact_non_served_demand)

        impact_non_served_demand_combined = impact_non_served_demand_combined.append(other=impact_non_served_demand_list, verify_integrity=True, sort=True)

        return impact_non_served_demand_combined

    @staticmethod
    def get_combined_impact_non_served_demand():
        """Returns the combined impact time series for all products and years."""

        combined_impact_time_series = pd.DataFrame()
        impact_time_series_list = []
        for product in progress.bar(Product.products):
            impact_time_series_list.append(product.get_impact_non_served_demand(ImpactCategory.get_list_of_names_of_active_impact_categories(), min(ProcessImpacts.invest_years), ProcessImpacts.invest_years))
        
        combined_impact_time_series = combined_impact_time_series.append(other=impact_time_series_list, verify_integrity=True, sort=True)
        index_names = combined_impact_time_series.index.names
        combined_impact_time_series = combined_impact_time_series.stack()
        combined_impact_time_series.index.names = index_names + ["impact category"]
        combined_impact_time_series = combined_impact_time_series.reset_index()
        combined_impact_time_series.set_index(["impact category"] + index_names, inplace=True)
        return combined_impact_time_series.sort_index()
 
    def get_impact_add_served_demand(self, impact_categories: list, reference_year: int, invest_years: list):
        """This methods gets the impact of add-served demand of a product in one or multiple impact categories for a specific year.
        
        If the impact category is "cost", the corresponding method Product._get_cost_non_served_demand is used.
        In all other cases the impacts of all ecoinvent processes defined by the process itself as the
        impacts of non-served demand, are multiplied with their corresponding weight factors and summed up together.
        """

        impact_add_served_demand_list = []
        impact_add_served_demand_combined = pd.DataFrame()
        
        for invest_year in invest_years:
            # Create empty dictionary for the operational impacts
            impact_add_served_demand = pd.DataFrame(data=units("0"), index=self._revenues_yearly_timeseries.index, columns=["product", "invest year"] + impact_categories)
            impact_add_served_demand["product"] = self.name
            impact_add_served_demand["invest year"] = invest_year

            # Loop over all requested impact categories
            for impact_category in impact_categories:
                # Check if impact category is "cost", if it is, use special methods from ProcessImpacts
                if impact_category == "cost":
                    impact_add_served_demand[impact_category] = self._get_revenues_add_served_demand(
                        reference_year, invest_year)

                # If impact category is not "cost" calculate the sum of the impacts of all invest processes
                else:
                    impact_add_served_demand[impact_category] = EcoinventImpacts._recursively_get_impact(self._processes_add_served_demand, impact_category, invest_year, invest_year)

            impact_add_served_demand.reset_index(inplace=True)
            impact_add_served_demand.set_index(["product", "invest year", "time slice"], inplace=True)
            impact_add_served_demand_list.append(impact_add_served_demand)

        impact_add_served_demand_combined = impact_add_served_demand_combined.append(other=impact_add_served_demand_list, verify_integrity=True, sort=True)

        return impact_add_served_demand_combined

    @staticmethod
    def get_combined_impact_add_served_demand():
        """Returns the combined impact time series for all products and years."""

        combined_impact_time_series = pd.DataFrame()
        impact_time_series_list = []
        for product in progress.bar(Product.products):
            impact_time_series_list.append(product.get_impact_add_served_demand(ImpactCategory.get_list_of_names_of_active_impact_categories(), min(ProcessImpacts.invest_years), ProcessImpacts.invest_years))
        
        combined_impact_time_series = combined_impact_time_series.append(other=impact_time_series_list, verify_integrity=True, sort=True)
        index_names = combined_impact_time_series.index.names
        combined_impact_time_series = combined_impact_time_series.stack()
        combined_impact_time_series.index.names = index_names + ["impact category"]
        combined_impact_time_series = combined_impact_time_series.reset_index()
        combined_impact_time_series.set_index(["impact category"] + index_names, inplace=True)
        return combined_impact_time_series.sort_index()

    def get_nodal_demand_time_series(self, year:int):
        """Returns the absolute nodal demand time series."""

        df = self._demand_nodal_timeseries.apply((lambda row: row * self._demand_nodal_average[str(year)].values), axis=1)
        df["product"] = self.name
        df["invest year"] = year
        df.reset_index(inplace=True)
        df.set_index(["product", "invest year", "time slice"], inplace=True)
        df.columns = [int(column) for column in df.columns]

        return df

    @staticmethod
    def get_combined_demand_time_series():
        """Returns the combined demand time series for all products and years."""

        logging.info("Get combined demand time series")

        combined_demand_time_series = pd.DataFrame()
        demand_time_series_list = []
        for product in progress.bar(Product.products):
            for year in ProcessImpacts.invest_years:
                demand_time_series_list.append(product.get_nodal_demand_time_series(year))
        
        combined_demand_time_series = combined_demand_time_series.append(other=demand_time_series_list, verify_integrity=True, sort=True)
        combined_demand_time_series.fillna(0, inplace=True)
        index_names = combined_demand_time_series.index.names
        combined_demand_time_series = combined_demand_time_series.stack()
        combined_demand_time_series.index.names = index_names + ["node"]

        return combined_demand_time_series.sort_index()

    def get_required_total_secured_capacity(self, invest_years: list):
        """Return the required secured capacity in the whole grid for product in a specific year."""
        
        required_total_secured_capacity = self._required_total_secured_capacity.reindex(index=[0], columns=[str(year) for year in invest_years])
        required_total_secured_capacity["product"] = self.name
        required_total_secured_capacity.set_index(["product"], inplace=True)
        required_total_secured_capacity.columns = [int(column) for column in required_total_secured_capacity.columns]

        return required_total_secured_capacity
    
    @staticmethod
    def get_combined_required_total_secured_capacity():
        """Returns the combined required total secured capacity for all products and years."""

        logging.info("Get combined required total secured capacity")

        combined_required_total_secured_capacity = pd.DataFrame()
        required_total_secured_capacity_list = []
        for product in progress.bar(Product.products):
            required_total_secured_capacity_list.append(product.get_required_total_secured_capacity(ProcessImpacts.invest_years))
        
        combined_required_total_secured_capacity = combined_required_total_secured_capacity.append(other=required_total_secured_capacity_list, verify_integrity=True, sort=True)
        combined_required_total_secured_capacity.fillna(0, inplace=True)
        index_names = combined_required_total_secured_capacity.index.names
        combined_required_total_secured_capacity = combined_required_total_secured_capacity.stack()
        combined_required_total_secured_capacity.index.names = index_names + ["invest year"]

        return combined_required_total_secured_capacity.sort_index()

    def get_required_nodal_secured_capacity(self, nodes: list, invest_years: list):
        """Return the required secured capacity in the whole grid for product in a specific year."""
        
        required_average_nodal_secured_capacity = self._required_average_nodal_secured_capacity.reindex(index=[0], columns=[str(year) for year in invest_years])
        required_relative_nodal_secured_capacity = self._required_relative_nodal_secured_capacity.reindex(index=nodes, columns=["value"])

        required_nodal_secured_capacity = pd.DataFrame(required_relative_nodal_secured_capacity.values * required_average_nodal_secured_capacity.values)
        required_nodal_secured_capacity.index = required_relative_nodal_secured_capacity.index
        required_nodal_secured_capacity.columns = invest_years
        required_nodal_secured_capacity["product"] = self.name
        required_nodal_secured_capacity.reset_index(inplace=True)
        required_nodal_secured_capacity.set_index(["product", "node"], inplace=True)
        required_nodal_secured_capacity.columns = [int(column) for column in required_nodal_secured_capacity.columns]

        return required_nodal_secured_capacity
    
    @staticmethod
    def get_combined_required_nodal_secured_capacity():
        """Returns the combined required nodal secured capacity for all products and years."""

        logging.info("Get combined required nodal secured capacity")

        combined_required_nodal_secured_capacity = pd.DataFrame()
        required_nodal_secured_capacity_list = []
        for product in progress.bar(Product.products):
            required_nodal_secured_capacity_list.append(product.get_required_nodal_secured_capacity(Grid.selected().get_list_of_node_ids(), ProcessImpacts.invest_years))
        
        combined_required_nodal_secured_capacity = combined_required_nodal_secured_capacity.append(other=required_nodal_secured_capacity_list, verify_integrity=True, sort=True)
        combined_required_nodal_secured_capacity.fillna(0, inplace=True)
        index_names = combined_required_nodal_secured_capacity.index.names
        combined_required_nodal_secured_capacity = combined_required_nodal_secured_capacity.stack()
        combined_required_nodal_secured_capacity.index.names = index_names + ["invest year"]

        return combined_required_nodal_secured_capacity.sort_index()


class ImpactCategory(object):
    """This class is used to manage impact categories, their corresponding limits and costs of overshoot."""

    impact_categories = []
    FRAMEWORK = "ReCiPe Midpoint (H)"
    MANUAL_IMPACT_CATEGORY_SELECTION = []

    def __init__(self, impact_category_path: Path):
        """See class documentation above for more information on the initialization."""

        logging.debug("Initialize a new instance of ImpactCategory.")
        
        if impact_category_path == None:
            # Do things if None has been given as path.
            raise NotImplementedError
        else:
            # Check if product directory exists
            if impact_category_path.is_dir():
                # Get name by directory name
                self.name = str(impact_category_path.name).replace("_", " ")
                # Get properties
                self._properties = pd.read_csv(impact_category_path / "properties.csv", float_precision="high", usecols=["property", "value"], index_col=["property"])
                # Get name in ecoinvent
                self.ecoinvent_name = self._properties.at["ecoinvent name", "value"]
                # Load the objective factor of the impact category
                self._objective_factor_impact = pd.read_csv(
                    impact_category_path / "objective_factor_impact.csv", float_precision="high", index_col="phase", usecols=(lambda column: column != "comments"), encoding= 'unicode_escape')
                # Fill empty value with 0 as there should be no impact on the objective then
                self._objective_factor_impact.fillna(0, inplace=True)
                # Unitize dataframe
                self._objective_factor_impact = secmod.helpers.unitize_dataframe(self._objective_factor_impact, units)
                # Load the objective factor of limit overshoots of the impact category
                self._objective_factor_impact_overshoot = pd.read_csv(
                    impact_category_path / "objective_factor_impact_overshot.csv", float_precision="high", usecols=(lambda column: column != "comments"), encoding= 'unicode_escape')
                # Fill empty value with 0 as there should be no impact on the objective then
                self._objective_factor_impact_overshoot.fillna(0, inplace=True)
                # Unitize dataframe
                self._objective_factor_impact_overshoot = secmod.helpers.unitize_dataframe(self._objective_factor_impact_overshoot, units)
                
                # Initialize dictionaries for results to be loaded in
                result_total_nodal_impact       = {}
                result_total_impact             = {}
                result_operational_nodal_impact = {}
                result_operational_impact       = {}
                result_invest_nodal_impact      = {}
                result_invest_impact            = {}
                
                # Check if subdirectory for grid-dependent data exists
                if (impact_category_path / Grid.selected().name).is_dir():
                    # Load the objective factor of the impact category
                    self._operational_nodal_impact_limits = pd.read_csv(
                        impact_category_path / Grid.selected().name / "operational_nodal_impact_limits.csv", float_precision="high", usecols=(lambda column: column != "comments"), index_col="node", encoding= 'unicode_escape')
                    # Fill empty value with 0 as there should be no impact on the objective then
                    self._operational_nodal_impact_limits.fillna(float("inf"), inplace=True)
                    # Unitize dataframe
                    self._operational_nodal_impact_limits = secmod.helpers.unitize_dataframe(self._operational_nodal_impact_limits, units)

                    # Load the objective factor of the impact category
                    self._operational_impact_limits = pd.read_csv(
                        impact_category_path / Grid.selected().name / "operational_impact_limits.csv", float_precision="high", usecols=(lambda column: column != "comments"), encoding= 'unicode_escape')
                    # Fill empty value with 0 as there should be no impact on the objective then
                    self._operational_impact_limits.fillna(float("inf"), inplace=True)
                    # Unitize dataframe
                    self._operational_impact_limits = secmod.helpers.unitize_dataframe(self._operational_impact_limits, units)

                    # Load the objective factor of the impact category
                    self._invest_nodal_impact_limits = pd.read_csv(
                        impact_category_path / Grid.selected().name / "invest_nodal_impact_limits.csv", float_precision="high", usecols=(lambda column: column != "comments"), index_col="node", encoding= 'unicode_escape')
                    # Fill empty value with 0 as there should be no impact on the objective then
                    self._invest_nodal_impact_limits.fillna(float("inf"), inplace=True)
                    # Unitize dataframe
                    self._invest_nodal_impact_limits = secmod.helpers.unitize_dataframe(self._invest_nodal_impact_limits, units)

                    # Load the objective factor of the impact category
                    self._invest_impact_limits = pd.read_csv(
                        impact_category_path / Grid.selected().name / "invest_impact_limits.csv", float_precision="high", usecols=(lambda column: column != "comments"), encoding= 'unicode_escape')
                    # Fill empty value with 0 as there should be no impact on the objective then
                    self._invest_impact_limits.fillna(float("inf"), inplace=True)
                    # Unitize dataframe
                    self._invest_impact_limits = secmod.helpers.unitize_dataframe(self._invest_impact_limits, units)

                    # Load the objective factor of the impact category
                    self._total_nodal_impact_limits = pd.read_csv(
                        impact_category_path / Grid.selected().name / "total_nodal_impact_limits.csv", float_precision="high", usecols=(lambda column: column != "comments"), index_col="node", encoding= 'unicode_escape')
                    # Fill empty value with 0 as there should be no impact on the objective then
                    self._total_nodal_impact_limits.fillna(float("inf"), inplace=True)
                    # Unitize dataframe
                    self._total_nodal_impact_limits = secmod.helpers.unitize_dataframe(self._total_nodal_impact_limits, units)

                    # Load the objective factor of the impact category
                    self._total_impact_limits = pd.read_csv(
                        impact_category_path / Grid.selected().name / "total_impact_limits.csv", float_precision="high", usecols=(lambda column: column != "comments"), encoding= 'unicode_escape')
                    # Fill empty value with 0 as there should be no impact on the objective then
                    self._total_impact_limits.fillna(float("inf"), inplace=True)
                    # Unitize dataframe
                    self._total_impact_limits = secmod.helpers.unitize_dataframe(self._total_impact_limits, units)

                else:
                    logging.warning("No grid dependent data found for impact category '{0}' in grid '{1}'! .".format(
                        self._name, Grid.selected().name))
                    # Create empty DataFrames, if no grid-dependent data is provided
                    self._operational_nodal_impact_limits = pd.DataFrame(index=Grid.selected().nodes.index.tolist(), data=float("inf"), columns=ProcessImpacts.invest_years)
                    self._operational_impact_limits = pd.DataFrame(index=[0], data=float("inf"), columns=ProcessImpacts.invest_years)
                    self._invest_nodal_impact_limits = pd.DataFrame(index=Grid.selected().nodes.index.tolist(), data=float("inf"), columns=ProcessImpacts.invest_years)
                    self._invest_impact_limits = pd.DataFrame(index=[0], data=float("inf"), columns=ProcessImpacts.invest_years)
                    self._total_nodal_impact_limits = pd.DataFrame(index=Grid.selected().nodes.index.tolist(), data=float("inf"), columns=ProcessImpacts.invest_years)
                    self._total_impact_limits = pd.DataFrame(index=[0], data=float("inf"), columns=ProcessImpacts.invest_years)
                    
            else:
                raise NotADirectoryError(
                    "Path {0} is not a directory. Please provide a path to the directory of the product.".format(str(impact_category_path)))
        # Append product instance to static list of products
        ImpactCategory.impact_categories.append(self)

    @property
    def name(self):
        """This property contains a unique name for the impact categoy.

        Getter:
            Gets the value of this property.

            Returns:
                Name of the impact categoy as string.

        Setter:
            Sets the property to the provided name, if it isn't already used by another impact categoy.

            Raises:
                ValueError: If the name is already used by another impact categoy.

        Type:
            str
        """

        return self._name

    @name.setter
    def name(self, name):
        """See property docstring above for setter information."""

        # If the provided name is equal to the current name, do nothing
        if hasattr(self, "_name"):
            if self.name == name:
                return
        # Check if any other defined grid has the provided name
        for impact_category in ImpactCategory.impact_categories:
            if impact_category.name == name:
                raise ValueError(
                    "There is already an impact category with the name '{0}'. Please choose another name.".format(name))
        self._name = name

    @property
    def ecoinvent_name(self):
        """This property contains a unique name for the impact category, which is used by ecoinvent.

        Getter:
            Gets the value of this property.

            Returns:
                ecoinvent name of the impact category as string.

        Setter:
            Sets the property to the provided ecoinvent name, if it isn't already used by another impact category.

            Raises:
                ValueError: If the ecoinvent name is already used by another impact category.

        Type:
            str
        """

        return self._ecoinvent_name

    @ecoinvent_name.setter
    def ecoinvent_name(self, ecoinvent_name):
        """See property docstring above for setter information."""

        # If the provided ecoinvent_name is equal to the current ecoinvent_name, do nothing
        if hasattr(self, "_ecoinvent_name"):
            if self.ecoinvent_name == ecoinvent_name:
                return
        # Check if any other defined grid has the provided ecoinvent_name
        for impact_category in ImpactCategory.impact_categories:
            if impact_category.ecoinvent_name == ecoinvent_name:
                raise ValueError(
                    "There is already an impact category with the ecoinvent_name '{0}'. Please choose another ecoinvent_name.".format(ecoinvent_name))
        self._ecoinvent_name = ecoinvent_name

    @classmethod
    def load_impact_categories_from_directory(cls, impact_categories_path: Path):
        """Loads all impact categories of a specific class into their class."""

        logging.info("Load impact categories of class '{0}".format(str(cls)))

        # Get a list of all files and folders in the directory
        impact_category_paths = os.listdir(impact_categories_path)

        # Iterate through the members of the impact_category directory
        for impact_category in progress.bar(impact_category_paths):
            # Check if the file ending is "csv"
            if (impact_categories_path / impact_category).is_dir():
                # If the file is csv, read it in as a dataframe
                cls(impact_categories_path / impact_category)

    @staticmethod
    def get_list_of_active_impact_categories():
        """Returns a list of the all active impact categories in the currently selected framework."""
        
        # Create empty list
        impact_categories = []

        if len(ImpactCategory.MANUAL_IMPACT_CATEGORY_SELECTION) != 0:
            for impact_category in ImpactCategory.impact_categories:
                # If it includes the name of the framework append it to the list
                if (impact_category._ecoinvent_name in ImpactCategory.MANUAL_IMPACT_CATEGORY_SELECTION) or (impact_category._ecoinvent_name == "cost"):
                    impact_categories.append(impact_category)
        else:
            # Check for all instances of ImpactCategory whether its ecoinvent name includes the name of the framework
            for impact_category in ImpactCategory.impact_categories:
                # If it includes the name of the framework append it to the list
                if (ImpactCategory.FRAMEWORK in impact_category._ecoinvent_name) or (impact_category._ecoinvent_name == "cost"):
                    impact_categories.append(impact_category)
            
        # Return the list of active impact categories
        return impact_categories

    @staticmethod
    def get_list_of_names_of_active_impact_categories():
        """Returns a list of the ecoinvent names of all impact categories active in the currently selected framework."""

        # Create list of names
        ecoinvent_names = [impact_category._ecoinvent_name for impact_category in ImpactCategory.get_list_of_active_impact_categories()]

        return ecoinvent_names

    def _get_operational_nodal_impact_limits(self, nodes: list, invest_years: list):
        """Return the operational nodal impact limit for an impact category in a specific year."""
        
        operational_nodal_impact_limits = self._operational_nodal_impact_limits.reindex(index=nodes, columns=[str(year) for year in invest_years])
        
        operational_nodal_impact_limits.columns = invest_years
        operational_nodal_impact_limits["impact category"] = self._ecoinvent_name
        operational_nodal_impact_limits.reset_index(inplace=True)
        operational_nodal_impact_limits.set_index(["node", "impact category"], inplace=True)
        operational_nodal_impact_limits.columns = [int(column) for column in operational_nodal_impact_limits.columns]

        return operational_nodal_impact_limits
    
    @staticmethod
    def get_combined_operational_nodal_impact_limits():
        """Returns the combined operational nodal impact limit for all impact categories and years."""

        logging.info("Get combined operational nodal impact limit")

        combined_operational_nodal_impact_limits = pd.DataFrame()
        operational_nodal_impact_limits_list = []
        for impact_category in progress.bar(ImpactCategory.get_list_of_active_impact_categories()):
            operational_nodal_impact_limits_list.append(impact_category._get_operational_nodal_impact_limits(Grid.selected().get_list_of_node_ids(), ProcessImpacts.invest_years))
        
        combined_operational_nodal_impact_limits = combined_operational_nodal_impact_limits.append(other=operational_nodal_impact_limits_list, verify_integrity=True, sort=True)
        combined_operational_nodal_impact_limits.fillna(float("inf"), inplace=True)
        index_names = combined_operational_nodal_impact_limits.index.names
        combined_operational_nodal_impact_limits = combined_operational_nodal_impact_limits.stack()
        combined_operational_nodal_impact_limits.index.names = index_names + ["invest year"]

        return combined_operational_nodal_impact_limits.sort_index()

    def _get_invest_nodal_impact_limits(self, nodes: list, invest_years: list):
        """Return the invest nodal impact limit for an impact category in a specific year."""
        
        invest_nodal_impact_limits = self._invest_nodal_impact_limits.reindex(index=nodes, columns=[str(year) for year in invest_years])
        
        invest_nodal_impact_limits.columns = invest_years
        invest_nodal_impact_limits["impact category"] = self._ecoinvent_name
        invest_nodal_impact_limits.reset_index(inplace=True)
        invest_nodal_impact_limits.set_index(["node", "impact category"], inplace=True)
        invest_nodal_impact_limits.columns = [int(column) for column in invest_nodal_impact_limits.columns]

        return invest_nodal_impact_limits
    
    @staticmethod
    def get_combined_invest_nodal_impact_limits():
        """Returns the combined invest nodal impact limit for all impact categories and years."""

        logging.info("Get combined invest nodal impact limit")

        combined_invest_nodal_impact_limits = pd.DataFrame()
        invest_nodal_impact_limits_list = []
        for impact_category in progress.bar(ImpactCategory.get_list_of_active_impact_categories()):
            invest_nodal_impact_limits_list.append(impact_category._get_invest_nodal_impact_limits(Grid.selected().get_list_of_node_ids(), ProcessImpacts.invest_years))
        
        combined_invest_nodal_impact_limits = combined_invest_nodal_impact_limits.append(other=invest_nodal_impact_limits_list, verify_integrity=True, sort=True)
        combined_invest_nodal_impact_limits.fillna(float("inf"), inplace=True)
        index_names = combined_invest_nodal_impact_limits.index.names
        combined_invest_nodal_impact_limits = combined_invest_nodal_impact_limits.stack()
        combined_invest_nodal_impact_limits.index.names = index_names + ["invest year"]

        return combined_invest_nodal_impact_limits.sort_index()

    def _get_total_nodal_impact_limits(self, nodes: list, invest_years: list):
        """Return the total nodal impact limit for an impact category in a specific year."""
        
        total_nodal_impact_limits = self._total_nodal_impact_limits.reindex(index=nodes, columns=[str(year) for year in invest_years])
        
        total_nodal_impact_limits.columns = invest_years
        total_nodal_impact_limits["impact category"] = self._ecoinvent_name
        total_nodal_impact_limits.reset_index(inplace=True)
        total_nodal_impact_limits.set_index(["node", "impact category"], inplace=True)
        total_nodal_impact_limits.columns = [int(column) for column in total_nodal_impact_limits.columns]

        return total_nodal_impact_limits
    
    @staticmethod
    def get_combined_total_nodal_impact_limits():
        """Returns the combined total nodal impact limit for all impact categories and years."""

        logging.info("Get combined total nodal impact limit")

        combined_total_nodal_impact_limits = pd.DataFrame()
        total_nodal_impact_limits_list = []
        for impact_category in progress.bar(ImpactCategory.get_list_of_active_impact_categories()):
            total_nodal_impact_limits_list.append(impact_category._get_total_nodal_impact_limits(Grid.selected().get_list_of_node_ids(), ProcessImpacts.invest_years))
        
        combined_total_nodal_impact_limits = combined_total_nodal_impact_limits.append(other=total_nodal_impact_limits_list, verify_integrity=True, sort=True)
        combined_total_nodal_impact_limits.fillna(float("inf"), inplace=True)
        index_names = combined_total_nodal_impact_limits.index.names
        combined_total_nodal_impact_limits = combined_total_nodal_impact_limits.stack()
        combined_total_nodal_impact_limits.index.names = index_names + ["invest year"]

        return combined_total_nodal_impact_limits.sort_index()

    def _get_operational_impact_limits(self, invest_years: list):
        """Return the operational impact limit for an impact category in a specific year."""
        
        operational_impact_limits = self._operational_impact_limits.reindex(index=[0], columns=[str(year) for year in invest_years])
        
        operational_impact_limits.columns = invest_years
        operational_impact_limits["impact category"] = self._ecoinvent_name
        operational_impact_limits.set_index(["impact category"], inplace=True)
        operational_impact_limits.columns = [int(column) for column in operational_impact_limits.columns]

        return operational_impact_limits
    
    @staticmethod
    def get_combined_operational_impact_limits():
        """Returns the combined operational impact limit for all impact categories and years."""

        logging.info("Get combined operational impact limit")

        combined_operational_impact_limits = pd.DataFrame()
        operational_impact_limits_list = []
        for impact_category in progress.bar(ImpactCategory.get_list_of_active_impact_categories()):
            operational_impact_limits_list.append(impact_category._get_operational_impact_limits(ProcessImpacts.invest_years))
        
        combined_operational_impact_limits = combined_operational_impact_limits.append(other=operational_impact_limits_list, verify_integrity=True, sort=True)
        combined_operational_impact_limits.fillna(float("inf"), inplace=True)
        index_names = combined_operational_impact_limits.index.names
        combined_operational_impact_limits = combined_operational_impact_limits.stack()
        combined_operational_impact_limits.index.names = index_names + ["invest year"]

        return combined_operational_impact_limits.sort_index()

    def _get_invest_impact_limits(self, invest_years: list):
        """Return the invest impact limit for an impact category in a specific year."""
        
        invest_impact_limits = self._invest_impact_limits.reindex(index=[0], columns=[str(year) for year in invest_years])
        
        invest_impact_limits.columns = invest_years
        invest_impact_limits["impact category"] = self._ecoinvent_name
        invest_impact_limits.set_index(["impact category"], inplace=True)
        invest_impact_limits.columns = [int(column) for column in invest_impact_limits.columns]

        return invest_impact_limits
    
    @staticmethod
    def get_combined_invest_impact_limits():
        """Returns the combined invest impact limit for all impact categories and years."""

        logging.info("Get combined invest impact limit")

        combined_invest_impact_limits = pd.DataFrame()
        invest_impact_limits_list = []
        for impact_category in progress.bar(ImpactCategory.get_list_of_active_impact_categories()):
            invest_impact_limits_list.append(impact_category._get_invest_impact_limits(ProcessImpacts.invest_years))
        
        combined_invest_impact_limits = combined_invest_impact_limits.append(other=invest_impact_limits_list, verify_integrity=True, sort=True)
        combined_invest_impact_limits.fillna(float("inf"), inplace=True)
        index_names = combined_invest_impact_limits.index.names
        combined_invest_impact_limits = combined_invest_impact_limits.stack()
        combined_invest_impact_limits.index.names = index_names + ["invest year"]

        return combined_invest_impact_limits.sort_index()

    def _get_total_impact_limits(self, invest_years: list):
        """Return the total impact limit for an impact category in a specific year."""
        
        total_impact_limits = self._total_impact_limits.reindex(index=[0], columns=[str(year) for year in invest_years])
        
        total_impact_limits.columns = invest_years
        total_impact_limits["impact category"] = self._ecoinvent_name
        total_impact_limits.set_index(["impact category"], inplace=True)
        total_impact_limits.columns = [int(column) for column in total_impact_limits.columns]

        return total_impact_limits
    
    @staticmethod
    def get_combined_total_impact_limits():
        """Returns the combined total impact limit for all impact categories and years."""

        logging.info("Get combined total impact limit")

        combined_total_impact_limits = pd.DataFrame()
        total_impact_limits_list = []
        for impact_category in progress.bar(ImpactCategory.get_list_of_active_impact_categories()):
            total_impact_limits_list.append(impact_category._get_total_impact_limits(ProcessImpacts.invest_years))
        
        combined_total_impact_limits = combined_total_impact_limits.append(other=total_impact_limits_list, verify_integrity=True, sort=True)
        combined_total_impact_limits.fillna(float("inf"), inplace=True)
        index_names = combined_total_impact_limits.index.names
        combined_total_impact_limits = combined_total_impact_limits.stack()
        combined_total_impact_limits.index.names = index_names + ["invest year"]

        return combined_total_impact_limits.sort_index()

    def _get_objective_factor_impact(self, invest_years: list):
        """Return the total impact limit for an impact category in a specific year."""
        
        objective_factor_impact = self._objective_factor_impact.reindex(index=["invest", "operation"], columns=[str(year) for year in invest_years])
        
        objective_factor_impact.columns = invest_years
        objective_factor_impact["impact category"] = self._ecoinvent_name
        objective_factor_impact.reset_index(inplace=True)
        objective_factor_impact.set_index(["phase", "impact category"], inplace=True)
        objective_factor_impact.columns = [int(column) for column in objective_factor_impact.columns]

        return objective_factor_impact
    
    @staticmethod
    def get_combined_objective_factor_impact():
        """Returns the combined total impact limit for all impact categories and years."""

        logging.info("Get combined objective factors for impacts")

        combined_objective_factor_impact = pd.DataFrame()
        objective_factor_impact_list = []
        for impact_category in progress.bar(ImpactCategory.get_list_of_active_impact_categories()):
            objective_factor_impact_list.append(impact_category._get_objective_factor_impact(ProcessImpacts.invest_years))
        
        combined_objective_factor_impact = combined_objective_factor_impact.append(other=objective_factor_impact_list, verify_integrity=True, sort=True)
        combined_objective_factor_impact.fillna(float("inf"), inplace=True)
        index_names = combined_objective_factor_impact.index.names
        combined_objective_factor_impact = combined_objective_factor_impact.stack()
        combined_objective_factor_impact.index.names = index_names + ["invest year"]

        return combined_objective_factor_impact.sort_index()

    def _get_objective_factor_impact_overshoot(self, invest_years: list):
        """Return the objective factors for impact overshoots for an impact category in a specific year."""
        
        objective_factor_impact_overshoot = self._objective_factor_impact_overshoot.reindex(index=[0], columns=[str(year) for year in invest_years])
        
        objective_factor_impact_overshoot.columns = invest_years
        objective_factor_impact_overshoot["impact category"] = self._ecoinvent_name
        objective_factor_impact_overshoot.set_index(["impact category"], inplace=True)
        objective_factor_impact_overshoot.columns = [int(column) for column in objective_factor_impact_overshoot.columns]

        return objective_factor_impact_overshoot
    
    @staticmethod
    def get_combined_objective_factor_impact_overshoot():
        """Returns the combined objective factors for impact overshoots for all impact categories and years."""

        logging.info("Get combined objective factors for impact overshoots")

        combined_objective_factor_impact_overshoot = pd.DataFrame()
        objective_factor_impact_overshoot_list = []
        for impact_category in progress.bar(ImpactCategory.get_list_of_active_impact_categories()):
            objective_factor_impact_overshoot_list.append(impact_category._get_objective_factor_impact_overshoot(ProcessImpacts.invest_years))
        
        combined_objective_factor_impact_overshoot = combined_objective_factor_impact_overshoot.append(other=objective_factor_impact_overshoot_list, verify_integrity=True, sort=True)
        combined_objective_factor_impact_overshoot.fillna(float("inf"), inplace=True)
        index_names = combined_objective_factor_impact_overshoot.index.names
        combined_objective_factor_impact_overshoot = combined_objective_factor_impact_overshoot.stack()
        combined_objective_factor_impact_overshoot.index.names = index_names + ["invest year"]

        return combined_objective_factor_impact_overshoot.sort_index()
    

class ProcessImpacts(abc.ABC):
    """This class is used as an interface for process impacts.

    In this class two abstract methods are defined which need to be implemented
    by subclasses. Therefore this class acts as an interface which is implemented by
    the inheriting classes. This way it is assured that the methods used to get the
    impacts of investment and operation can be called in all inheriting classes.

    Furthermore it includes static variables for the economic time period and assumed
    interest rate. And since costs are special impacts, which all processes must have,
    the instance variables for costs are defined in this class. Furthermore the 
    initialization of this class imports the costs from a processes directory, if provided.
    Methods to get costs of investment and operation are implemented as well.

    Args:
        process_path (Path): A path of the directory of an existing process definition, which
                includes a CSV-file with data about the costs of the process.

    Attributes:
        interest_rate (float): The interest rate used for economic impacts as share of 1.

        economic_period (pint quantity): The time period used for the calculation of all
            annualized impacts.

    Raises:
        FileNotFoundError: If no file "costs.csv" can be found in the process directory.
    """

    IMPACT_SOURCES = {None: ["operation", "invest"]}

    # Set static variables used for all impacts
    # These values are not used, but instead replaced by values from the config.py
    # interest rate in share of 1
    interest_rate = 0.05
    # time period used to calculate the annualized impacts
    economic_period = 30 * units.year
    # invest years
    invest_years = [2016, 2020, 2025, 2030, 2035, 2040, 2045, 2050]
    # construction year (will be determined automatically, if not set)
    construction_years = []
    # list of all processes
    processes = []

    def __init__(self, process_path: Path = None):
        """See class documentation above for more information on the initialization."""

        logging.debug("Initialize a new instance of ProcessImpact.")

        # Check if a process path is provided and create an empty costs table, if not
        if process_path == None:
            self.name = ""
            self._costs = pd.DataFrame(data=np.nan, columns=["unit"] + ProcessImpacts.invest_years + [
                                       "comments"], index=["invest", "operation", "maintenance_absolute", "maintenace_relative_invest"])
            self._costs.index.name = "phase"
            self._lifetime_duration = pd.DataFrame(index=[1], data=np.nan, columns=[
                                                   "unit"] + ProcessImpacts.invest_years + ["comments"])
        else:
            if process_path.is_dir():
                self.name = str(process_path.name).replace("_", " ")
                self._costs = pd.read_csv(
                    process_path / "costs.csv", index_col="phase", float_precision="high", usecols=(lambda column: column != "comments"), encoding= 'unicode_escape')
                self._lifetime_duration = pd.read_csv(
                    process_path / "lifetime_duration.csv", float_precision="high", usecols=(lambda column: column != "comments"), encoding= 'unicode_escape')
            else:
                raise NotADirectoryError(
                    "Path {0} is not a directory. Please provide a path to the directory of the process.".format(str(process_path)))

    @property
    def name(self):
        """This property contains a unique name for the grid.

        Getter:
            Gets the value of this property.

            Returns:
                Name of the grid as string.

        Setter:
            Sets the property to the provided name, if it isn't already used by another grid.

            Raises:
                ValueError: If the name is already used by another grid.

        Type:
            str
        """

        return self._name

    @name.setter
    def name(self, name):
        """See property docstring above for setter information."""

        # If the provided name is equal to the current name, do nothing
        if hasattr(self, "_name"):
            if self.name == name:
                return
        # Check if any other defined grid has the provided name
        for process in Process.processes:
            if process.name == name:
                raise ValueError(
                    "There is already a process with the name '{0}'. Please choose another name.".format(name))
        self._name = name

    @abc.abstractmethod
    def get_impact_invest(self, impact_categories: list, construction_year: int, invest_year: int):
        """This method gets the annual investment impacts of a process

        Args:
            impacts_categories (list): List of strings, which define for which impact categories
                the yearly investment impacts shall be returned

            construction_year (int): Year of construction of an instance of this process

            invest_year (int): Year for which the impact is calculated    
        """

        raise NotImplementedError

    @abc.abstractmethod
    def get_impact_operation(self, impact_categories: list, construction_year: int, reference_year: int, invest_year: int):
        """This method gets the operational impacts of a process

        Args:
            impacts_categories (list): List of strings, which define for which impact categories
                the yearly investment impacts shall be returned

            construction_year (int): Year of construction of an instance of this process

            invest_year (int): Year for which the impact is calculated
        """

        raise NotImplementedError

    @staticmethod
    def _get_investment_period_duration(invest_year: int):
        """Gets the duration of the period between this and the next invest year.

        If it is the last year in the list of invest years, a period duration of
        one year is returned.

        Args:
            invest_year (int): A year, e.g. 2020.
        """

        # Get the index of the selected invest year in the list of invest years
        index = config.invest_years.index(invest_year)
        # Get the number of invest years
        number_of_invest_years = len(config.invest_years)
        # Check if invest year is not the last invest year
        if index < (number_of_invest_years - 1):
            # if not the last year, calculate the duration to the next invest year
            return (config.invest_years[index + 1] - invest_year) * units.year
        # Check if there are more than one invest year
        elif number_of_invest_years > 1:
            # if it is the last year, but not the only, take the previous duration
            return (invest_year - config.invest_years[index - 1]) * units.year
        else:
            # if it is the only year, calculate only for that year
            return 1 * units.year

    def _get_cost_invest(self, construction_year: int, reference_year: int, invest_year: int):
        """This method gets the annual investment costs of a process

        Calculates the present value of the selected impact year and its investment period
        in the reference year for a process instance build in the construction year.

        Args:
            construction_year (int): Year of construction, e.g. 2016. Earliest year is used,
                if construction year is earlier than the earliest year in the data.

            reference_year (int): Reference year of the optimized time horizon, e.g. 2020

            invest_year (int): An investment year of the optimized time horizon, e.g. 2025
        """

        # Determine whether lifetime duration or economic period should be used, based on which is smaller
        full_annuity_period = min(ProcessImpacts.economic_period,
                                  units(str(self._lifetime_duration.at[0, str(max(construction_year, int(min(self._lifetime_duration.columns))))]) + str(self._lifetime_duration.at[0, "unit"])))
        # Calculate the annuity present value factor for the full annuity period
        full_annuity_present_value_factor = ((1+ProcessImpacts.interest_rate) ** (full_annuity_period / units.year) - 1) / (
            (1+ProcessImpacts.interest_rate) ** (full_annuity_period / units.year) * ProcessImpacts.interest_rate)
        
        # Determine the length of the current investment period
        investment_period_duration = ProcessImpacts._get_investment_period_duration(invest_year)
        
        # Calculate annuity present value factor for the current investment period
        investment_period_annuity_present_value_factor = ((1+ProcessImpacts.interest_rate) ** (investment_period_duration / units.year) - 1) / (
            (1+ProcessImpacts.interest_rate) ** (investment_period_duration / units.year) * ProcessImpacts.interest_rate)

        # Check if invest year is within the full annuity period after the construction
        # and therefore has investment and maintenance costs
        if (invest_year >= construction_year) and ((invest_year - construction_year) * units.year < full_annuity_period):
            return (
                (
                    # Get total invest costs
                    self._costs.at["invest", str(
                        max(construction_year, int(min(self._costs.columns))))]
                    # Multiply with the corresponding unit
                    * units(self._costs.at["invest", "unit"])
                    # divide it by the full annuity present value factor to get the invest cost annuity
                    / full_annuity_present_value_factor

                    # Add maintenance cost in relative share of invest costs
                    + self._costs.at["maintenance_relative_invest",
                                     str(max(construction_year, int(min(self._costs.columns))))]
                    # Multiplied with its unit
                    * units(self._costs.at["maintenance_relative_invest", "unit"])
                    # Multiplied with the total invest costs
                    * self._costs.at["invest", str(max(construction_year, int(min(self._costs.columns))))]
                    # Multiplied with the unit of the total invest costs
                    * units(self._costs.at["invest", "unit"])

                    # Add maintenance costs in absolute value
                    + self._costs.at["maintenance_absolute",
                                     str(max(construction_year, int(min(self._costs.columns))))]
                    # Multiplied with its unit
                    * units(self._costs.at["maintenance_absolute", "unit"])
                    
                # all together multiplied with the annuity present value factor of the current investment period
                ) * investment_period_annuity_present_value_factor

                # Discount from invest year to reference year
                / (1 + ProcessImpacts.interest_rate)**(invest_year - reference_year)
            )

        # Else check if invest year is still within the lifetime duration of the process instance
        # and therefore still has maintenance costs
        elif (invest_year >= construction_year) and (invest_year - construction_year) * units.year < units(str(self._lifetime_duration.at[0, str(max(construction_year, int(min(self._lifetime_duration.columns))))]) + str(self._lifetime_duration.at[0, "unit"])):
            return (
                (  # Get maintenance cost in relative share of invest costs
                    self._costs.at["maintenance_relative_invest", str(
                        max(construction_year, int(min(self._costs.columns))))]
                    # Multiplied with its unit
                    * units(self._costs.at["maintenance_relative_invest", "unit"])
                    # Multiplied with the total invest costs
                    * self._costs.at["invest", str(max(construction_year, int(min(self._costs.columns))))]
                    # Multiplied with the unit of the total invest costs
                    * units(self._costs.at["invest", "unit"])

                    # Add maintenance costs in absolute value
                    + self._costs.at["maintenance_absolute",
                                     str(max(construction_year, int(min(self._costs.columns))))]
                    # Multiplied with its unit
                    * units(self._costs.at["maintenance_absolute", "unit"])
                    
                # all together multiplied with the annuity present value factor of the current investment period
                ) * investment_period_annuity_present_value_factor

                # Discount from invest year to reference year
                / (1 + ProcessImpacts.interest_rate)**(invest_year - reference_year)
            )

        else:
            # If invest year is outside the lifetime duration of the process instance return zero costs
            return units("0")

    def _get_cost_operation(self, construction_year: int, reference_year: int, invest_year: int):
        """This method gets the annual investment costs of a process

        Calculates the present value of the selected impact year and its investment period
        in the reference year for a process instance build in the construction year.

        Args:
            construction_year (int): Year of construction, e.g. 2016. Earliest year is used,
                if construction year is earlier than the earliest year in the data.

            reference_year (int): Reference year of the optimized time horizon, e.g. 2020

            invest_year (int): An investment year of the optimized time horizon, e.g. 2025
        """

        # Determine the length of the current investment period
        investment_period_duration = ProcessImpacts._get_investment_period_duration(
            invest_year)
        
        # Calculate annuity present value factor for the current investment period
        investment_period_annuity_present_value_factor = ((1+ProcessImpacts.interest_rate) ** (investment_period_duration / units.year) - 1) / (
            (1+ProcessImpacts.interest_rate) ** (investment_period_duration / units.year) * ProcessImpacts.interest_rate)


        return (
            # Get operational costs per used capacity
            self._costs.at["operation", str(
                max(construction_year, int(min(self._costs.columns))))]
            # Multiplied with its unit
            * units(self._costs.at["operation", "unit"])
            
             # multiplied with the annuity present value factor of the current investment period
            * investment_period_annuity_present_value_factor

            # discounted to reference year
            / (1 + ProcessImpacts.interest_rate)**(invest_year - reference_year)
        )
    
    @classmethod
    def get_combined_impact_matrix(cls):
        """Method that is used to return the combined impact matrix of all process of a process category.

        """

        reference_year = min(cls.invest_years)

        if len(cls.construction_years) == 0:
                cls.setup_construction_years()
        
        combined_impact_matrix = pd.DataFrame()
        combined_impact_matrix_list = []
        for process in progress.bar(cls.processes):
            EcoinventImpacts._get_single_combined_impact_matrix(combined_impact_matrix_list, process, ImpactCategory.get_list_of_names_of_active_impact_categories(), cls.construction_years, reference_year, cls.invest_years)
        
        if len(combined_impact_matrix_list) != 0:
            combined_impact_matrix = combined_impact_matrix.append(other=combined_impact_matrix_list, verify_integrity=True, sort=True)

        index_names = combined_impact_matrix.index.names
        combined_impact_matrix = combined_impact_matrix.stack()
        combined_impact_matrix.index.names = index_names + ["impact category"]
        combined_impact_matrix = combined_impact_matrix.reset_index()
        combined_impact_matrix.set_index(["impact category"] + index_names, inplace=True)
        return combined_impact_matrix.sort_index()

    @staticmethod
    def _get_single_combined_impact_matrix(combined_impact_matrix_list, process, impact_categories, construction_years, reference_year, invest_years):
        """Returns impacts for component investment and operation"""
        combined_impact_matrix_list.append(copy.deepcopy(process.get_impact_invest(impact_categories, construction_years, reference_year, invest_years)))
        combined_impact_matrix_list.append(copy.deepcopy(process.get_impact_operation(impact_categories, construction_years, reference_year, invest_years)))

    def get_lifetime_duration(self, construction_years: list):
        """Return the lifetime duration of the process for a list of construction years"""
        # Unitize Dataframe
        unitized_lifetime = secmod.helpers.unitize_dataframe(self._lifetime_duration, units)

        lifetime_duration = unitized_lifetime.reindex(index=[0] , columns=[str(construction_year) for construction_year in construction_years])

        for construction_year in construction_years:
            if construction_year < int(min(unitized_lifetime.columns)):
                lifetime_duration[str(construction_year)] = lifetime_duration[min(unitized_lifetime.columns)]
            elif construction_year > int(max(unitized_lifetime.columns)):
                lifetime_duration[str(construction_year)] = lifetime_duration[max(unitized_lifetime.columns)]
            lifetime_duration[str(construction_year)] = lifetime_duration[str(construction_year)].apply(lambda row: row.magnitude)
        
        lifetime_duration.columns = [int(column) for column in lifetime_duration.columns]
        lifetime_duration["process"] = self.name
        lifetime_duration.set_index(["process"], inplace=True)

        return lifetime_duration

    @classmethod
    def get_combined_lifetime_duration(cls):
        """Returns the combined lifetime durations of all processes in the class using the construction years of the class."""

        if len(cls.construction_years) == 0:
                cls.setup_construction_years()

        combined_lifetime_duration = pd.DataFrame()
        combined_lifetime_duration_list = []
        for process in progress.bar(cls.processes):
            combined_lifetime_duration_list.append(process.get_lifetime_duration(cls.construction_years))

        if len(combined_lifetime_duration_list) != 0:
            combined_lifetime_duration = combined_lifetime_duration.append(other=combined_lifetime_duration_list, verify_integrity=True, sort=True)

        index_names = combined_lifetime_duration.index.names
        combined_lifetime_duration = combined_lifetime_duration.stack()
        combined_lifetime_duration.index.names = index_names + ["construction year"]
        combined_lifetime_duration = combined_lifetime_duration.reset_index()
        combined_lifetime_duration.set_index(index_names + ["construction year"], inplace=True)
        return combined_lifetime_duration.sort_index()
    
    @abc.abstractclassmethod
    def setup_construction_years(cls):
        """Abstract method to setup the construction years from existing capacity data."""

        raise NotImplementedError
    
    def setup_years(self):
        """"""

        raise NotImplementedError


class EcoinventImpacts(ProcessImpacts):
    """This class provides impacts based on a list of ecoinvent processes.

    An instance based on this class gets its impacts from the ecoinvent
    database. The impacts are calculated by adding a list of ecoinvent
    processes multiplied with a scaling factor, which are used to define
    the environmental impact of a process in SecMOD.

    Args:
        process_path (Path) = None: A path to the directory of an ecoinvent process.
            If no path is provided, an empty instance will be created.

    Raises:
        NotADirectoryError: If provided path is not a directory. 
    """

    # Static variable for the ecoinvent database to be loaded
    database = None
    # Static dictionary with the definitions of all subassemblies
    subassemblies = {}

    def __init__(self, process_path: Path = None):
        """See class documentation above for more information on the initialization."""

        logging.debug("Initialize a new instance of EcoinventImpacts.")

        # Call inherited constructors
        super(EcoinventImpacts, self).__init__(process_path)

        # Check if a path has been provided as parameter
        if process_path == None:
            # Create empty dataframes for invest ecoinvent processes, if no directory is provided
            self._processes_invest = pd.DataFrame(columns=["process_name", "unit"] + ProcessImpacts.invest_years + [
                "group", "comments", "product", "location", "activity", "amountBeforeRecyclingAndScaling", "recyclingRate", "EOLfactor", "scalingFactor"])
            # Create empty dataframes for operational ecoinvent processes, if no directory is provided
            self._processes_operation = pd.DataFrame(columns=["process_name", "unit"] + ProcessImpacts.invest_years + [
                "group", "comments", "product", "location", "activity", "amountBeforeRecyclingAndScaling", "recyclingRate", "EOLfactor", "scalingFactor"])
        else:
            # Read invest ecoinvent processes
            self._processes_invest = pd.read_csv(
                process_path / "ecoinvent_invest.csv", float_precision="high", usecols=(lambda column: column != "comments"), encoding= 'unicode_escape')
            # Read operational ecoinvent processes
            self._processes_operation = pd.read_csv(
                process_path / "ecoinvent_operation.csv", float_precision="high", usecols=(lambda column: column != "comments"), encoding= 'unicode_escape')
            
    @staticmethod
    def load_ecoinvent_database(database_path: Path, units_to_change_path: Path = None):
        """Loads the impacts of all ecoinvent processes from a multiindexed CSV-file.

        Additionally this methods loads new unit defintions to be used in pint and
        translates ecoinvent units to the necessary format to be used with pint.

        Furthermore the process names are edited to match previous naming logic from SecMOD 1.0.

        Args:
            database_path (Path): A path which includes the filename of the CSV-file.

            units_to_change_path (Path) = None: A path to a JSON-file which defines a dictionary of
                existing units and the units they shall be replaced with.

            new_pint_units (Path) = None: A path to a TXT-file which includes new unit definitions
                to be used as pint units. E.g. "car = []" or "mass_CO2_equivalent = [GWP100] = CO2_eq"
        """

        logging.info("Load ecoinvent database")
        # Load database from CSV file
        EcoinventImpacts.database = pd.read_csv(database_path, index_col=[
                                                "process", "impact category"], usecols=lambda x: "Unnamed" not in x, float_precision="high", encoding= 'unicode_escape')

        # Change units from ecoinvent to fit the defined pint units
        if units_to_change_path != None:
            logging.info("Translate ecoinvent units to pint format")
            EcoinventImpacts._translate_units_to_pint(units_to_change_path)

        # Get list of all process names
        process_names = list(EcoinventImpacts.database.index.levels[0])
        # Translate every single process name and save the new list to process_names
        process_names = [EcoinventImpacts._translate_process_name_to_ecoinvent_identifier(
            process_name) for process_name in process_names]
        # Set the process name index in the database to the translated process names
        EcoinventImpacts.database.index.set_levels(
            process_names, level=0, inplace=True)
        # Handle invest year not included in ecoinvent database
        ecoinvent_years = []
        columns = EcoinventImpacts.database.columns
        for column in columns:
            try: # check if column name can be converted to integer
                ecoinvent_years.append(int(column))
            except ValueError:
                continue
        ecoinvent_years.sort()
        earliest_ecoinvent_year = ecoinvent_years[0]    
        latest_ecoinvent_year = ecoinvent_years[-1]    
        for year in reversed(config.invest_years):
            if year > earliest_ecoinvent_year and year < latest_ecoinvent_year and year not in ecoinvent_years:
                # apply linear interpolation
                logging.info("Invest year {} not specified in ecoinvent database, applying linear interpolation".format(year))
                # find the next earlier and next later year in database
                earlier_year = earliest_ecoinvent_year
                for y in ecoinvent_years:
                    later_year = y
                    if y > year:
                        break # end for loop afer smaller year is found
                    else: 
                        earlier_year = y
                interpolated_values = EcoinventImpacts.database[str(earlier_year)] + (EcoinventImpacts.database[str(later_year)] - EcoinventImpacts.database[str(earlier_year)]) * (year-earlier_year) / (later_year-earlier_year)
                # insert column in database before the column of the later year
                EcoinventImpacts.database.insert(columns.get_loc(str(later_year)), str(year), interpolated_values)
            elif year > latest_ecoinvent_year:
                logging.info("Invest year {0} later than the latest year specified in ecoinvent data. Using data of {1}".format(year,latest_ecoinvent_year))
                EcoinventImpacts.database.insert(columns.get_loc(str(latest_ecoinvent_year)), str(year), EcoinventImpacts.database[str(latest_ecoinvent_year)])
            elif year < earliest_ecoinvent_year:
                logging.info("Invest year {0} earlier than the earliest year specified in ecoinvent data. Using data of {1}".format(year,earliest_ecoinvent_year))
                EcoinventImpacts.database.insert(columns.get_loc(str(earliest_ecoinvent_year))-1, str(year), EcoinventImpacts.database[str(earliest_ecoinvent_year)])
                
    @staticmethod
    def _translate_process_name_to_ecoinvent_identifier(process_name: str):
        """Modifies a process name to match the ecoinvent identifiers.

        Deletes special charaters, spaces, commas, dots, etc. and replaces
        them by underscores. Furthermore secures that there is only one underscore
        in a row.  
        e.g.: 
            "passenger car, electric, without battery//[GLO] passenger car production, electric, without battery"
            becomes
            "passenger_car_electric_without_battery_GLO_passenger_car_production_electric_without_battery"

        """

        # Replace special characters
        process_name = process_name.replace(",", "_")
        process_name = process_name.replace("/", "_")
        process_name = process_name.replace("-", "_")
        process_name = process_name.replace(".", "_")
        process_name = process_name.replace(" ", "_")
        process_name = process_name.replace("//", "_")
        process_name = process_name.replace("[", "")
        process_name = process_name.replace("]", "")

        if process_name != "%":
            process_name = process_name.replace("%", "")

        # Making sure that there are never multiple underscore at the same time
        # it's inefficient, but does the job safely
        for underscore_counter in range(0, process_name.count("_")):
            process_name = process_name.replace("__", "_")

        return process_name

    @staticmethod
    def _translate_units_to_pint(json_path: Path):
        """Modifies units of impacts to match pint unit definitions.

        This method simply exchanges some units from the ecoinvent database to fit
        the corresponding unit definitions in pint. It uses a dictionary to replace
        the units. This dictionary is provided as a JSON-file.
        The path to the file has to be given to this method or preferably to the 
        method load_ecoinvent_database. 

        .. NOTE::
            If new units are needed they have to be added to this JSON-file.
            e.g. like this

                "kg PM2.5-.": "kg PM2_5_eq"
        """

        # Open JSON-file with dictionary of units to translate
        with open(json_path) as f:
            # Load JSON-file into a dictionary
            units_to_change = json.load(f)

        # Iterate over all units to translate
        for unit in progress.bar(units_to_change):
            # Change the unit of each row which unit equals the unit to translate
            EcoinventImpacts.database.loc[EcoinventImpacts.database.unit ==
                                          unit, 'unit'] = units_to_change[unit]

    @staticmethod
    def load_ecoinvent_subassemblies(subassemblies_path: Path):
        """Loads ecoinvent subassemblies from a directory.

        This methods automatically detects all CSV-files in a directory and
        assumes that all of them are ecoinvent subassemblies.
        """

        logging.info("Load ecoinvent subassemblies")

        # Get a list of all files and folders in the directory
        subassembly_paths = os.listdir(subassemblies_path)

        # Iterate through the members of the subassembly directory
        for subassembly in progress.bar(subassembly_paths):
            # Split the names at each "."
            filename = subassembly.split(".")
            # Check if the file ending is "csv"
            if filename[1] == "csv":
                # If the file is csv, read it in as a subassembly dataframe
                EcoinventImpacts.subassemblies[filename[0]] = pd.read_csv(
                    subassemblies_path / subassembly, float_precision="high", usecols=(lambda column: column != "comments"), encoding= 'unicode_escape')
                # Handle invest year not included in ecoinvent database
                ecoinvent_years = []
                columns = EcoinventImpacts.subassemblies[filename[0]].columns
                for column in columns:
                    try: # check if column name can be converted to integer
                        ecoinvent_years.append(int(column))
                    except ValueError:
                        continue
                ecoinvent_years.sort()
                earliest_ecoinvent_year = ecoinvent_years[0]    
                latest_ecoinvent_year = ecoinvent_years[-1]   
                for year in reversed(config.invest_years):
                    if year > earliest_ecoinvent_year and year < latest_ecoinvent_year and year not in ecoinvent_years:
                        # apply linear interpolation
                        logging.info("Invest year {} not specified in ecoinvent subassemblies, applying linear interpolation".format(year))
                        # find the next earlier and next later year in database
                        earlier_year = earliest_ecoinvent_year
                        for y in ecoinvent_years:
                            later_year = y
                            if y > year:
                                break # end for loop afer smaller year is found
                            else: 
                                earlier_year = y
                        interpolated_values = EcoinventImpacts.subassemblies[filename[0]][str(earlier_year)] + (EcoinventImpacts.subassemblies[filename[0]][str(later_year)] -EcoinventImpacts.subassemblies[filename[0]][str(earlier_year)]) * (year-earlier_year) / (later_year-earlier_year)
                        # insert column in database before the column of the later year
                        EcoinventImpacts.subassemblies[filename[0]].insert(columns.get_loc(str(later_year)), str(year), interpolated_values)
                    elif year > latest_ecoinvent_year:
                        logging.info("Invest year {0} later than the latest year specified in ecoinvent subassemblies. Using data of {1}".format(year,latest_ecoinvent_year))
                        EcoinventImpacts.subassemblies[filename[0]].insert(columns.get_loc(str(latest_ecoinvent_year)), str(year), EcoinventImpacts.subassemblies[filename[0]][str(latest_ecoinvent_year)])
                    elif year < earliest_ecoinvent_year:
                        logging.info("Invest year {0} earlier than the earliest year specified in ecoinvent subassemblies. Using data of {1}".format(year,earliest_ecoinvent_year))
                        EcoinventImpacts.subassemblies[filename[0]].insert(columns.get_loc(str(earliest_ecoinvent_year)), str(year), EcoinventImpacts.subassemblies[filename[0]][str(earliest_ecoinvent_year)])

    def get_impact_invest(self, impact_categories: list, construction_years: list, reference_year: int, invest_years: list):
        """This method gets the invest impact of a process in one or multiple impact categories for a specific year.

        This method determines the value of the requested impact categories for a process in a specific year.
        It takes into account when the process capacity was constructed, which year is the reference year
        of the current optimization horizon and which year is actually the current invest year.

        If the impact category is "cost", the corresponding method ProcessImpacts._get_cost_invest is used.
        In all other cases the impacts of all ecoinvent processes defined by the process itself as the
        invest phase, are multiplied with their corresponding weight factors and summed up together.

        It returns the results as a dictionary of impact categories and their corresponding value,
        including the correct units.

        Args:
            impact_categories (list): A list of one or more impact categories to be determined.

            construction_year (int): The year in which the process capacity was build. It is used to
                determine the actual impact a capacity build in that year has. Earliest year is used,
                if construction year is earlier than the earliest year in the data.

            reference_year (int): The reference year of the current optimization horizon. It is only
                used to discount the cost of the process during the whole optimization horizon to
                the reference year value.

            invest_year(int): The invest year investigated right now. It is used to determine whether
                a process still got invest annuities to pay, only maintenance costs remaining or 
                is already beyond its lifetime duration and therefore has no invest impacts anymore
                at all.
        """

        impact_invest_combined = []
         
        for invest_year in invest_years:
            for construction_year in construction_years:
                # Calculate annuity periods to determine whether invest impacts have to considered or not
                # Determine whether lifetime duration or economic period should be used, based on which is smaller
                full_annuity_period = min(ProcessImpacts.economic_period,
                                        units(str(self._lifetime_duration.at[0, str(max(construction_year, int(min(self._lifetime_duration.columns))))]) + str(self._lifetime_duration.at[0, "unit"])))
                                        
                # Create empty dictionary for the invest impacts
                impact_invest = {}
                impact_invest["process"] = self.name
                impact_invest["invest year"] = invest_year
                impact_invest["impact source"] = "invest"
                impact_invest["construction year"] = construction_year
                
                # Check if invest year is within the full annuity period after the construction
                # and therefore has investment impacts
                if (invest_year >= construction_year) and ((invest_year - construction_year) * units.year < full_annuity_period):
                    # Loop over all requested impact categories
                    for impact_category in impact_categories:
                        # Set the impact of the current impact category to 0 by default, before adding impacts from ecoinvent
                        impact_invest[impact_category] = units("0")

                        # Check if impact category is "cost", if it is, use special methods from ProcessImpacts
                        if impact_category == "cost":
                            impact_invest[impact_category] = self._get_cost_invest(
                                construction_year, reference_year, invest_year)

                        # If impact category is not "cost" calculate the sum of the impacts of all invest processes
                        else:
                            impact_invest[impact_category] = (self._recursively_get_impact(self._processes_invest, impact_category, construction_year, construction_year)
                                / full_annuity_period.to("years").magnitude)

                # If invest year is outside the full annuity period of the process instance return zero costs
                else:
                    for impact_category in impact_categories:
                        # Check if impact category is "cost", if it is, use special methods from ProcessImpacts
                        if impact_category == "cost":
                            # Get invest and maintenance costs
                            impact_invest[impact_category] = self._get_cost_invest(
                                construction_year, reference_year, invest_year)
                        else:
                            # Return 0 as impact since the invest impact has been fully accounted for already
                            impact_invest[impact_category] = units("0")

                impact_invest_combined.append(impact_invest)

        impact_invest_combined = pd.DataFrame(impact_invest_combined)
        impact_invest_combined.set_index(["impact source", "process", "invest year", "construction year"], inplace=True)

        return impact_invest_combined

    def get_impact_operation(self, impact_categories: list, construction_years: int, reference_year: int, invest_years: int):
        """This method gets the operational impact of a process in one or multiple impact categories for a specific year.

        This method determines the value of the requested impact categories for a process in a specific year.
        It takes into account when the process capacity was constructed, which year is the reference year
        of the current optimization horizon and which year is actually the current invest year.

        If the impact category is "cost", the corresponding method ProcessImpacts._get_cost_operation is used.
        In all other cases the impacts of all ecoinvent processes defined by the process itself as the
        operational phase, are multiplied with their corresponding weight factors and summed up together.

        It returns the results as a DataFrame of impact categories and their corresponding value,
        including the correct units.

        Args:
            impact_categories (list): A list of one or more impact categories to be determined.

            construction_year (int): The year in which the process capacity was build. It is used to
                determine the actual impact a capacity build in that year has. Earliest year is used,
                if construction year is earlier than the earliest year in the data.

            reference_year (int): The reference year of the current optimization horizon. It is only
                used to discount the cost of the process during the whole optimization horizon to
                the reference year value.

            invest_year(int): The invest year investigated right now. It is only used to
                discount the cost of the process during the whole optimization horizon to
                the reference year value.
        """

        impact_operation_combined = []
        
        for invest_year in invest_years:
            for construction_year in construction_years:
                # Create empty dictionary for the operational impacts
                impact_operation = {}
                impact_operation["process"] = self.name
                impact_operation["invest year"] = invest_year
                impact_operation["impact source"] = "operation"
                impact_operation["construction year"] = construction_year

                # Loop over all requested impact categories
                for impact_category in impact_categories:
                    # Set the impact of the current impact category to 0 by default, before adding impacts from ecoinvent
                    impact_operation[impact_category] = units("0")

                    # Check if impact category is "cost", if it is, use special methods from ProcessImpacts
                    if impact_category == "cost":
                        impact_operation[impact_category] = self._get_cost_operation(
                            construction_year, reference_year, invest_year)

                    # If impact category is not "cost" calculate the sum of the impacts of all invest processes
                    else:
                        impact_operation[impact_category] = self._recursively_get_impact(self._processes_operation, impact_category, construction_year, invest_year)

                impact_operation_combined.append(impact_operation)

        impact_operation_combined = pd.DataFrame(impact_operation_combined)
        impact_operation_combined.set_index(["impact source", "process", "invest year", "construction year"], inplace=True)

        return impact_operation_combined

    @staticmethod
    def _get_ecoinvent_process_impact(construction_year_attribute_name, process, impact_category: str, construction_year: int, impact_year: int):
        """Get the impact of a process in a specific impact category and construction year.

        Takes in an interable tuple from a process list (e.g. self._processes_invest) and returns
        the corresponding impact value.

        Args:
            process_list (pd.Dataframe): A DataFrame, which represents a list of processes, e.g. self._processes_invest

            process: An element of process_list.itertuple() which is currently evaluated.

            impact_category (str): The name of the evaluated impact category.

            construction_year (int): The year of construction for which the impact is determined.
                If the year of construction is earlier than the earliest year in the data, the
                earliest year is used.

            impact_year (int): The year in which the impact actually occurs. For invest impacts
                the impact_year is equal to the construction_year. If the impact_year is
                earlier than the earliest year in the data, the earliest year is used.

        Raises:
            KeyError: If the searched process does not exist in the ecoinvent database and seems to be used,
                since its weight factor is not zero.
        """

        # Check if process does not exist in ecoinvent database
        if not getattr(process, "process_name") in EcoinventImpacts.database.index.get_level_values("process"):
            # If the process does not exist, but would be multiplied by 0 anyway, output a warning, but continue
            if getattr(process, construction_year_attribute_name) == 0:
                logging.info("The process '{0}' does not exist in the ecoinvent database. Though its impact factor is 0, therefore it is skipped.".format(
                    getattr(process, "process_name")))
                return units("0") * units(EcoinventImpacts.database.loc[(slice(None), impact_category), "unit"].head(1).values[0])
            # If the process does not exist, but seems to be used, raise a KeyError
            else:
                logging.error("The process '{0}' does not exist in the ecoinvent database, but its impact factor is not 0. Make sure to only use existing processes.".format(
                    getattr(process, "process_name")))
                raise KeyError("The process '{0}' does not exist in the ecoinvent database, but its impact factor is not 0. Make sure to only use existing processes.".format(
                    getattr(process, "process_name")))

        # If process exists, return impact
        else:
            return (
                # Get impact of process from ecoinvent database
                EcoinventImpacts.database.at[(getattr(process, "process_name"), impact_category), str(
                    max(impact_year, int(min(EcoinventImpacts.database.columns))))]
                # Multiplied with its weight factor
                * getattr(process, construction_year_attribute_name)
                # Multiplied with its combined unit from the ecoinvent database...
                * units(EcoinventImpacts.database.at[(getattr(process, "process_name"), impact_category), "unit"])
            )

    @staticmethod
    def _isSubassembly(process_name: str):
        """Checks whether a process is actually a subassembly"""

        # Check if any subassemblies exist
        if EcoinventImpacts.subassemblies:
            # Check if the process name is in the dictionary of subassemblies ...
            if process_name in EcoinventImpacts.subassemblies:
                # ... return True, if it is
                return True
            else:
                # ... return False, if it isn't
                return False
        else:
            # If there are no subassemblies at all, issue a warning, in case the user forgot to load the subassembly definitions
            logging.warning(
                "There are no subassemblies. Assure that you have read in your ecoinvent subassemblies using load_ecoinvent_subassemblies.")
            return False

    @staticmethod
    def _recursively_get_impact(process_list: pd.DataFrame, impact_category: str, construction_year: int, impact_year: int):
        """Recursively calculates the impact of a process list"""

        # Iterate of the defined invest processes
        impact = 0
        for process in process_list.itertuples():
            # Check if process is not a comment line
            if not getattr(process, "process_name") == "%":
                logging.debug("Get {0} \tof {1}".format(impact_category, getattr(process, "process_name")))
                # Determine the attribute name of the construction year, since Integers column labels are transformed to
                # numbers following an underscore (e.g. "_03") to follow attribute naming conventions, when calling DataFrame.itertuples()
                construction_year_attribute_name = "_" + \
                    str(process_list.columns.get_loc(
                        str(max(construction_year, int(min(EcoinventImpacts.database.columns))))) + 1)
                
                # Check if process is not a subassembly
                if not EcoinventImpacts._isSubassembly(getattr(process, "process_name")):
                    # Add impact of ecoinvent process to the total impact
                   impact += (EcoinventImpacts._get_ecoinvent_process_impact(
                        construction_year_attribute_name, process, impact_category, construction_year, impact_year)
                        * units(str(process_list.at[getattr(process, "Index"), "unit"])))
                # If the process is actually a subassembly, iterate over all processes in that subassembly
                # and add their impacts to the total impact
                else:
                    impact += (EcoinventImpacts._recursively_get_impact(EcoinventImpacts.subassemblies[getattr(process, "process_name")], impact_category, construction_year, impact_year) 
                        * getattr(process, construction_year_attribute_name)
                        * units(str(process_list.at[getattr(process, "Index"), "unit"])))
        return impact


class ManualImpact(ProcessImpacts):
    """This is the class for manual impact definitions, when own databases are used. Currently, this is not fully implemented yet.

    """

    def __init__(self, process_path: Path = None):
        """See class documentation above for more information on the initialization."""

        logging.debug("Initialize a new instance of ManualImpact.")

        if process_path == None:
            raise NotImplementedError
        else:
            self._impact_invest = pd.read_csv(
                process_path / "impact_invest.csv", float_precision="high", usecols=(lambda column: column != "comments"), encoding= 'unicode_escape')
            self._impact_operation = pd.read_csv(
                process_path / "impact_operation.csv", float_precision="high", usecols=(lambda column: column != "comments"), encoding= 'unicode_escape')
  
    def get_impact_invest(self, impact_categories: list, construction_year: int, reference_year: int, invest_year: int):
        """ Get manual invest impacts.

        """

        raise NotImplementedError

    def get_impact_operation(self, impact_categories: list, construction_year: int, reference_year: int, invest_year: int):
        """ Get operational invest impacts.

        """

        raise NotImplementedError


class Process(ProcessImpacts):
    """This is the base class for processes of all kind.

    It inherits from ProcessImpacts, since all processes have impacts and costs.

    Args:
        process_path (Path): The path to a process directory.
    """

    construction_years = []
    # List of either nodes or connections, to be filled by subclasses
    locations = []

    def __init__(self, process_path: Path):
        """See class documentation above for more information on the initialization."""

        logging.debug("Initialize a new instance of Process.")

        super(Process, self).__init__(process_path)

        if process_path == None:
            self._properties = pd.DataFrame(columns=["value"])
            self._properties.index.name = "property"
        else:
            self._properties = pd.read_csv(
                process_path / "properties.csv", index_col="property", float_precision="high", usecols=(lambda column: column != "comments"), encoding= 'unicode_escape')

            if (process_path / Grid.selected().name).is_dir():
                self._existing_capacity = pd.read_csv(
                    process_path / Grid.selected().name / "existing_capacity.csv", float_precision="high", usecols=(lambda column: column != "comments"), encoding= 'unicode_escape')
                self._potential_capacity = pd.read_csv(
                    process_path / Grid.selected().name / "potential_capacity.csv", float_precision="high", usecols=(lambda column: column != "comments"), encoding= 'unicode_escape')
            else:
                logging.warning("No capacity data found for process '{0}' in grid '{1}'! Existing and potential capacities are assumed.".format(
                    self.name, Grid.selected().name))
                self._existing_capacity = pd.DataFrame(index=locations, data=np.nan, columns=["unit"] + ProcessImpacts.invest_years + ["comments"])
                self._potential_capacity = pd.DataFrame(index=locations, data=np.nan, columns=["unit"] + ProcessImpacts.invest_years + ["comments"])
        Process.processes.append(self)

    @classmethod
    def load_processes_from_directory(cls, process_category_path: Path):
        """Loads all processes of a specific class into their class."""

        logging.info("Load processes of class '{0}".format(str(cls)))

        # Get a list of all files and folders in the directory
        process_paths = os.listdir(process_category_path)

        # Iterate through the members of the process directory
        for process in progress.bar(process_paths):
            # Check if the file ending is "csv"
            if (process_category_path / process).is_dir():
                # If the file is csv, read it in as a dataframe
                cls(process_category_path / process)

    def get_numerical_property(self, property_to_return: str):
        """

        """

        return (units(self._properties.at[str(property_to_return), "value"]) * units(str(self._properties.at[str(property_to_return), "unit"])))

    @abc.abstractmethod
    def get_existing_capacity(self, locations: list, invest_years: list):
        """

        """

        raise NotImplementedError

    @abc.abstractmethod
    def get_potential_capacity(self, locations: list, invest_years: list):
        """

        """

        raise NotImplementedError

    def add_capacity(self, location: int, construction_year: int, new_capacity: pint.quantity):
        """Abstract method that adds capacity to a specific node or connection in a specific construction year."""

        existing_capacity = self._existing_capacity.reindex(
                index=[location], columns=[str(construction_year), "unit"]).fillna(0)
        
        self._existing_capacity.at[location, str(construction_year)] = existing_capacity.at[location, str(construction_year)] + (new_capacity / units(existing_capacity.at[location, "unit"])).to("dimensionless").magnitude

    @classmethod
    def get_combined_existing_capacity(cls):
        """ Returns the existing capacity

        """

        combined_existing_capacity = pd.DataFrame()
        combined_existing_capacity_list = []
        for process in progress.bar(cls.processes):
            if combined_existing_capacity.empty:
                combined_existing_capacity = process.get_existing_capacity(cls.locations, cls.invest_years)
            else:
                combined_existing_capacity_list.append(copy.deepcopy(process.get_existing_capacity(cls.locations, cls.invest_years)))
        
        if len(combined_existing_capacity_list) != 0:
            combined_existing_capacity = combined_existing_capacity.append(other=combined_existing_capacity_list, verify_integrity=True, sort=True)
        
        combined_existing_capacity.fillna(0, inplace=True)
        
        combined_existing_capacity = combined_existing_capacity.loc[:, (combined_existing_capacity != 0).any(axis=0)]

        if len(combined_existing_capacity.columns) == 0:
            combined_existing_capacity[min(ProcessImpacts.invest_years)] = 0

        index_names = combined_existing_capacity.index.names
        combined_existing_capacity = combined_existing_capacity.stack()
        combined_existing_capacity.index.names = index_names + ["construction year"]

        return combined_existing_capacity.sort_index()

    @classmethod
    def get_combined_potential_capacity(cls):
        """ Returns the potential capacity

        """

        combined_potential_capacity = pd.DataFrame()
        combined_potential_capacity_list = []
        for process in progress.bar(cls.processes):
            if combined_potential_capacity.empty:
                combined_potential_capacity = process.get_potential_capacity(cls.locations, cls.invest_years)
            else:
                combined_potential_capacity_list.append(copy.deepcopy(process.get_potential_capacity(cls.locations, cls.invest_years)))
        
        if len(combined_potential_capacity_list) != 0:
            combined_potential_capacity = combined_potential_capacity.append(other=combined_potential_capacity_list, verify_integrity=True, sort=True)
        
        combined_potential_capacity.fillna(0, inplace=True)
        

        index_names = combined_potential_capacity.index.names
        combined_potential_capacity = combined_potential_capacity.stack()
        combined_potential_capacity.index.names = index_names + ["construction year"]

        return combined_potential_capacity.sort_index()

    @classmethod
    def setup_construction_years(cls, combined_existing_capacity=pd.Series()):
        """Looks up the required construction years for this process class.
        
        A matrix of combined existing capacity can be provided to speed up the process.
        Otherwise the method will create a new combined_existing_capacity matrix.
        """

        if combined_existing_capacity.empty:
            combined_existing_capacity = cls.get_combined_existing_capacity()

        existing_construction_years = combined_existing_capacity.index.levels[combined_existing_capacity.index.names.index("construction year")]

        cls.construction_years = sorted(set([int(construction_year) for construction_year in existing_construction_years] + ProcessImpacts.invest_years))

        # return 
        return 


class NodalProcess(Process):
    """ This is the class for nodal processes.
    It inherits from Process.

    Args:
        process_path (Path): The path to a process directory.

    """

    # list of nodes used
    locations = []

    def __init__(self, process_path: Path):
        """See class documentation above for more information on the initialization.

        """

        logging.debug("Initialize a new instance of NodalProcess.")

        super(NodalProcess, self).__init__(process_path)

        # Setup nodes, if it hasn't happened yet
        if len(NodalProcess.locations) == 0:
            NodalProcess.locations = Grid.get_list_of_node_ids()

        if process_path == None:
            self._existing_capacity.columns = [
                "node", "unit"] + ProcessImpacts.invest_years + ["comments"]
            self._existing_capacity.index.name = "node"
            self._secured_capacity_factor = pd.DataFrame(
                columns=["unit"] + ProcessImpacts.invest_years + ["comments"])
            self._potential_capacity.index.name = "node"
        else:
            self._existing_capacity.set_index("node", inplace=True)
            self._potential_capacity.set_index("node", inplace=True)
            self._secured_capacity_factor = pd.read_csv(
                process_path / "secured_capacity_factor.csv", float_precision="high", usecols=(lambda column: column != "comments"), encoding= 'unicode_escape')
            self._technologymatrix = pd.read_csv(
                process_path / "technologymatrix.csv", float_precision="high", usecols=(lambda column: column != "comments"), encoding= 'unicode_escape')
            self._maximum_production_share = pd.read_csv(
                process_path / Grid.selected().name / "maximum_production_share.csv", index_col="node", float_precision="high", usecols=(lambda column: column != "comments"), encoding= 'unicode_escape')

    def get_existing_capacity(self, nodes: list, invest_years: list):
        """Returns existing capacity.

        """

        existing_capacity_combined  = pd.DataFrame
        existing_capacity_combined_list = []

        for invest_year in invest_years:
            construction_years = [column for column in self._existing_capacity.columns if ((secmod.helpers.isInteger(column) and (int(column) <= invest_year) and (invest_year - int(column) < self._lifetime_duration.at[0, str(max(int(column), int(min(self._lifetime_duration.columns))))])))]
            if len(construction_years) == 0:
                construction_years = [str(min(invest_years))]
            existing_capacity = self._existing_capacity.reindex(
                index=nodes, columns=construction_years + ["unit"])
            existing_capacity["process"] = self.name
            existing_capacity["invest year"] = invest_year
            existing_capacity.reset_index(inplace=True)
            existing_capacity.set_index(["node", "process", "invest year"], inplace=True)
            existing_capacity.fillna(0, inplace=True)
            for construction_year in construction_years:
                existing_capacity[construction_year] = existing_capacity[construction_year] * existing_capacity["unit"].apply(lambda unit: units(str(unit)))
            existing_capacity.drop(labels=["unit"], axis=1, inplace=True)

            if existing_capacity_combined.empty:
                existing_capacity_combined = existing_capacity
            else:
                existing_capacity_combined_list.append(copy.deepcopy(existing_capacity))

        if len(existing_capacity_combined_list) != 0:
            existing_capacity_combined = existing_capacity_combined.append(other=existing_capacity_combined_list, verify_integrity=True, sort=True)

        existing_capacity_combined.columns = [int(column) for column in existing_capacity_combined.columns]
        existing_capacity_combined.fillna(0, inplace=True)
        existing_capacity_combined.sort_index(inplace=True)
        
        return existing_capacity_combined

    def get_potential_capacity(self, locations: list, invest_years: list):
        """Returns potential capacity.

        """

        column_year = {}
        for invest_year in invest_years:
            column_year[invest_year] = str(max(invest_year, int(min(self._potential_capacity.columns))))

        potential_capacity = self._potential_capacity.reindex(index=locations, columns= [str(column_year[invest_year]) for invest_year in invest_years] + ["unit"])
        potential_capacity["process"] = self.name
        potential_capacity.fillna(np.inf, inplace=True)
        potential_capacity.reset_index(inplace=True)
        potential_capacity.set_index(["node", "process"], inplace=True)
        potential_capacity.columns = invest_years + ["unit"]
        potential_capacity.fillna(np.inf, inplace=True)
        for invest_year in invest_years:
            potential_capacity[invest_year] = potential_capacity[invest_year] * potential_capacity["unit"].apply(lambda unit: units(str(unit)))
        potential_capacity.drop(labels=["unit"], axis=1, inplace=True)
        potential_capacity.sort_index(inplace=True)
        potential_capacity.columns = [int(column) for column in potential_capacity.columns]
        return potential_capacity

    def get_secured_capacity_factor(self, construction_years: list):
        """Returns secured capacity factor.

        """

        column_year = {}
        for construction_year in construction_years:
            column_year[construction_year] = str(max(construction_year, int(min(self._secured_capacity_factor.columns))))
        
        secured_capacity_factor = self._secured_capacity_factor.reindex(index=[0], columns=[str(column_year[construction_year]) for construction_year in construction_years] + ["unit"])
        secured_capacity_factor.columns = construction_years + ["unit"]
        secured_capacity_factor["process"] = self.name
        secured_capacity_factor.fillna(1, inplace=True)
        secured_capacity_factor.set_index(["process"], inplace=True)
        for construction_year in construction_years:
            secured_capacity_factor[construction_year] = secured_capacity_factor[construction_year] * secured_capacity_factor["unit"].apply(lambda unit: units(str(unit)))
        secured_capacity_factor.drop(labels=["unit"], axis=1, inplace=True)
        secured_capacity_factor.sort_index(inplace=True)

        return secured_capacity_factor

    @classmethod
    def get_combined_secured_capacity_factor(cls):
        """Gets the secured capacity factors for all processes of a process category."""

        if len(cls.construction_years) == 0:
            cls.setup_construction_years()

        combined_secured_capacity_factor = pd.DataFrame()
        combined_secured_capacity_factor_list = []
        for process in progress.bar(cls.processes):
            combined_secured_capacity_factor_list.append(copy.deepcopy(process.get_secured_capacity_factor(cls.construction_years)))
        
        if len(combined_secured_capacity_factor_list) != 0:
            combined_secured_capacity_factor = combined_secured_capacity_factor.append(other=combined_secured_capacity_factor_list, verify_integrity=True, sort=True)

        index_names = combined_secured_capacity_factor.index.names
        combined_secured_capacity_factor = combined_secured_capacity_factor.stack()
        combined_secured_capacity_factor.index.names = index_names + ["construction year"]
        return combined_secured_capacity_factor.sort_index()

    @abc.abstractmethod
    def get_technologymatrix(self, construction_years: list, products: list):
        """

        """

        raise NotImplementedError

    @classmethod
    def get_combined_technology_matrix(cls, products: list):
        """Returns technology matrix for all production processes.

        """

        if len(cls.construction_years) == 0:
                cls.setup_construction_years()

        combined_technology_matrix = pd.DataFrame()
        combined_technology_matrix_list = []
        for process in progress.bar(cls.processes):
            combined_technology_matrix_list.append(copy.deepcopy(process.get_technologymatrix(cls.construction_years, products)))
        
        if len(combined_technology_matrix_list) != 0:
            combined_technology_matrix = combined_technology_matrix.append(other=combined_technology_matrix_list, verify_integrity=True, sort=True)

        index_names = combined_technology_matrix.index.names
        combined_technology_matrix = combined_technology_matrix.stack()
        combined_technology_matrix.index.names = index_names + ["construction year"]
        return combined_technology_matrix.sort_index()
    
    def get_maximum_production_share(self, nodes: list, invest_years: list):
        """Returns maximum production share.

        """

        column_year = {}
        for invest_year in invest_years:
            column_year[invest_year] = str(max(invest_year, int(min(self._secured_capacity_factor.columns))))

        maximum_production_share = self._maximum_production_share.reindex(index=nodes, columns= [str(column_year[invest_year]) for invest_year in invest_years] +["unit"])
        maximum_production_share["process"] = self.name
        maximum_production_share.fillna(1, inplace=True)
        maximum_production_share.reset_index(inplace=True)
        maximum_production_share.set_index(["node", "process"], inplace=True)
        maximum_production_share.sort_index(inplace=True)

        return maximum_production_share

class ConnectionProcess(Process):
    """This is the class for connection processes.
    It inherits from Process.

    Args:
        process_path (Path): The path to a process directory.

    """

    # List of connections used
    locations = []

    def __init__(self, process_path: Path):
        """See class documentation above for more information on the initialization."""

        logging.debug("Initialize a new instance of ConnectionProcess.")

        super(ConnectionProcess, self).__init__(process_path)

        # Setup connections, if it hasn't happened yet
        if len(ConnectionProcess.locations) == 0:
            ConnectionProcess.locations = Grid.get_list_of_connection_ids()

        if process_path == None:
            self._existing_capacity.index.name = "connection"
            self._potential_capacity.index.name = "connection"
        else:
            self._existing_capacity.set_index("connection", inplace=True)
            self._potential_capacity.set_index("connection", inplace=True)

    def get_existing_capacity(self, connections: list, invest_years: list):
        """Returns existing capacity.

        """

        existing_capacity_combined = pd.DataFrame()
        existing_capacity_combined_list = []

        for invest_year in invest_years:
            existing_capacity = pd.DataFrame()
            construction_years = [column for column in self._existing_capacity.columns if ((secmod.helpers.isInteger(column) and (float(column) <= invest_year) and (invest_year - int(column) < self._lifetime_duration.at[0, str(max(int(column), int(min(self._lifetime_duration.columns))))])))]
            if len(construction_years) == 0:
                construction_years = [min(invest_years)]
            existing_capacity = self._existing_capacity.reindex(
                index=connections, columns=construction_years + ["unit"])
            existing_capacity["process"] = self.name
            existing_capacity["invest year"] = invest_year
            existing_capacity.reset_index(inplace=True)
            existing_capacity.set_index(["connection", "process", "invest year"], inplace=True)
            existing_capacity.fillna(0, inplace=True)
            for construction_year in construction_years:
                existing_capacity[construction_year] = existing_capacity[construction_year] * existing_capacity["unit"].apply(lambda unit: units(str(unit)))
            existing_capacity.drop(labels=["unit"], axis=1, inplace=True)

            existing_capacity_combined_list.append(copy.deepcopy(existing_capacity))

        if len(existing_capacity_combined_list) != 0:
            existing_capacity_combined = existing_capacity_combined.append(other=existing_capacity_combined_list, verify_integrity=True, sort=True)

        existing_capacity_combined.columns = [int(column) for column in existing_capacity_combined.columns]
        existing_capacity_combined.fillna(0, inplace=True)
        existing_capacity_combined.sort_index(inplace=True)

        return existing_capacity_combined
        

    @abc.abstractmethod
    def get_potential_capacity(self, locations: list, invest_years: list):
        """Returns potential capacity.

        """

        raise NotImplementedError


class ProductionProcess(NodalProcess):
    """ This is the class for production processes.
    It inherits from nodal process.

    Args:
        process_path (Path): The path to a process directory.

    """

    processes           = []
    construction_years  = []

    def __init__(self, process_path: Path):
        """See class documentation above for more information on the initialization."""

        logging.debug("Initialize a new instance of ProductionProcess.")

        super(ProductionProcess, self).__init__(process_path)

        if process_path == None:
            self._existing_capacity.columns = [
                "unit"] + ProcessImpacts.invest_years + ["comments"]
            self._technologymatrix = pd.DataFrame(
                columns=["product", "unit"] + ProcessImpacts.invest_years + ["comments"])
        else:
            self._technologymatrix.set_index("product", inplace=True)
            self._technologymatrix_timeseries = secmod.helpers.correct_time_stamp_of_timeseries(pd.read_csv(
                process_path / "technologymatrix_timeseries.csv", index_col="time slice", float_precision="high", usecols=(lambda column: column != "comments"), encoding= 'unicode_escape'))
            self._availability_timeseries = secmod.helpers.correct_time_stamp_of_timeseries(pd.read_csv(
                process_path / Grid.selected().name / "availability_timeseries.csv", index_col="time slice", float_precision="high", usecols=(lambda column: column != "comments"), encoding= 'unicode_escape'))
            # additional data in production processes for MILP Optimization
            # Read-in the minimal capacity that is required if a component is built
            if config.optimization_mode == 'MILP':
                self._minimal_capacity = pd.read_csv(
                    process_path / Grid.selected().name / "minimal_capacity.csv", index_col="node", float_precision="high", usecols=(lambda column: column != "comments"), encoding= 'unicode_escape')
            
        ProductionProcess.processes.append(self)

    def get_technologymatrix(self, construction_years: list, products: list):
        """Return technologymatrix for a single process.

        For MILP, the technology matrix consists of several part-load sections

        """

        if config.optimization_mode == 'MILP':
            corner_points = self._technologymatrix.index.value_counts()[0] 
            vertices = [x for x in range(corner_points)] # create set of vertices
            #vertices = ['vertex'+str(x) for x in range(corner_points)] # create set of vertices
            used_products = self._technologymatrix.index.unique().to_list()   
            i1=[(x,y,self.name) for x in vertices for y in used_products] # all tuple combinations for used produces
            
            not_used_products = [x for x in products if x not in used_products]
            i2=[(x,y,self.name) for x in vertices for y in not_used_products] # all tuple combinations for not used produces
            
            column_years = [str(max(construction_year, int(min(self._technologymatrix.columns)))) for construction_year in construction_years]
            column_years += ["unit"]
            
            technologymatrix = self._technologymatrix[self._technologymatrix.columns.intersection(column_years)]
            
            technologymatrix_0 = pd.DataFrame(0, index=range(len(i2)), columns=range(len(column_years)))
            technologymatrix_0.columns = column_years
            technologymatrix=pd.concat([technologymatrix, technologymatrix_0])

            i_tot = i1+i2
            vert=[i[0] for i in i_tot]
            prod=[i[1] for i in i_tot]
            proc=[i[2] for i in i_tot]
            technologymatrix["vertex"] = vert
            technologymatrix["product"] = prod
            technologymatrix["process"] = proc
            
            technologymatrix.reset_index(inplace=True)
            technologymatrix.set_index(["vertex","product","process"], inplace=True)
            
            # Add unit to "unit" (for not used products)
            technologymatrix["unit"] = technologymatrix.apply(lambda row: row.at["unit"] if isinstance(row.at["unit"], str) else config.unit_dictionary[row.name[1]], axis=1)
            
            # add units
            for construction_year in construction_years:
                technologymatrix[str(construction_year)] = technologymatrix[str(construction_year)] * technologymatrix["unit"].apply(lambda unit: units(str(unit)))#) / units(self._properties.at["reference product", "unit"]))
                        
            technologymatrix.drop(labels=["unit","index"], axis=1, inplace=True)
            technologymatrix.sort_index(inplace=True)
            technologymatrix.columns = technologymatrix.columns.astype(int)

        else:
            column_years = [str(max(construction_year, int(min(
                self._technologymatrix.columns)))) for construction_year in construction_years]
        
            technologymatrix = self._technologymatrix.reindex(
                index=products, columns=column_years + ["unit"])
            technologymatrix.columns = construction_years + ["unit"]
            technologymatrix["process"] = self.name
            technologymatrix.fillna(0, inplace=True)
            technologymatrix.reset_index(inplace=True)
            technologymatrix.set_index(["product", "process"], inplace=True)
            for construction_year in construction_years:
                technologymatrix[construction_year] = technologymatrix[construction_year] * technologymatrix["unit"].apply(lambda unit: units(str(unit)) / units(self._properties.at["reference product", "unit"]))
            technologymatrix.drop(labels=["unit"], axis=1, inplace=True)
            technologymatrix.sort_index(inplace=True)

        return technologymatrix
    
    def get_availability_timeseries(self):
        """Returns availability timeseries

        """

        time_slices = list(self._availability_timeseries.index)
        availability_timeseries = self._availability_timeseries.reindex(index=time_slices, columns=[str(location) for location in self.locations])
        availability_timeseries.columns = self.locations
        availability_timeseries["process"] = self.name
        availability_timeseries.reset_index(inplace=True)
        availability_timeseries.set_index(["time slice", "process"], inplace=True)

        return availability_timeseries

    @staticmethod
    def get_combined_availability_timeseries():
        """Returns combined availability timeseries

        """

        combined_availability_timeseries = pd.DataFrame()
        combined_availability_timeseries_list = []

        for process in ProductionProcess.processes:
            combined_availability_timeseries_list.append(copy.deepcopy(process.get_availability_timeseries()))

        if len(combined_availability_timeseries_list) != 0:
            combined_availability_timeseries = combined_availability_timeseries.append(other=combined_availability_timeseries_list, verify_integrity=True, sort=True)

        combined_availability_timeseries.fillna(1, inplace=True)
        combined_availability_timeseries = combined_availability_timeseries.stack()
        combined_availability_timeseries = combined_availability_timeseries.reset_index()
        combined_availability_timeseries.columns = ["time slice", "process", "node", "value"]
        combined_availability_timeseries.set_index(["node", "process", "time slice"], inplace=True)
        combined_availability_timeseries.sort_index(inplace=True)

        return combined_availability_timeseries.sort_index()

    def get_technology_matrix_timeseries(self):
        """Returns time-dependent technologymatrix

        """

        technologymatrix_timeseries = self._technologymatrix_timeseries.drop(["unit"], axis=1)
        technologymatrix_timeseries["process"] = self.name
        technologymatrix_timeseries.reset_index(inplace=True)
        technologymatrix_timeseries.set_index(["time slice", "process"], inplace=True)

        return technologymatrix_timeseries
    
    @staticmethod
    def get_combined_technology_matrix_timeseries():
        """Returns combined availability timeseries

        """

        combined_technologymatrix_timeseries = pd.DataFrame()
        combined_technologymatrix_timeseries_list = []

        for process in ProductionProcess.processes:
            combined_technologymatrix_timeseries_list.append(copy.deepcopy(process.get_technology_matrix_timeseries()))

        if len(combined_technologymatrix_timeseries_list) != 0:
            combined_technologymatrix_timeseries = combined_technologymatrix_timeseries.append(other=combined_technologymatrix_timeseries_list, verify_integrity=True, sort=True)

        combined_technologymatrix_timeseries.fillna(1, inplace=True)
        combined_technologymatrix_timeseries = combined_technologymatrix_timeseries.stack()
        combined_technologymatrix_timeseries = combined_technologymatrix_timeseries.reset_index()
        combined_technologymatrix_timeseries.columns = ["time slice", "process", "product", "value"]
        combined_technologymatrix_timeseries.set_index(["product", "process", "time slice"], inplace=True)
        combined_technologymatrix_timeseries.sort_index(inplace=True)

        return combined_technologymatrix_timeseries.sort_index()
    
    def get_cornerpoints(self):
        """
        determines the number of corner points/part-load sections to discretize the process in an MILP
        """
        corner_points = self._technologymatrix.index.value_counts()[0]
        return corner_points

    @staticmethod
    def get_combined_cornerpoints():
        """
        determines the number of cornerpoints for all processes"
        """
        combined_cornerpoints_list = {}

        for process in ProductionProcess.processes:
            combined_cornerpoints_list[process.name] = process.get_cornerpoints()
            
        return combined_cornerpoints_list
    
    def get_reference_products(self):
        """
        determines the reference product of a production process
        """
        reference_product = self._properties.at["reference product","value"]
        return reference_product

    @staticmethod
    def get_combined_reference_product():
        """
        determines the reference products for all production processes
        """
        combined_reference_products_list = {}

        for process in ProductionProcess.processes:
            combined_reference_products_list[process.name] = process.get_reference_products()
            
        return combined_reference_products_list

    def get_gradient_technologymatrix(self, construction_years: list, products: list):
        """
        determines the gradient between two cornerpoints/in a section for the defined process
        """
        gradient = {}
        
        techmatrix=self.get_technologymatrix(construction_years, products)
        cornerpoints=self.get_cornerpoints()
        ref_prod = self._properties.at['reference product','value']
        
        # determine gradient
        for cp in range(cornerpoints-1): # cornerpoints-1: cannot determine gradient starting from last point
            v_0 = cp
            v_1 = cp+1
            for product in products:
                    enumerator = techmatrix.loc[(v_1,ref_prod,self.name)]-techmatrix.loc[(v_0,ref_prod,self.name)]
                    numerator = techmatrix.loc[(v_1,product,self.name)]-techmatrix.loc[(v_0,product,self.name)]
                    gradient[v_0,product,self.name] = numerator.divide(enumerator)
                    return_matrix = pd.DataFrame(*[gradient]).transpose()
        
        # bring return_matrix in "correct" form
        return_matrix = return_matrix.stack()
        return_matrix = return_matrix.reset_index()
        return_matrix.columns = ["vertex", "product", "process", "construction year", "value"]
        return_matrix.set_index(["vertex", "product", "process", "construction year"], inplace=True)

        return return_matrix
    
    def get_combined_gradient_technologymatrix(cls, products: list):
        """
        get gradients for all processes
        """
        if len(cls.construction_years) == 0:
            cls.setup_construction_years()
            
        combined_gradient_technologymatrix = pd.DataFrame()
        combined_gradient_technologymatrix_list = []
        for process in progress.bar(cls.processes):
            combined_gradient_technologymatrix_list.append(copy.deepcopy(process.get_gradient_technologymatrix(cls.construction_years, products)))

        
        if len(combined_gradient_technologymatrix_list) != 0:
            combined_gradient_technologymatrix = combined_gradient_technologymatrix.append(other=combined_gradient_technologymatrix_list, verify_integrity=True, sort=True)

        return combined_gradient_technologymatrix.sort_index()
    
    def get_relative_partload_capacity(self, construction_years: list, products: list):
        """
        get the amount of capacity that can be used at each cornerpoint
        """

        partload_rel=self.get_technologymatrix(construction_years, products)
        ref_prod = self.get_reference_products()
        partload_rel=partload_rel[partload_rel.index.get_level_values('product').isin([ref_prod])]
        partload_rel = partload_rel.abs()
        partload_rel= partload_rel.droplevel('product')
            
        partload_rel = partload_rel.stack()
        partload_rel = partload_rel.reset_index()
        partload_rel.columns = ["vertex", "process", "construction year", "value"]
        partload_rel.set_index(["vertex", "process", "construction year"], inplace=True)

        return partload_rel
    
    def get_combined_relative_partload_capacity(cls, products: list):
        """
        get the amount of capacity that can be used at each cornerpoint for all processes
        """
        if len(cls.construction_years) == 0:
            cls.setup_construction_years()
            
        combined_partload_capacity = pd.DataFrame()
        combined_partload_capacity_list = []
        for process in progress.bar(cls.processes):
            combined_partload_capacity_list.append(copy.deepcopy(process.get_relative_partload_capacity(cls.construction_years, products)))
        
        if len(combined_partload_capacity_list) != 0:
            combined_partload_capacity = combined_partload_capacity.append(other=combined_partload_capacity_list, verify_integrity=True, sort=True)

        return combined_partload_capacity.sort_index()
    
    def get_minimal_capacity(self, locations: list, invest_years: list):
        """
        get minimal capacity to be built for a production process
        """

        column_year = {}
        for invest_year in invest_years:
            column_year[invest_year] = str(max(invest_year, int(min(self._potential_capacity.columns))))

        minimal_capacity = self._minimal_capacity.reindex(index=locations, columns= [str(column_year[invest_year]) for invest_year in invest_years] + ["unit"])
        minimal_capacity["process"] = self.name
        minimal_capacity.fillna(0, inplace=True)
        minimal_capacity.reset_index(inplace=True)
        minimal_capacity.set_index(["node", "process"], inplace=True)
        minimal_capacity.columns = invest_years + ["unit"]
        minimal_capacity.fillna(0, inplace=True)
        for invest_year in invest_years:
            minimal_capacity[invest_year] = minimal_capacity[invest_year] * minimal_capacity["unit"].apply(lambda unit: units(str(unit)))
        minimal_capacity.drop(labels=["unit"], axis=1, inplace=True)
        minimal_capacity.sort_index(inplace=True)
        minimal_capacity.columns = [int(column) for column in minimal_capacity.columns]
        return minimal_capacity
    
    @classmethod
    def get_combined_minimal_capacity(cls):
        """
        get minimal capacities to be built for all processes
        """

        combined_minimal_capacity = pd.DataFrame()
        combined_minimal_capacity_list = []
        for process in progress.bar(cls.processes):
            if combined_minimal_capacity.empty:
                combined_minimal_capacity = process.get_minimal_capacity(cls.locations, cls.invest_years)
            else:
                combined_minimal_capacity_list.append(copy.deepcopy(process.get_minimal_capacity(cls.locations, cls.invest_years)))
        
        if len(combined_minimal_capacity_list) != 0:
            combined_minimal_capacity = combined_minimal_capacity.append(other=combined_minimal_capacity_list, verify_integrity=True, sort=True)
        
        combined_minimal_capacity.fillna(0, inplace=True)

        index_names = combined_minimal_capacity.index.names
        combined_minimal_capacity = combined_minimal_capacity.stack()
        combined_minimal_capacity.index.names = index_names + ["construction year"]

        return combined_minimal_capacity.sort_index()
        
class StorageProcess(NodalProcess):
    """This is the class for storage processes.
    It inherits from nodal process.

    Args:
        process_path (Path): The path to a process directory.

    """

    DIRECTIONS_STORAGE      = {None: ["deposit", "withdraw"]}
    STORAGE_LEVEL_FACTOR    = {"deposit": 1, "withdraw": -1}

    processes           = []
    construction_years  = []

    def __init__(self, process_path: Path):
        """See class documentation above for more information on the initialization."""

        logging.debug("Initialize a new instance of StorageProcess.")

        super(StorageProcess, self).__init__(process_path)

        if process_path == None:
            self._existing_capacity.columns = [
                "unit"] + ProcessImpacts.invest_years + ["comments"]
            self._technologymatrix = pd.DataFrame(
                columns=["unit"] + ProcessImpacts.invest_years + ["comments"], index=["deposit", "withdraw"])
            self._technologymatrix.index.name = "direction"
        else:
            self._technologymatrix.set_index("direction", inplace=True)
            self._flow_to_storage_capacity_factor = pd.read_csv(
                process_path / "flow_to_storage_capacity_factor.csv", float_precision="high", usecols=(lambda column: column != "comments"), encoding= 'unicode_escape')
            # additional data in production processes for MILP Optimization
            if config.optimization_mode == 'MILP':
                self._minimal_capacity = pd.read_csv(
                    process_path / Grid.selected().name / "minimal_capacity.csv", index_col="node", float_precision="high", usecols=(lambda column: column != "comments"), encoding= 'unicode_escape')
                self._min_rel_storage_level_factor = pd.read_csv(
                    process_path / "min_rel_storage_level_factor.csv", float_precision="high", usecols=(lambda column: column != "comments"), encoding= 'unicode_escape')
                self._rel_storage_loss_factor = pd.read_csv(
                    process_path / "rel_storage_loss_factor.csv", float_precision="high", usecols=(lambda column: column != "comments"), encoding= 'unicode_escape')

        StorageProcess.processes.append(self)

    def get_technologymatrix(self, construction_years: list, products: list):
        """Returns the technologymatrix for storage process

        """

        column_years = {}
        for construction_year in construction_years:
            column_years[construction_year] = str(
                max(construction_year, int(min(self._technologymatrix.columns))))

        technologymatrix = []

        for product in products:
            for direction in ["deposit", "withdraw"]:
                # Set index values
                row = {"product": product, "process": self.name,
                       "direction": direction}

                # Set value
                for construction_year in construction_years:
                    
                    if product != self._properties.at["reference product", "value"]:
                        row[construction_year] = np.nan
                    else:
                        row[construction_year] = self._technologymatrix.at[direction,
                                                                                column_years[construction_year]]
                # Add row to list
                technologymatrix.append(copy.deepcopy(row))

        technologymatrix = pd.DataFrame(technologymatrix)
        technologymatrix.fillna(0, inplace=True)
        technologymatrix.set_index(
            ["product", "process", "direction"], inplace=True)
        return (technologymatrix)

    def get_flow_to_storage_capacity_factor(self, construction_years: list):
        """Returns the flow-to-storagecapacity-factor

        """

        column_year = {}
        for construction_year in construction_years:
            column_year[construction_year] = str(max(construction_year, int(min(self._flow_to_storage_capacity_factor.columns))))
        
        flow_to_storage_capacity_factor = self._flow_to_storage_capacity_factor.reindex(index=[0], columns=[str(column_year[construction_year]) for construction_year in construction_years] + ["unit"])
        flow_to_storage_capacity_factor.columns = construction_years + ["unit"]
        flow_to_storage_capacity_factor["process"] = self.name
        flow_to_storage_capacity_factor.fillna(1, inplace=True)
        flow_to_storage_capacity_factor.set_index(["process"], inplace=True)
        for construction_year in construction_years:
            flow_to_storage_capacity_factor[construction_year] = flow_to_storage_capacity_factor[construction_year] * flow_to_storage_capacity_factor["unit"].apply(lambda unit: units(str(unit)))
        flow_to_storage_capacity_factor.drop(labels=["unit"], axis=1, inplace=True)

        return flow_to_storage_capacity_factor
        
    @staticmethod
    def get_combined_flow_to_storage_capacity_factor():
        """Returns the combined flow to storagecapacity factor

        """

        combined_flow_to_storage_capacity_factor = pd.DataFrame()
        combined_flow_to_storage_capacity_factor_list = []

        for process in progress.bar(StorageProcess.processes):
            combined_flow_to_storage_capacity_factor_list.append(copy.deepcopy(process.get_flow_to_storage_capacity_factor(StorageProcess.construction_years)))

        if len(combined_flow_to_storage_capacity_factor_list) != 0:
            combined_flow_to_storage_capacity_factor = combined_flow_to_storage_capacity_factor.append(other=combined_flow_to_storage_capacity_factor_list, verify_integrity=True, sort=True)

        combined_flow_to_storage_capacity_factor.fillna(1, inplace=True)
        combined_flow_to_storage_capacity_factor = combined_flow_to_storage_capacity_factor.stack()
        combined_flow_to_storage_capacity_factor.sort_index(inplace=True)

        return combined_flow_to_storage_capacity_factor.sort_index()

    @staticmethod
    def get_combined_storage_products(products):
        """Return a dictionary of all storage processes and their storeable product."""

        combined_storage_products_dict = {}

        for process in progress.bar(StorageProcess.processes):
            combined_storage_products_dict[process.name] = process._properties.at["reference product", "value"]

        return combined_storage_products_dict
    
    def get_minimal_capacity(self, locations: list, invest_years: list):
        """
        get minimal capacity to be built for a storage process
        """

        column_year = {}
        for invest_year in invest_years:
            column_year[invest_year] = str(max(invest_year, int(min(self._potential_capacity.columns))))

        minimal_capacity = self._minimal_capacity.reindex(index=locations, columns= [str(column_year[invest_year]) for invest_year in invest_years] + ["unit"])
        minimal_capacity["process"] = self.name
        minimal_capacity.fillna(0, inplace=True)
        minimal_capacity.reset_index(inplace=True)
        minimal_capacity.set_index(["node", "process"], inplace=True)
        minimal_capacity.columns = invest_years + ["unit"]
        minimal_capacity.fillna(0, inplace=True)
        for invest_year in invest_years:
            minimal_capacity[invest_year] = minimal_capacity[invest_year] * minimal_capacity["unit"].apply(lambda unit: units(str(unit)))
        minimal_capacity.drop(labels=["unit"], axis=1, inplace=True)
        minimal_capacity.sort_index(inplace=True)
        minimal_capacity.columns = [int(column) for column in minimal_capacity.columns]
        return minimal_capacity
    
    @classmethod
    def get_combined_minimal_capacity(cls):
        """
        get minimal capacity to be built for all storage processes
        """

        combined_minimal_capacity = pd.DataFrame()
        combined_minimal_capacity_list = []
        for process in progress.bar(cls.processes):
            if combined_minimal_capacity.empty:
                combined_minimal_capacity = process.get_minimal_capacity(cls.locations, cls.invest_years)
            else:
                combined_minimal_capacity_list.append(copy.deepcopy(process.get_minimal_capacity(cls.locations, cls.invest_years)))
        
        if len(combined_minimal_capacity_list) != 0:
            combined_minimal_capacity = combined_minimal_capacity.append(other=combined_minimal_capacity_list, verify_integrity=True, sort=True)
        
        combined_minimal_capacity.fillna(0, inplace=True)

        index_names = combined_minimal_capacity.index.names
        combined_minimal_capacity = combined_minimal_capacity.stack()
        combined_minimal_capacity.index.names = index_names + ["construction year"]

        return combined_minimal_capacity.sort_index()

    def get_min_rel_storage_level_factor(self, construction_years: list):
        """
        return minimal relative storage level
        """

        column_year = {}
        for construction_year in construction_years:
            column_year[construction_year] = str(max(construction_year, int(min(self._min_rel_storage_level_factor.columns))))
        
        min_rel_storage_level_factor = self._min_rel_storage_level_factor.reindex(index=[0], columns=[str(column_year[construction_year]) for construction_year in construction_years] + ["unit"])
        min_rel_storage_level_factor.columns = construction_years + ["unit"]
        min_rel_storage_level_factor["process"] = self.name
        min_rel_storage_level_factor.fillna(1, inplace=True)
        min_rel_storage_level_factor.set_index(["process"], inplace=True)
        for construction_year in construction_years:
            min_rel_storage_level_factor[construction_year] = min_rel_storage_level_factor[construction_year] * min_rel_storage_level_factor["unit"].apply(lambda unit: units(str(unit)))
        min_rel_storage_level_factor.drop(labels=["unit"], axis=1, inplace=True)

        return min_rel_storage_level_factor
        
    @staticmethod
    def get_combined_min_rel_storage_level_factor():
        """
        return minimal relative storage level for all storage processes
        """

        combined_min_rel_storage_level_factor = pd.DataFrame()
        combined_min_rel_storage_level_factor_list = []

        for process in progress.bar(StorageProcess.processes):
            combined_min_rel_storage_level_factor_list.append(copy.deepcopy(process.get_min_rel_storage_level_factor(StorageProcess.construction_years)))

        if len(combined_min_rel_storage_level_factor_list) != 0:
            combined_min_rel_storage_level_factor = combined_min_rel_storage_level_factor.append(other=combined_min_rel_storage_level_factor_list, verify_integrity=True, sort=True)

        combined_min_rel_storage_level_factor.fillna(1, inplace=True)
        combined_min_rel_storage_level_factor = combined_min_rel_storage_level_factor.stack()
        combined_min_rel_storage_level_factor.sort_index(inplace=True)

        return combined_min_rel_storage_level_factor.sort_index()
    
    def get_rel_storage_loss_factor(self, construction_years: list):
        """
        returns storage loss between to time steps for a single storage process
        """
        
        column_year = {}
        for construction_year in construction_years:
            column_year[construction_year] = str(max(construction_year, int(min(self._rel_storage_loss_factor.columns))))
        
        rel_storage_loss_factor = self._rel_storage_loss_factor.reindex(index=[0], columns=[str(column_year[construction_year]) for construction_year in construction_years] + ["unit"])
        rel_storage_loss_factor.columns = construction_years + ["unit"]
        rel_storage_loss_factor["process"] = self.name
        rel_storage_loss_factor.fillna(1, inplace=True)
        rel_storage_loss_factor.set_index(["process"], inplace=True)
        for construction_year in construction_years:
            rel_storage_loss_factor[construction_year] = rel_storage_loss_factor[construction_year] * rel_storage_loss_factor["unit"].apply(lambda unit: units(str(unit)))
        rel_storage_loss_factor.drop(labels=["unit"], axis=1, inplace=True)

        return rel_storage_loss_factor
    
    @staticmethod
    def get_combined_rel_storage_loss_factor():
        """
        returns storage loss between to time steps for all storage processes
        """

        combined_rel_storage_loss_factor = pd.DataFrame()
        combined_rel_storage_loss_factor_list = []

        for process in progress.bar(StorageProcess.processes):
            combined_rel_storage_loss_factor_list.append(copy.deepcopy(process.get_rel_storage_loss_factor(StorageProcess.construction_years)))

        if len(combined_rel_storage_loss_factor_list) != 0:
            combined_rel_storage_loss_factor = combined_rel_storage_loss_factor.append(other=combined_rel_storage_loss_factor_list, verify_integrity=True, sort=True)

        combined_rel_storage_loss_factor.fillna(1, inplace=True)
        combined_rel_storage_loss_factor = combined_rel_storage_loss_factor.stack()
        combined_rel_storage_loss_factor.sort_index(inplace=True)

        return combined_rel_storage_loss_factor.sort_index()

class TransshipmentProcess(ConnectionProcess):
    """ This is the class for transshipment processes.
    It inherits from connection process.

    Args:
        process_path (Path): The path to a process directory.

    """

    DIRECTIONS_TRANSSHIPMENT        = {None: ["forward", "backward"]}
    DIRECTION_FACTOR_TRANSSHIPMENT  = {"forward": 1, "backward": -1}

    processes           = []
    construction_years  = []

    def __init__(self, process_path: Path):
        """See class documentation above for more information on the initialization."""

        logging.debug("Initialize a new instance of TransshipmentProcess.")

        super(TransshipmentProcess, self).__init__(process_path)

        if process_path == None:
            self._existing_capacity.columns = ["connection", "2016", "unit", "comments"]
        else:
            pass

        TransshipmentProcess.processes.append(self)

    def get_transshipment_efficiency(self):
        """Return a dataframe with the efficiency of every connection for a transshipment process."""

        specific_efficiency = self.get_numerical_property("transshipment efficiency")
        efficiency_exponent = self.get_numerical_property("efficiency exponent")

        transshipment_efficiency = pd.DataFrame(Grid.selected().connections["distance"])
        transshipment_efficiency["efficiency"] = transshipment_efficiency.apply(lambda connection: specific_efficiency ** (efficiency_exponent * connection.distance).to("dimensionless").magnitude, axis=1)

        transshipment_efficiency["process"] = self.name
        transshipment_efficiency.drop(labels=["distance"], axis=1, inplace=True)

        transshipment_efficiency.reset_index(inplace=True)
        transshipment_efficiency.set_index(["process", "connection"], inplace=True)

        return transshipment_efficiency

    def get_potential_capacity(self, locations: list, invest_years: list):
        """Return potential capacity.

        """

        column_year = {}
        for invest_year in invest_years:
            column_year[invest_year] = str(max(invest_year, int(min(self._potential_capacity.columns))))

        potential_capacity = self._potential_capacity.reindex(index=locations, columns= [str(column_year[invest_year]) for invest_year in invest_years] + ["unit"])
        potential_capacity["process"] = self.name
        potential_capacity.fillna(np.inf, inplace=True)         
        potential_capacity.reset_index(inplace=True)
        potential_capacity.set_index(["connection", "process"], inplace=True)
        potential_capacity.sort_index(inplace=True)
        potential_capacity.fillna(np.inf, inplace=True)
        potential_capacity.columns = invest_years + ["unit"]
        for invest_year in invest_years:
            potential_capacity[invest_year] = potential_capacity[invest_year] * potential_capacity["unit"].apply(lambda unit: units(str(unit)))
        potential_capacity.drop(labels=["unit"], axis=1, inplace=True)
        return potential_capacity

    @classmethod
    def get_combined_transshipment_efficiency(cls):
        """Return a dataframe with the efficiencies of every connection for every transshipment process."""

        combined_transshipment_efficiency = pd.DataFrame()
        combined_transshipment_efficiency_list = []
        for process in progress.bar(cls.processes):
            combined_transshipment_efficiency_list.append(copy.deepcopy(process.get_transshipment_efficiency()))

        if len(combined_transshipment_efficiency_list) != 0:
            combined_transshipment_efficiency = combined_transshipment_efficiency.append(other=combined_transshipment_efficiency_list, verify_integrity=True, sort=True)

        return combined_transshipment_efficiency.sort_index()

    @classmethod
    def get_combined_transhipment_products(cls):
        """Returns a dictionary with the names and products of all transshipment processes"""

        combined_transshipment_products = {}
        
        for process in progress.bar(cls.processes):
            combined_transshipment_products[process.name] = process._properties.at["product", "value"]

        return combined_transshipment_products


class TransmissionProcess(ConnectionProcess):
    """ This is the class for transmission processes.
    It inherits from connection process.

    Args:
        process_path (Path): The path to a process directory.
    """

    PRODUCTS = {None: ["electricity"]}

    processes = []
    construction_years = []

    per_unit_base = units("500 MVA")
    reference_node = 1

    def __init__(self, process_path: Path):
        """See class documentation above for more information on the initialization."""

        logging.debug("Initialize a new instance of TransmissionProcess.")

        super(TransmissionProcess, self).__init__(process_path)

        if process_path == None:
            self._existing_capacity.columns = ["circuits", "comments"]
        else:
            pass

        TransmissionProcess.processes.append(self)

    def get_potential_capacity(self, locations: list, invest_years: list):
        """Returns potential capacity

        """

        column_year = {}
        for invest_year in invest_years:
            column_year[invest_year] = str(max(invest_year, int(min(self._potential_capacity.columns))))

        potential_capacity = self._potential_capacity.reindex(index=locations, columns= [str(column_year[invest_year]) for invest_year in invest_years] + ["unit"])
        
        if self._properties.at["process type", "value"] == "power line":
            potential_capacity["process"] = self.name
            potential_capacity.fillna(np.inf, inplace=True)         
        elif self._properties.at["process type", "value"] == "voltage switch":
            if potential_capacity.isnull().values.any:
                base_voltage = units(str(self._properties.at["base voltage", "value"]) + " " + str(self._properties.at["base voltage", "unit"]))
                base_voltage_process = TransmissionProcess._get_process_by_voltage(base_voltage)
                base_voltage_existing_capacity = base_voltage_process.get_existing_capacity(locations, invest_years)
                construction_years = [column for column in base_voltage_existing_capacity.columns if (secmod.helpers.isInteger(column))]
                base_voltage_existing_capacity["total existing circuits"] = 0
                for construction_year in construction_years:
                    base_voltage_existing_capacity["total existing circuits"] += base_voltage_existing_capacity[construction_year]

                nan_matrix = potential_capacity[[str(column_year[invest_year]) for invest_year in invest_years]].isnull().values
                
                potential_capacity = potential_capacity[[str(column_year[invest_year]) for invest_year in invest_years]].fillna(0) + nan_matrix.astype(float) * base_voltage_existing_capacity["total existing circuits"].unstack("invest year").values

                potential_capacity["process"]   = self.name
                potential_capacity["unit"]      = 1

        potential_capacity.reset_index(inplace=True)
        potential_capacity.set_index(["connection", "process"], inplace=True)
        potential_capacity.sort_index(inplace=True)
        potential_capacity.fillna(np.inf, inplace=True)
        potential_capacity.columns = invest_years + ["unit"]
        for invest_year in invest_years:
            potential_capacity[invest_year] = potential_capacity[invest_year] * potential_capacity["unit"].apply(lambda unit: units(str(unit)))
        potential_capacity.drop(labels=["unit"], axis=1, inplace=True)
        return potential_capacity

    def _get_process_connection_properties(self, connections: list, invest_years: list):
        """Returns connection properties, such as circuits, power limit, safety margin, and more.

        """

        properties = []

        existing_capacity = self.get_existing_capacity(connections, invest_years)
        construction_years = [column for column in existing_capacity.columns if (secmod.helpers.isInteger(column))]
        existing_capacity["total existing circuits"] = 0
        for construction_year in construction_years:
            existing_capacity["total existing circuits"] += existing_capacity[construction_year]

        for connection in connections:
            connection_properties = {}
            connection_properties["connection"] = connection

            for invest_year in invest_years:
                connection_properties["invest year"] = invest_year

                if not (existing_capacity.at[(connection, self.name, invest_year), "total existing circuits"].magnitude == 0):
                    if self._properties.at["process type", "value"] == "power line":
                        connection_properties["power limit"] = calculate_power_line_power_limit(self.get_numerical_property("power limit"),
                            self.get_numerical_property("safety margin"), existing_capacity.at[(connection, self.name, invest_year), "total existing circuits"])
                        
                        connection_properties["resistance per unit"] = calculate_power_line_resistance_per_unit(self.get_numerical_property("specific resistance"),
                            Grid.selected().connections.at[connection, "distance"], existing_capacity.at[(connection, self.name, invest_year), "total existing circuits"],
                            self.get_numerical_property("voltage"), TransmissionProcess.per_unit_base)

                        connection_properties["reactance per unit"] = calculate_power_line_resistance_per_unit(self.get_numerical_property("specific reactance"),
                            Grid.selected().connections.at[connection, "distance"], existing_capacity.at[(connection, self.name, invest_year), "total existing circuits"],
                            self.get_numerical_property("voltage"), TransmissionProcess.per_unit_base)

                        connection_properties["susceptance per unit"] = calculate_power_line_susceptance_per_unit(connection_properties["reactance per unit"],
                            connection_properties["resistance per unit"])

                    elif self._properties.at["process type", "value"] == "voltage switch":
                        base_voltage_process = TransmissionProcess._get_process_by_voltage(self.get_numerical_property("base voltage"))
                        base_voltage = base_voltage_process.get_numerical_property("voltage")
                        target_voltage_process = TransmissionProcess._get_process_by_voltage(self.get_numerical_property("target voltage"))
                        target_voltage = target_voltage_process.get_numerical_property("voltage")

                        connection_properties["power limit"] = calculate_voltage_switch_power_limit(
                            target_voltage_process.get_numerical_property("power limit"), base_voltage_process.get_numerical_property("power limit"),
                            self.get_numerical_property("safety margin"), existing_capacity.at[(connection, self.name, invest_year), "total existing circuits"])

                        connection_properties["resistance per unit"] = calculate_voltage_switch_resistance_per_unit(target_voltage, base_voltage,
                            target_voltage_process.get_numerical_property("specific resistance"), base_voltage_process.get_numerical_property("specific resistance"),
                            TransmissionProcess.per_unit_base, existing_capacity.at[(connection, self.name, invest_year), "total existing circuits"],
                            Grid.selected().connections.at[connection, "distance"])

                        connection_properties["reactance per unit"] = calculate_voltage_switch_resistance_per_unit(target_voltage, base_voltage,
                            target_voltage_process.get_numerical_property("specific reactance"), base_voltage_process.get_numerical_property("specific reactance"),
                            TransmissionProcess.per_unit_base, existing_capacity.at[(connection, self.name, invest_year), "total existing circuits"],
                            Grid.selected().connections.at[connection, "distance"])

                        connection_properties["susceptance per unit"] = calculate_power_line_susceptance_per_unit(connection_properties["reactance per unit"],
                            connection_properties["resistance per unit"])
                else:
                    connection_properties["power limit"] = 0
                    connection_properties["resistance per unit"] = np.inf
                    connection_properties["reactance per unit"] = np.inf
                    connection_properties["susceptance per unit"] = 0

                properties.append(copy.deepcopy(connection_properties))

        properties = pd.DataFrame(properties)

        properties.set_index(["connection", "invest year"], inplace=True)
        properties.sort_index(inplace=True)

        return properties

    @staticmethod
    def _get_process_by_voltage(voltage: pint.quantity):
        """Returns processes sorted by voltage.

        """

        for process in TransmissionProcess.processes:
            if process._properties.at["process type", "value"] == "power line":
                if units(str(process._properties.at["voltage", "value"]) + " " + str(process._properties.at["voltage", "unit"])) == voltage:
                    return process
        
        return None

    @staticmethod
    def get_connection_properties():
        """Returns connection properties.

        """
        connection_properties = pd.DataFrame
        for process in progress.bar(TransmissionProcess.processes):
            process_properties = process._get_process_connection_properties(TransmissionProcess.locations, ProcessImpacts.invest_years)
            if connection_properties.empty:
                connection_properties = process_properties
            else:
                connection_properties["power limit"] += process_properties["power limit"]
                connection_properties["resistance per unit"] = (
                    (((connection_properties["resistance per unit"])**(-1) + (process_properties["resistance per unit"]) ** (-1)).replace(0, np.nan) **(-1)).fillna(np.inf))
                connection_properties["reactance per unit"] = (
                    (((connection_properties["reactance per unit"])**(-1) + (process_properties["reactance per unit"]) ** (-1)).replace(0, np.nan) **(-1)).fillna(np.inf))
                connection_properties["susceptance per unit"] = (
                    connection_properties["reactance per unit"] / (
                        connection_properties["resistance per unit"] ** 2 + connection_properties["reactance per unit"] ** 2))
        
        return connection_properties

    @staticmethod
    def get_combined_power_limit_per_circuit():
        """Returns a dictionary of the power limits per circuit of all transmission processes."""

        combined_power_limit = {}
        for process in TransmissionProcess.processes:
            if process._properties.at["process type", "value"] == "power line":
                combined_power_limit[process.name] = process.get_numerical_property("power limit")
            elif process._properties.at["process type", "value"] == "voltage switch":
                base_voltage_process   = TransmissionProcess._get_process_by_voltage(process.get_numerical_property("base voltage"))
                target_voltage_process = TransmissionProcess._get_process_by_voltage(process.get_numerical_property("target voltage"))
                combined_power_limit[process.name] = target_voltage_process.get_numerical_property("power limit") - base_voltage_process.get_numerical_property("power limit")
        
        return combined_power_limit

    @staticmethod
    def get_combined_safety_margin():
        """Returns a dictionary of the safety margin of all transmission processes."""

        combined_safety_margin = {}
        for process in progress.bar(TransmissionProcess.processes):
            combined_safety_margin[process.name] = process.get_numerical_property("safety margin")
        
        return combined_safety_margin

class ProductionProcessEcoinvent(ProductionProcess, EcoinventImpacts):
    """ This is the class for ecoinvent production processes.
    It inherits from ProductionProcess and EcoinventImpacts.

    Args:
        process_path (Path): The path to a process directory.
    """

    def __init__(self, process_path: Path):
        """See class documentation above for more information on the initialization."""

        logging.debug(
            "Initialize a new instance of ProductionProcessEcoinvent.")

        super(ProductionProcessEcoinvent, self).__init__(process_path)


class ProductionProcessManual(ProductionProcess, ManualImpact):
    """ This is the class for manually defined production processes.
    It inherits from ProductionProcess and ManualImpact.

    Args:
        process_path (Path): The path to a process directory.
    """

    def __init__(self, process_path: Path):
        """See class documentation above for more information on the initialization."""

        logging.debug("Initialize a new instance of ProductionProcessManual.")

        super(ProductionProcessManual, self).__init__(process_path)


class StorageProcessEcoinvent(StorageProcess, EcoinventImpacts):
    """ This is the class for ecoinvent storage processes.
    It inherits from StorageProcess and Ecoinvent Impacts.

    Args:
        process_path (Path): The path to a process directory.

    """

    def __init__(self, process_path: Path):
        """See class documentation above for more information on the initialization."""

        logging.debug("Initialize a new instance of StorageProcessEcoinvent.")

        super(StorageProcessEcoinvent, self).__init__(process_path)


class StorageProcessManual(StorageProcess, ManualImpact):
    """ This is the class for manually defined storage processes.
    It inherits from StorageProcess and ManualImpact.

    Args:
        process_path (Path): The path to a process directory.
    """

    def __init__(self, process_path: Path):
        """See class documentation above for more information on the initialization."""

        logging.debug("Initialize a new instance of StorageProcessManual.")

        super(StorageProcessManual, self).__init__(process_path)


class TransshipmentProcessEcoinvent(TransshipmentProcess, EcoinventImpacts):
    """ This is the class for ecoinvent transshipment processes.
    It inherits from TransshipmentProcess and EcoinventImpacts.

    Args:
        process_path (Path): The path to a process directory.
    """

    def __init__(self, process_path: Path):
        """See class documentation above for more information on the initialization."""

        logging.debug(
            "Initialize a new instance of TransshipmentProcessEcoinvent.")

        super(TransshipmentProcessEcoinvent, self).__init__(process_path)


class TransshipmentProcessManual(TransshipmentProcess, ManualImpact):
    """ This is the class for manually defined transshipment processes.
    It inherits from TransshipmentProcess and Ecoinvent Impacts.

    Args:
        process_path (Path): The path to a process directory.
    """

    def __init__(self, process_path: Path):
        """See class documentation above for more information on the initialization."""

        logging.debug(
            "Initialize a new instance of TransshipmentProcessManual.")

        super(TransshipmentProcessManual, self).__init__(process_path)


class TransmissionProcessEcoinvent(TransmissionProcess, EcoinventImpacts):
    """ This is the class for ecoinvent transmission processes.
    It inherits from TransmissionProcess and EcoinventImpacts.

    Args:
        process_path (Path): The path to a process directory.

    """

    def __init__(self, process_path: Path):
        """See class documentation above for more information on the initialization."""

        logging.debug(
            "Initialize a new instance of TransmissionProcessEcoinvent.")

        super(TransmissionProcessEcoinvent, self).__init__(process_path)


class TransmissionProcessManual(TransmissionProcess, ManualImpact):
    """ This is the class for manually-defined tranmission processes.
    It inherits from TransmissionProcess and EcoinventImpacts.

    Args:
        process_path (Path): The path to a process directory.

    """

    def __init__(self, process_path: Path):
        """See class documentation above for more information on the initialization."""

        logging.debug(
            "Initialize a new instance of TransmissionProcessManual.")

        super(TransmissionProcessManual, self).__init__(process_path)


# Helper functions for pint
@units.wraps(units.MW, [units.MW, units.dimensionless, units.circuits], False)
def calculate_power_line_power_limit(power_limit, safety_margin, circuits):
    return (power_limit * (1 - safety_margin) * circuits)

@units.wraps(units.MW * units.ohm / units.kV ** 2, [units.ohm / units.km, units.km, units.circuits, units.kV, units.MVA])
def calculate_power_line_resistance_per_unit(specific_resistance, distance, circuits, voltage, per_unit_base):
    return(specific_resistance * distance / circuits / ((voltage ** 2) / per_unit_base))

@units.wraps(units.kV ** 2 / (units.MW * units.ohm), [units.MW * units.ohm / units.kV ** 2, units.MW * units.ohm / units.kV ** 2])
def calculate_power_line_susceptance_per_unit(reactance_per_unit, resistance_per_unit):
    return (reactance_per_unit / (resistance_per_unit ** 2 + reactance_per_unit ** 2))

@units.wraps(units.MW, [units.MW, units.MW, units.dimensionless, units.circuits], False)
def calculate_voltage_switch_power_limit(target_power_limit, base_power_limit, safety_margin, circuits):
    return ((target_power_limit - base_power_limit) * (1 - safety_margin) * circuits)

@units.wraps(units.MW * units.ohm / units.kV ** 2, [units.kV, units.kV, units.ohm / units.km, units.ohm / units.km, units.MVA, units.circuits, units.km])
def calculate_voltage_switch_resistance_per_unit(target_voltage, base_voltage, target_specific_resistance, base_specific_resistance, per_unit_base, circuits, distance):
    return((target_voltage ** 2 / target_specific_resistance - base_voltage ** 2 / base_specific_resistance) ** (-1)
        * per_unit_base / circuits * distance)
