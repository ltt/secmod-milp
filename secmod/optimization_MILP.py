# import namespace for abstract classes
import abc
import logging

# import necessary elements of pyomo - alternatively use "from pyomo.environ import *", if you are missing something
import pyomo.environ as pe
from pyomo.opt import SolverStatus, TerminationCondition

import pandas as pd
from secmod.classes import config

class Optimization_MILP(abc.ABC):
    """This is the class for optimization models.

    It includes all declarations of Sets, Parameters, Variables and Constraints. 
    Furthermore it contains all optimization methods.
    """
    
    def __init__(self):
        """This method initializes the model as an abstract pyomo model.
        
        Hence, all necessary pe.Sets, pe.Params (including scaling parameters), pe.Vars,
        pe.Constraints as well as the Objective Function are setup here
        """

        # create Pyomo AbstractModel
        self.model = pe.AbstractModel()
        
        # SETS # Declaration of sets used in the optimization
        self.setupSets()

        # PARAMETERS # Declaration of parameters used in the optimization
        self.setupParameters()

        # VARIABLES # Declaration of variables used in the optimization
        self.setupVariables()
        
        # CONSTRAINTS # Definition of constraints used in the optimization
        self.setupConstraints()
    
        # pe.Objective
        self.setupObjective()

    #### SETUP
    def setupSets(self):
        """This method sets up all Sets required by all optimization models.

        Some Sets are initialized with default values, if no other values are specified 
        in the input file."""

        # pe.Set of the nodes of the grid
        self.model.nodes = pe.Set(
            within=pe.NonNegativeIntegers)
        # pe.Set of the connections between the nodes of the grid
        self.model.connections = pe.Set(
            within=pe.NonNegativeIntegers)
        # pe.Set of two nodes to identify both nodes of a connection
        self.model.connection_nodes = pe.Set(
            initialize={"node1", "node2"})
        # pe.Set of the used products for which there is a demand
        self.model.products = pe.Set()
        # pe.Set of the used processes for production
        self.model.processes_production = pe.Set()
        # pe.Set of the used processes for storage of products
        self.model.processes_storage = pe.Set()
        # pe.Set of the used processes for transshipment of products between nodes
        self.model.processes_transshipment = pe.Set()
        # pe.Set of the used processes for transshipment of products between nodes
        self.model.processes_transmission = pe.Set()
        # pe.Set of storage directions (deposit and withdraw)
        self.model.directions_storage = pe.Set(
            initialize={"deposit", "withdraw"})
        # pe.Set of transshipment direction (forward and backward)
        self.model.directions_transshipment = pe.Set(
           initialize={"forward", "backward"})
        # pe.Set of the properties of a transmission process/power line
        self.model.power_line_property_categories = pe.Set(
            initialize={
                "power limit",
                "susceptance per unit"})
        # pe.Set of products using transmission via DC load flow
        self.model.products_transmission = pe.Set(
            initialize={"electricity"})
        # pe.Set of the years of construction of existing capacity production processes
        self.model.construction_years_production = pe.Set(
            within=pe.NonNegativeIntegers)
        # pe.Set of the years of construction of existing capacity storage processes
        self.model.construction_years_storage = pe.Set(
            within=pe.NonNegativeIntegers)
        # pe.Set of the years of construction of existing capacity transshipment processes
        self.model.construction_years_transshipment = pe.Set(
            within=pe.NonNegativeIntegers)
        # pe.Set of the years of construction of existing capacity transmission processes
        self.model.construction_years_transmission = pe.Set(
            within=pe.NonNegativeIntegers)
        # pe.Set of the years in the current optimization period
        self.model.years = pe.Set(
            within=pe.NonNegativeIntegers)
        # pe.Set of the time slices of a year
        self.model.time_slices = pe.Set(
            within=pe.NonNegativeIntegers)
        # pe.Set of the impact categories, including environmental and monetary costs
        self.model.impact_categories = pe.Set()
        # pe.Set of the impact sources, e.g. whether the impacts result from invest or operation
        self.model.impact_sources = pe.Set(
            initialize={"operation", "invest"})
        # pe.Set of vertices
        self.model.vertices = pe.Set()

    def setupParameters(self):
        """This method sets up all Parameters required by all optimization models."""
        
        # length of the connections of the grid
        self.model.connection_length = pe.Param(
            self.model.connections,
            within=pe.NonNegativeReals,
            doc="Parameter which contains the length of all connections of the grid.")
        # technology matrix of storage processes like pumped hydro storage/heat storage
        self.model.efficiency_matrix_storage = pe.Param(
            self.model.products,
            self.model.processes_storage,
            self.model.directions_storage,
            self.model.construction_years_storage,
            default=0,
            doc="Parameter that models the efficiency of a storage process for charging and discharging.")
        # products stored by storage processes
        self.model.storage_products = pe.Param(
            self.model.processes_storage,
            within=self.model.products,
            doc="Parameter which contains the product stored by a storage process.")
        # minimal storage capacity
        self.model.minimal_capacity_storage = pe.Param(
            self.model.nodes,
            self.model.processes_storage,
            self.model.construction_years_storage,
            within=pe.NonNegativeReals,
            doc="Parameter describing the minimal storage capacity.")
        # relative minimal storage level
        self.model.min_rel_storage_level_factor = pe.Param(
            self.model.processes_storage,
            self.model.construction_years_storage,
            within=pe.NonNegativeReals,
            doc="Parameter describing the relative minimal storage level.")
        # relative energy loss storages  
        self.model.rel_storage_loss_factor = pe.Param(
            self.model.processes_storage,
            self.model.construction_years_storage,
            within=pe.NonNegativeReals,
            doc="Parameter describing the relative loss of storage level between to consecutive time steps")
        
        # efficiency of transshipment processes along all connections
        self.model.transshipment_efficiency = pe.Param(
            self.model.processes_transshipment,
            self.model.connections,
            default=0,
            doc="Parameter which contains the efficiency of a transshipment process along a connection.")
        # products which are transshipped by the corresponding transshipment processes
        self.model.transshipment_products = pe.Param(
            self.model.processes_transshipment,
            within=pe.Any,
            default=None,
            doc="Parameter which contains the transshipped product of each transshipment process.")
        # properties of each existing power line, regarding power limit and susceptance per unit
        self.model.power_line_properties = pe.Param(
            self.model.connections,
            self.model.years,
            self.model.power_line_property_categories,
            within=pe.NonNegativeReals,
            default=0,
            doc="Parameter which contains the power limit and susceptance per unit of the existing capacity.")

        # timeseries multiplier of the technology matrix of production processes
        self.model.technology_matrix_production_timeseries = pe.Param(
            self.model.products,
            self.model.processes_production,
            self.model.time_slices,
            default=1,
            doc="Parameter which contains the time series factor for the technology matrix of a production process.")
        
        # impact matrices, which define how big the life cycle impact of any process is - includes monetary costs
        # impact matrix of production processes
        self.model.impact_matrix_production = pe.Param(
            self.model.impact_categories,
            self.model.impact_sources,
            self.model.processes_production,
            self.model.years,
            self.model.construction_years_production,
            doc="Parameter which contains the data about the impacts of a production process. \
                First 'year' marks the year for which the value is valid. Second 'year' marks the \
                year of construction.")
        # impact matrix of storage processes
        self.model.impact_matrix_storage = pe.Param(
            self.model.impact_categories,
            self.model.impact_sources,
            self.model.processes_storage,
            self.model.years,
            self.model.construction_years_storage,
            doc="Parameter which contains the data about the impacts of a storage process. \
                Operational impact data is based on the **withdrawn** product. \
                First 'year' marks the year for which the value is valid. Second 'year' marks the \
                year of construction.")
        # # impact matrix of transshipment processes
        self.model.impact_matrix_transshipment = pe.Param(
            self.model.impact_categories,
            self.model.impact_sources,
            self.model.processes_transshipment,
            self.model.years,
            self.model.construction_years_transshipment,
            doc="Parameter which contains the data about the impacts of a transshipment process. \
                First 'year' marks the year for which the value is valid. Second 'year' marks the \
                year of construction.")
        # impact matrix of transmssion processes
        self.model.impact_matrix_transmission = pe.Param(
            self.model.impact_categories,
            self.model.impact_sources,
            self.model.processes_transmission,
            self.model.years,
            self.model.construction_years_transmission,
            doc="Parameter which contains the data about the impacts of transmission processes \
                in the transmission grid. First 'year' marks the year for which the value is valid. \
                Second 'year' marks the year of construction.")
        # impact matrix of non-served demand of a product
        self.model.impact_matrix_non_served_demand = pe.Param(
            self.model.impact_categories,
            self.model.products,
            self.model.years,
            self.model.time_slices,
            doc="Parameter which contains the data about the impacts of non-served demand/purchased products.")
        # impact matrix of add-served demand of a product (impact of sold products)
        self.model.impact_matrix_add_demand = pe.Param(
            self.model.impact_categories,
            self.model.products,
            self.model.years,
            self.model.time_slices,
            doc="Parameter which contains the data about the impacts of add-served demand/sold products.")

        # lifetime duration of production process
        self.model.lifetime_duration_production = pe.Param(
            self.model.processes_production,
            self.model.construction_years_production,
            within=pe.NonNegativeReals,
            doc="Parameter which contains the data about the lifetime duration of each production process\
                in the corresponding construction year.")
        # lifetime duration of storage process
        self.model.lifetime_duration_storage = pe.Param(
            self.model.processes_storage,
            self.model.construction_years_storage,
            within=pe.NonNegativeReals,
            doc="Parameter which contains the data about the lifetime duration of each storage process\
                in the corresponding construction year.")
        # lifetime duration of transshipment process
        self.model.lifetime_duration_transshipment = pe.Param(
            self.model.processes_transshipment,
            self.model.construction_years_transshipment,
            within=pe.NonNegativeReals,
            doc="Parameter which contains the data about the lifetime duration of each production process\
                in the corresponding construction year.")
        # lifetime duration of transmission process
        self.model.lifetime_duration_transmission = pe.Param(
            self.model.processes_transmission,
            self.model.construction_years_transmission,
            within=pe.NonNegativeReals,
            doc="Parameter which contains the data about the lifetime duration of each production process\
                in the corresponding construction year.")
        
        # existing capacity of all technologies
        # installed capacity of production technologies based on nodes and investment years
        self.model.existing_capacity_production = pe.Param(
            self.model.nodes,
            self.model.processes_production,
            self.model.years,
            self.model.construction_years_production,
            within=pe.NonNegativeReals,
            default=0,
            doc="Parameter which contains the data about the existing capacity of a production process. \
                First 'year' marks the year for which the value is valid. Second 'year' marks the \
                year of construction.")
        # installed capacity of storage technologies based on nodes and investment years
        self.model.existing_capacity_storage = pe.Param(
            self.model.nodes,
            self.model.processes_storage,
            self.model.years,
            self.model.construction_years_storage,
            default=0,
            within=pe.NonNegativeReals,
            doc="Parameter which contains the data about the existing capacity of a storage process. \
                First 'year' marks the year for which the value is valid. Second 'year' marks the \
                year of construction.")
        # installed capacity of transshipment technologies based on nodes and investment years
        self.model.existing_capacity_transshipment = pe.Param(
            self.model.connections,
            self.model.processes_transshipment,
            self.model.years,
            self.model.construction_years_transshipment,
            default=0,
            within=pe.NonNegativeReals,
            doc="Parameter which contains the data about the existing capacity of a transshipment process. \
                First 'year' marks the year for which the value is valid. Second 'year' marks the \
                year of construction.")
        # installed capacity of transshipment technologies based on nodes and investment years
        self.model.existing_capacity_transmission = pe.Param(
            self.model.connections,
            self.model.processes_transmission,
            self.model.years,
            self.model.construction_years_transmission,
            default=0,
            within=pe.NonNegativeReals,
            doc="Parameter which contains the data about the existing capacity of a transshipment process. \
                First 'year' marks the year for which the value is valid. Second 'year' marks the \
                year of construction.")
        # product flow to storage capacity factor based on processes and investment years
        self.model.flow_to_storage_capacity_factor = pe.Param(
            self.model.processes_storage,
            self.model.construction_years_storage,
            within=pe.NonNegativeReals,
            doc="Parameter which contains the data about the relation between the maximum input \
                and output (power) of a storage process and its storage capacity.")
        
        # capacity limits of all processes on nodal base
        # capacity limits of production processes
        self.model.potential_capacity_production = pe.Param(
            self.model.nodes,
            self.model.processes_production,
            self.model.years,
            within=pe.NonNegativeReals,
            default=float("inf"),
            doc="Parameter which contains the data about the capacity limit of a production process at each node.")
        # capacity limits of storage processes
        self.model.potential_capacity_storage = pe.Param(
            self.model.nodes,
            self.model.processes_storage,
            self.model.years,
            within=pe.NonNegativeReals,
            default=float("inf"),
            doc="Parameter which contains the data about the capacity limit of a storage process at each node.")
        # capacity limits of transshipment processes
        self.model.potential_capacity_transshipment = pe.Param(
            self.model.connections,
            self.model.processes_transshipment,
            self.model.years,
            within=pe.NonNegativeReals,
            default=float("inf"),
            doc="Parameter which contains the data about the capacity limit of a transshipment process at each node.")
        # capacity limits of transmission processes
        self.model.potential_capacity_transmission = pe.Param(
            self.model.connections,
            self.model.processes_transmission,
            self.model.years,
            within=pe.NonNegativeReals,
            default=float("inf"),
            doc="Parameter which contains the data about the capacity limit of a transmission process at each connection.")
        
        
        # lists of nodes connected by connections
        self.model.connected_nodes = pe.Param(
            self.model.connections,
            self.model.connection_nodes,
            within=pe.NonNegativeIntegers,
            doc="Parameter which contains the two nodes connected by a connection process.")
        # reference node of which the phase difference is pe.Set to zero
        self.model.transmission_reference_node = pe.Param(
            within=self.model.nodes,
            doc="Parameter which contains one node which is used as reference node for the \
                calculation of the phase difference between the nodes. Therefore, its \
                phase difference is set to zero.")
        # apparent power base value for the per-unit calculations
        self.model.per_unit_base = pe.Param(
            within=pe.NonNegativeReals,
            default=500,
            doc="Parameter which contains the base value in MVA for the per-unit calculations.")
        # time dependent parameters
        # weight of every time slice regarding the full year - sum of all shares equals 8760 hours
        self.model.time_slice_yearly_weight = pe.Param(
            self.model.time_slices,
            within=pe.NonNegativeReals,
            doc="Parameter which contains the data about the share of a year represented by each time slice.")
        # actual duration of one time slice - necessary for storage level calculations
        self.model.time_slice_duration = pe.Param(
            self.model.time_slices,
            within=pe.NonNegativeReals,
            doc="Parameter which contains the data about the duration of each time slice.")
        # next connected time slice of the current time slice, used for storage level boundaries
        self.model.next_time_slice = pe.Param(
            self.model.time_slices,
            doc="Parameter which contains the data about the connections between the time slices.")
        # timeseries for the available share of any capacity e.g. production from renewable energy
        self.model.usable_capacity_factor_timeseries_production = pe.Param(
            self.model.nodes,
            self.model.processes_production,
            self.model.time_slices,
            within=pe.NonNegativeReals,
            default=1,
            doc="Parameter which contains the data about the available share of capacity of a production \
                process in a specific time slice.")
        # demand for every product
        self.model.demand = pe.Param(
            self.model.products,
            self.model.years,
            self.model.time_slices,
            self.model.nodes,
            default=0,
            doc="Parameter which contains the data about the demand for each product at each node.")
        
        # parameters regarding the impact of the system
        # limits for operational impact
        # nodal limits for the operational impacts
        self.model.operational_nodal_impact_limits = pe.Param(
            self.model.nodes,
            self.model.impact_categories,
            self.model.years,
            default=float("inf"),
            doc="Parameter which contains all limits for the operational impacts on nodal base.")
        # overall limits for the operational impacts
        self.model.operational_impact_limits = pe.Param(
            self.model.impact_categories,
            self.model.years,
            default=float("inf"),
            doc="Parameter which contains all limits for the operational impacts overall.")
        # limits for invest impact
        # nodal limits for the invest impacts
        self.model.invest_nodal_impact_limits = pe.Param(
            self.model.nodes,
            self.model.impact_categories,
            self.model.years,
            default=float("inf"),
            doc="Parameter which contains all limits for the invest impacts on nodal base.")
        # overall limits for the invest impacts
        self.model.invest_impact_limits = pe.Param(
            self.model.impact_categories,
            self.model.years,
            default=float("inf"),
            doc="Parameter which contains all limits for the invest impacts overall.")
        # limits for total impact
        # nodal limits for the total impacts
        self.model.total_nodal_impact_limits = pe.Param(
            self.model.nodes,
            self.model.impact_categories,
            self.model.years,
            default=float("inf"),
            doc="Parameter which contains all limits for the total impacts on nodal base.")
        # overall limits for the total impacts
        self.model.total_impact_limits = pe.Param(
            self.model.impact_categories,
            self.model.years,
            default=float("inf"),
            doc="Parameter which contains all limits for the total impacts overall.")
        # parameters which specify with which weight impacts is taken into account in the pe.Objective rule
        self.model.objective_factor_impacts = pe.Param(
            self.model.impact_sources,
            self.model.impact_categories,
            self.model.years,
            default=0,
            doc="Parameter which defines the factor used to multiply an impact category within the objective rule, \
                necessary, e.g. carry out multi-criteria optimizations")
        # parameters for the weight of overshoot and non-served demand variables
        # parameters which specify with which weight impact overshoots are taken into account in the pe.Objective rule
        self.model.objective_factor_impact_overshoot = pe.Param(
            self.model.impact_categories,
            self.model.years,
            doc="Parameters which specify with which weight impact overshoots are taken into account in the objective rule.")
        # parameter used for the transshipment in different directions. 
        # Therefore its values need to be {"forward": 1, "backward": -1}
        self.model.factor_transshipment = pe.Param(
            self.model.directions_transshipment,
            initialize={"forward": 1, "backward": -1},
            doc="Parameter which is used for transshipment equations and does not need further initialization.")
        # additional parameter to be used for the calculation of the impact of storage usage on the storage level
        # therefore its values need to be {"withdraw": -1, "deposit": 1}
        self.model.storage_level_factor = pe.Param(
            self.model.directions_storage,
            initialize={"withdraw": -1, "deposit": 1},
            doc="Parameter which is used for storage equations and does not need further initialization. Takes -1 for withdraw and 1 for deposit.")
        
        # number of cornerpoints, number of discretized corner point per production process
        # Note that that there are always at least two cornerpoints (at least start and endpoint of discretized line,
        # no matter the process can be switched off or not) 
        self.model.cornerpoints_number = pe.Param(
            self.model.processes_production,
            doc="Parameter for the number of discretized corner points per production process")
        
        # gradient of technology matrix
        self.model.technology_matrix_production_gradient = pe.Param(
            self.model.vertices,
            self.model.products,
            self.model.processes_production,
            self.model.construction_years_production,
            doc="Parameter which contains the data about the gradient between two cornerpoints/in a section for each production process")
        
        # technology matrices, which define how much of any product is produces by a process
        # technology matrix of production processes like power plant processes
        self.model.technology_matrix_production = pe.Param(
            self.model.vertices,
            self.model.products,
            self.model.processes_production,
            self.model.construction_years_production,
            default=0,
            doc="Parameter which contains the data about the product conversion at each corner point for each production process.")
        
        # relative fraction of full process   
        self.model.processes_rel_partload_capacity = pe.Param(
            self.model.vertices,
            self.model.processes_production,
            self.model.construction_years_production,
            doc="Parameter which contains the data about the relative capacity to be used in a part-load segment.")
        
        # minimal capacity to be built in a production process
        self.model.minimal_capacity_production = pe.Param(
            self.model.nodes,
            self.model.processes_production,
            self.model.construction_years_production,
            doc="Parameter which contains the data about minimal capacity to be built in a production process.")
        
        # power limit per circuit of each transmission process
        self.model.power_limit_per_circuit = pe.Param(
            self.model.processes_transmission,
            default=0,
            doc="Parameter which contains the power limit per circuit of each transmission process.")
        # reliability margin for a transmission process
        self.model.reliability_margin_transmission = pe.Param(
            self.model.processes_transmission,
            within=pe.NonNegativeReals,
            default=0,
            doc="Parameter which contains the data about the share of the power limit of a process that can be used.")
         
    def setupVariables(self):
        """This method sets up all Variables required by all optimization models."""

        # usage of production technologies based on nodes and time
        self.model.used_production = pe.Var(
            self.model.nodes,
            self.model.processes_production,
            self.model.years,
            self.model.construction_years_production,
            self.model.time_slices,
            self.model.vertices,
            within=pe.NonNegativeReals)
        # usage of storage technologies based on nodes and time
        self.model.used_storage = pe.Var(
            self.model.nodes,
            self.model.processes_storage,
            self.model.directions_storage,
            self.model.years,
            self.model.construction_years_storage,
            self.model.time_slices,
            within=pe.NonNegativeReals) 
        # usage of transshipment technologies based on nodes and time
        self.model.used_transshipment = pe.Var(
            self.model.connections,
            self.model.processes_transshipment,
            self.model.directions_transshipment,
            self.model.years,
            self.model.construction_years_transshipment,
            self.model.time_slices,
            within=pe.NonNegativeReals)
        # non-served demand/purchase of a product based on nodes and time
        self.model.non_served_demand = pe.Var(
            self.model.nodes,
            self.model.products,
            self.model.years,
            self.model.time_slices,
            #rule = lambda *args: self.fix_variables('non_served_demand',*args),
            within=pe.NonNegativeReals)
        # storage level of storage technologies based on nodes, years and time slices
        self.model.storage_level = pe.Var(
            self.model.nodes,
            self.model.processes_storage,
            self.model.years,
            self.model.construction_years_storage,
            self.model.time_slices,
            within=pe.NonNegativeReals)
         # existing storage
        self.model.existence_storage = pe.Var(
            self.model.nodes,
            self.model.processes_storage,
            self.model.construction_years_storage,
            within=pe.Binary)
        # storage deposit or withdraw
        self.model.storage_deposit_withdraw_decision = pe.Var(
            self.model.nodes,
            self.model.processes_storage,
            self.model.directions_storage,
            self.model.years,
            self.model.construction_years_storage,
            self.model.time_slices,
            within=pe.Binary)
        # DC load flow - transmission related variables
        self.model.phase_difference = pe.Var(
            self.model.nodes,
            self.model.years,
            self.model.time_slices,
            within=pe.Reals)
        # impact related variables
        # operational impact
        # nodal operational environmental and monetary impact based on impact categories and years
        self.model.operational_nodal_impact = pe.Var(
            self.model.nodes,
            self.model.impact_categories,
            self.model.years,
            within=pe.Reals)
        # overshoot of nodal operational environmental and monetary impact based on impact categories and years
        self.model.operational_nodal_impact_overshoot = pe.Var(
            self.model.nodes,
            self.model.impact_categories,
            self.model.years,
            within=pe.NonNegativeReals)
        # overall operational environmental and monetary impact based on impact categories and years
        self.model.operational_impact = pe.Var(
            self.model.impact_categories,
            self.model.years,
            within=pe.Reals)
        # overshoot of overall operational environmental and monetary impact based on impact categories and years
        self.model.operational_impact_overshoot = pe.Var(
            self.model.impact_categories,
            self.model.years,
            within=pe.NonNegativeReals)
        # invest impact
        # nodal invest environmental and monetary impact based on impact categories and years
        self.model.invest_nodal_impact = pe.Var(
            self.model.nodes,
            self.model.impact_categories,
            self.model.years,
            within=pe.Reals)
        # overshoot of nodal invest environmental and monetary impact based on impact categories and years
        self.model.invest_nodal_impact_overshoot = pe.Var(
            self.model.nodes,
            self.model.impact_categories,
            self.model.years,
            within=pe.NonNegativeReals)
        # overall invest environmental and monetary impact based on impact categories and years
        self.model.invest_impact = pe.Var(
            self.model.impact_categories,
            self.model.years,
            within=pe.Reals)
        # overshoot of overall invest environmental and monetary impact based on impact categories and years
        self.model.invest_impact_overshoot = pe.Var(
            self.model.impact_categories,
            self.model.years,
            within=pe.NonNegativeReals)
        # total impact
        # nodal total environmental and monetary impact based on impact categories and years
        self.model.total_nodal_impact = pe.Var(
            self.model.nodes,
            self.model.impact_categories,
            self.model.years,
            within=pe.Reals)
        # overshoot of nodal total environmental and monetary impact based on impact categories and years
        self.model.total_nodal_impact_overshoot = pe.Var(
            self.model.nodes,
            self.model.impact_categories,
            self.model.years,
            within=pe.NonNegativeReals)
        # overall total environmental and monetary impact based on impact categories and years
        self.model.total_impact = pe.Var(
            self.model.impact_categories,
            self.model.years,
            within=pe.Reals)
        # overshoot of overall total environmental and monetary impact based on impact categories and years
        self.model.total_impact_overshoot = pe.Var(
            self.model.impact_categories,
            self.model.years,
            within=pe.NonNegativeReals)
        
        # Variables related to MILP formulation
        # binary variable for modeling partload of production_processes
        self.model.used_production_binary = pe.Var(
            self.model.nodes,
            self.model.processes_production,
            self.model.years,
            self.model.construction_years_production,
            self.model.time_slices,
            self.model.vertices,
            within=pe.Binary)
        # positive contiuous auxiliary variable needed to reformulate bilinear term (c.f. Voll2013)
        self.model.xsi_production = pe.Var(
            self.model.nodes,
            self.model.processes_production,
            self.model.years,
            self.model.construction_years_production,
            self.model.time_slices,
            self.model.vertices,
            within=pe.NonNegativeReals)
        # additional variable for higher demand due to sold products
        self.model.add_demand = pe.Var(
            self.model.nodes,
            self.model.products,
            self.model.years,
            self.model.time_slices,
            within=pe.NonNegativeReals)
        
        # Introduce investment variables for the Production, Storage, Transmission, Transshipment
        # newly installed capacity of production technologies based on nodes and investment years
        self.model.new_capacity_production = pe.Var(
            self.model.nodes,
            self.model.processes_production,
            self.model.years,
            within=pe.NonNegativeReals)
        # newly installed capacity of storage technologies based on nodes and investment years
        self.model.new_capacity_storage = pe.Var(
            self.model.nodes,
            self.model.processes_storage,
            self.model.years,
            within=pe.NonNegativeReals)
        # # newly installed capacity of transshipment technologies based on nodes and investment years
        self.model.new_capacity_transshipment = pe.Var(
            self.model.connections,
            self.model.processes_transshipment,
            self.model.years,
            within=pe.NonNegativeReals)
        # newly installed transmission capacity from switching
        self.model.new_capacity_transmission = pe.Var(
            self.model.connections,
            self.model.processes_transmission,
            self.model.years,
            within=pe.NonNegativeReals)
        
    def setupConstraints(self):
        """
        This method sets up all Constraints required by all optimization models.
        """
        
        #### Products
        # declaring the balance equation for all products used in the optimization
        self.model.constraint_product_balance = pe.Constraint(
            self.model.nodes,
            self.model.products,
            self.model.years,
            self.model.time_slices,
            rule=self.constraint_product_balance_rule)
        # constrain maximum number of used binaries in part-load behaviour
        self.model.constraint_production_limit_binaries = pe.Constraint(
            self.model.nodes,
            self.model.processes_production,
            self.model.years,
            self.model.construction_years_production,
            self.model.time_slices,
            rule=self.constraint_production_limit_binaries_rule)
        # upper bound for production variable
        self.model.constraint_production_limit_by_capacity_upper = pe.Constraint(
            self.model.nodes,
            self.model.processes_production,
            self.model.years,
            self.model.construction_years_production,
            self.model.time_slices,
            self.model.vertices,
            rule = self.constraint_production_limit_by_capacity_upper_rule)
        # lower bound for production variable
        self.model.constraint_production_limit_by_capacity_lower = pe.Constraint(
            self.model.nodes,
            self.model.processes_production,
            self.model.years,
            self.model.construction_years_production,
            self.model.time_slices,
            self.model.vertices,
            rule = self.constraint_production_limit_by_capacity_lower_rule)
        #### Glover equations
        # constraints from glover reformulation (c.f. Voll2013)       
        self.model.constraint_production_glovers_1a = pe.Constraint(
            self.model.nodes,
            self.model.processes_production,
            self.model.years,
            self.model.construction_years_production,
            self.model.time_slices,
            self.model.vertices,
            rule = self.constraint_production_glovers_1a_rule)
        self.model.constraint_production_glovers_1b = pe.Constraint(
            self.model.nodes,
            self.model.processes_production,
            self.model.years,
            self.model.construction_years_production,
            self.model.time_slices,
            self.model.vertices,
            rule = self.constraint_production_glovers_1b_rule)
        self.model.constraint_production_glovers_2a = pe.Constraint(
            self.model.nodes,
            self.model.processes_production,
            self.model.years,
            self.model.construction_years_production,
            self.model.time_slices,
            self.model.vertices,
            rule = self.constraint_production_glovers_2a_rule)
        self.model.constraint_production_glovers_2b = pe.Constraint(
            self.model.nodes,
            self.model.processes_production,
            self.model.years,
            self.model.construction_years_production,
            self.model.time_slices,
            self.model.vertices,
            rule = self.constraint_production_glovers_2b_rule)
        #### Storage
        # declaring the limit of the storage level, limited by the storage capacity
        self.model.constraint_storage_level_limit_by_capacity = pe.Constraint(
            self.model.nodes,
            self.model.processes_storage,
            self.model.years,
            self.model.construction_years_storage,
            self.model.time_slices,
            rule=self.constraint_storage_level_limit_by_capacity_rule)
        # declaring the lower limit of the storage level, limited by the storage capacity
        self.model.constraint_storage_level_limit_by_capacity_lower = pe.Constraint(
            self.model.nodes,
            self.model.processes_storage,
            self.model.years,
            self.model.construction_years_storage,
            self.model.time_slices,
            rule=self.constraint_storage_level_limit_by_capacity_lower_rule)
        # declaring the limit of the used storage, limited by the storage capacity and flow-to-capacity factor
        self.model.constraint_used_storage_limit_by_capacity = pe.Constraint(
            self.model.nodes,
            self.model.processes_storage,
            self.model.directions_storage,
            self.model.years,
            self.model.construction_years_storage,
            self.model.time_slices,
            rule=self.constraint_used_storage_limit_by_capacity_rule)
        # declaring the limit of the capacity of storage processes by limits from the input e.g. maximum potential
        self.model.constraint_storage_potential_capacity_by_input = pe.Constraint(
            self.model.nodes,
            self.model.processes_storage,
            self.model.years,
            self.model.construction_years_storage,
            rule=self.constraint_storage_potential_capacity_by_input_rule)
        # declaring the limit of the capacity of storage processes by limits from the input e.g. minimal potential
        self.model.constraint_storage_minimal_capacity_by_input = pe.Constraint(
            self.model.nodes,
            self.model.processes_storage,
            self.model.years,
            self.model.construction_years_storage,
            rule=self.constraint_storage_minimal_capacity_by_input_rule)
        # decision rule for deposit / withdrawel of storage in each time step
        self.model.constraint_storage_deposit_withdraw_decision = pe.Constraint(
            self.model.nodes,
            self.model.processes_storage,
            self.model.directions_storage,
            self.model.years,
            self.model.construction_years_storage,
            self.model.time_slices,
            rule=self.constraint_storage_deposit_withdraw_decision_rule)
        # declaring the storage restriction per timeslice
        self.model.constraint_storage_restrict_storing_per_timeslice = pe.Constraint(
            self.model.nodes,
            self.model.processes_storage,
            self.model.years,
            self.model.construction_years_storage,
            self.model.time_slices,
            rule=self.constraint_storage_restrict_storing_per_timeslice_rule)
        # declaring the calculation of the storage level at the end of each time slice
        self.model.constraint_storage_level = pe.Constraint(
            self.model.nodes,
            self.model.processes_storage,
            self.model.years,
            self.model.construction_years_storage,
            self.model.time_slices,
            rule=self.constraint_storage_level_rule)
        
        #### Transshipment
        # declaring the limit of transshipment, limited by the capacity
        self.model.constraint_transshipment_limit_by_capacity = pe.Constraint(
            self.model.connections,
            self.model.processes_transshipment,
            self.model.directions_transshipment,
            self.model.years,
            self.model.construction_years_transshipment,
            self.model.time_slices,
            rule=self.constraint_transshipment_limit_by_capacity_rule)
        #### Transmission
        # constraints for DC load flow modelling of transmission
        # declaring the limit of positive transmission, limited by the capacity
        self.model.constraint_transmission_limit_by_capacity_positive = pe.Constraint(
            self.model.connections,
            self.model.years,
            self.model.time_slices,
            rule=self.constraint_transmission_limit_by_capacity_positive_rule)
        # declaring the limit of negative transmission, limited by the capacity
        self.model.constraint_transmission_limit_by_capacity_negative = pe.Constraint(
            self.model.connections,
            self.model.years,
            self.model.time_slices,
            rule=self.constraint_transmission_limit_by_capacity_negative_rule)
        
        #### impact calculation and limitation
        # operational impacts nodal based
        # declaring the calculation of all operational impact categories,
        # summed over all processes on nodal base
        self.model.constraint_operational_nodal_impact = pe.Constraint(
            self.model.nodes,
            self.model.impact_categories,
            self.model.years,
            rule=self.constraint_operational_nodal_impact_rule)
        # declaring the calculation of all operational impact category limits,
        # summed over all processes on nodal base
        self.model.constraint_operational_nodal_impact_limits = pe.Constraint(
            self.model.nodes,
            self.model.impact_categories,
            self.model.years,
            rule=self.constraint_operational_nodal_impact_limits_rule)
        # operational impacts over all nodes
        # declaring the calculation of all operational impact categories,
        # summed over all processes
        self.model.constraint_operational_impact = pe.Constraint(
            self.model.impact_categories,
            self.model.years,
            rule=self.constraint_operational_impact_rule)
        # declaring the calculation of all operational impact category limits,
        # summed over all processes
        self.model.constraint_operational_impact_limits = pe.Constraint(
            self.model.impact_categories,
            self.model.years,
            rule=self.constraint_operational_impact_limits_rule)
        # invest impacts nodal based
        # declaring the calculation of all invest impact categories,
        # summed over all processes on nodal base
        self.model.constraint_invest_nodal_impact = pe.Constraint(
            self.model.nodes,
            self.model.impact_categories,
            self.model.years,
            rule=self.constraint_invest_nodal_impact_rule)
        # declaring the calculation of all invest impact category limits,
        # summed over all processes on nodal base
        self.model.constraint_invest_nodal_impact_limits = pe.Constraint(
            self.model.nodes,
            self.model.impact_categories,
            self.model.years,
            rule=self.constraint_invest_nodal_impact_limits_rule)
        # invest impacts over all nodes
        # declaring the calculation of all invest impact categories,
        # summed over all processes
        self.model.constraint_invest_impact = pe.Constraint(
            self.model.impact_categories,
            self.model.years,
            rule=self.constraint_invest_impact_rule)
        # declaring the calculation of all invest impact category limits,
        # summed over all processes
        self.model.constraint_invest_impact_limits = pe.Constraint(
            self.model.impact_categories,
            self.model.years,
            rule=self.constraint_invest_impact_limits_rule)
        # total impacts nodal based
        # declaring the calculation of all impact categories,
        # summed over all processes on nodal base
        self.model.constraint_total_nodal_impact = pe.Constraint(
            self.model.nodes,
            self.model.impact_categories,
            self.model.years,
            rule=self.constraint_total_nodal_impact_rule)
        # declaring the limitation of all impact categories on nodal scale
        self.model.constraint_total_nodal_impact_limits = pe.Constraint(
            self.model.nodes,
            self.model.impact_categories,
            self.model.years,
            rule=self.constraint_total_nodal_impact_limits_rule)
        # total impacts over all nodes
        # declaring the calculation of all impact categories,
        # summed over all processes and nodes
        self.model.constraint_total_impact = pe.Constraint(
            self.model.impact_categories,
            self.model.years,
            rule=self.constraint_total_impact_rule)
        # declaring the limitation of all impact categories on system-wide scale
        self.model.constraint_total_impact_limits = pe.Constraint(
            self.model.impact_categories,
            self.model.years,
            rule=self.constraint_total_impact_limits_rule)

        # declaring the limit of production capacity, limited by the provided input
        self.model.constraint_production_potential_capacity_by_input = pe.Constraint(
            self.model.nodes,
            self.model.processes_production,
            self.model.years,
            self.model.construction_years_production,
            rule=self.constraint_production_potential_capacity_by_input_rule)

        # declaring the limit of transshipment capacity, limited by the provided input
        self.model.constraint_transshipment_potential_capacity_by_input = pe.Constraint(
            self.model.connections,
            self.model.processes_transshipment,
            self.model.years,
            rule=self.constraint_transshipment_potential_capacity_by_input_rule)
        # declaring the limit of transmission capacity, limited by the provided input
        self.model.constraint_transmission_potential_capacity_by_input_rule = pe.Constraint(
            self.model.connections,
            self.model.processes_transmission,
            self.model.years,
            rule=self.constraint_transmission_potential_capacity_by_input_rule_rule)

        # Constraint to restrict export/selling of products
        self.model.constraint_restrict_export = pe.Constraint(
            self.model.nodes,
            self.model.products,
            self.model.years,
            self.model.time_slices,
            rule=self.constraint_restrict_export_rule)
        # Constraint to restrict non-served demand/purchase of products to 0 (e.g., for heat and cold)
        self.model.constraint_restrict_import = pe.Constraint(
            self.model.nodes,
            self.model.products,
            self.model.years,
            self.model.time_slices,
            rule=self.constraint_restrict_import_rule)

        # Constrain used_production to time_series_availability (e.g. for variable renewables)
        self.model.constraint_usable_production = pe.Constraint(
            self.model.nodes,
            self.model.processes_production,
            self.model.years,
            self.model.construction_years_production,
            self.model.time_slices,
            self.model.vertices,
            rule=self.constraint_usable_production_rule)

    def setupObjective(self):
        """This method declares the pe.Objective required by all optimization models."""

        self.model.Objective = pe.Objective(rule=self.objectiveRule)
    
    #### INSTANTIATE and RUN MODEL   
    def instantiate_model(self, input_dict: dict = None, filepath: str = None, skip_instantiation: bool = False):
        """This method creates a model instance and fixes variables"""
        
        if not skip_instantiation:
          if input_dict:
              self.model_instance = self.model.create_instance(data=input_dict)
          elif filepath:
              self.model_instance = self.model.create_instance(filename=filepath)
          else:
              raise AttributeError("Please provide input data either as input dictionary for pyomo or as filepath to an input file.")
          
          # fix uneeded variables
          self.fix_unneeded_variables()
          # fix transmission phase difference of one node to zero 
          self.fix_transmission_reference_node_phase_difference()
          
    def run(self, input_dict: dict = None, filepath: str = None, solver: str = 'glpk', solver_options: dict = None, debug: bool = False, skip_instantiation: bool = False):
        """This method is used to start an optimization.

        To start an optimization it is necessary to provide a file with the necessary input data.
        This file includes all information required to instantiate the optimization problem.

        Args:
            input_dict (dict): Input dictionary that contains all the necessary data for the optimization
            filename (str): Filename of the previously generated input file
            solve (str): Name of the solve to be used. Standard is 'gurobi'
        """

        logging.info("Instantiate optimization")

        self.mysolver = pe.SolverFactory(solver, options=solver_options)
        self.instantiate_model(input_dict,filepath,skip_instantiation)
        
        if (solver == "gurobi_persistent"):
            logging.disable(logging.WARNING)
            self.mysolver.set_instance(self.model_instance, symbolic_solver_labels=True)
            self.results = self.mysolver.solve(tee=debug)
        else:
            self.results = self.mysolver.solve(self.model_instance, tee=debug)

        logging.info("Termination condition is {}".format(self.results.solver.termination_condition))
        if self.results.solver.termination_condition == TerminationCondition.optimal:
            self.model_instance.solutions.load_from(self.results)
            logging.disable(logging.NOTSET)

    def fix_transmission_reference_node_phase_difference(self):
        """This method is used as rule for fixing the transmission phase difference of one node to zero.

        This is necessary for the calculation in the DC load flow model of the transmission grid.
        Furthermore the reference node should be a node at which no loads of any kind occur.
        Therefore extractions or feed-in must not take place at this node.
        """

        logging.info("Fix reference node")

        for year in self.model_instance.years:
            for time_slice in self.model_instance.time_slices:
                    self.model_instance.phase_difference[self.model_instance.transmission_reference_node.value, year, time_slice].fix(0)

    def fix_unneeded_variables(self):
        """Fixes unneeded variables to 0 for all processes."""

        logging.info("Fix unneeded variables")
        number_of_variables = 0
        number_of_fixed_variables = 0

        for year in self.model_instance.years:
            for time_slice in self.model_instance.time_slices:
                for node in self.model_instance.nodes:
                    for process_production in self.model_instance.processes_production:
                        for construction_year in self.model_instance.construction_years_production:
                            number_of_variables += 1
                            if ((year - construction_year) >= self.model_instance.lifetime_duration_production[process_production, construction_year]):
                                for vertex in range(self.model_instance.cornerpoints_number[process_production]-1):
                                    self.model_instance.used_production[node, process_production, year, construction_year, time_slice, vertex].fix(0)
                                    self.model_instance.xsi_production[node, process_production, year, construction_year, time_slice, vertex].fix(0)
                                number_of_fixed_variables += 2
                    for process_storage in self.model_instance.processes_storage:
                        for construction_year in self.model_instance.construction_years_storage:
                            number_of_variables += 2
                            if ((year - construction_year) >= self.model_instance.lifetime_duration_storage[process_storage, construction_year]):
                                number_of_fixed_variables += 2
                                for direction_storage in self.model_instance.directions_storage:
                                    self.model_instance.used_storage[node, process_storage, direction_storage, year, construction_year, time_slice].fix(0)
                for connection in self.model_instance.connections:
                    for process_transshipment in self.model_instance.processes_transshipment:
                        for direction_transshipment in self.model_instance.directions_transshipment:
                            for construction_year in self.model_instance.construction_years_transshipment:
                                number_of_variables += 2
                                if ((year - construction_year) >= self.model_instance.lifetime_duration_transshipment[process_transshipment, construction_year]):
                                    number_of_fixed_variables += 2
                                    for direction_transshipment in self.model_instance.directions_transshipment:
                                        self.model_instance.used_transshipment[connection, process_transshipment, direction_transshipment, year, construction_year, time_slice].fix(0)

        logging.info("{0} of {1} variables fixed in AbstractOptimization - {2} %".format(number_of_fixed_variables, number_of_variables, number_of_fixed_variables / number_of_variables * 100))
    """
    @staticmethod
    def fix_variables(*args):
         This method sets the variables that need to be fixed to 0 at initialization 
        
        By setting the default value to 0, the fixed variables can be identified before explicitly fixing them. 
        If condition is met, function returns 0, otherwise None. This is subsequently set as the variable's default value
        Args:
            *args: 
                args[0](str): string of variable name to identify variable
                args[1](pe.ConcreteModel): ConcreteModel with necessary sets and parameters
                args[2:]: Set indizes which define the current variable index (e.g. year, product, production_process, node etc.) 
        
        # select items from *args, see variable definition for order of data
        variable_name = args[0]
        model = args[1]
        data = args[2:]

        # non_served_demand
        # data[0] = node
        # data[1] = product
        # data[2] = year
        # data[3] = time_slice
        if variable_name == 'non_served_demand':
            if (data[1] != 'electricity' or data[1] != 'natural_gas'): 
                # Fix import of heat (general and high T) and cold to 0 to prevent impact-free import when optimizing GWI
                return 0
            #else:
            #    return None
        elif variable_name == 'add_demand':
            if data[1] == 'electricity':
                # Fix additional demand (=exported product) to 0 for electricity to prevent impact credit from exporting when optimizing GWI
                return 0
            #else:
            #    return None
    """
    #### METHODS 
    # Evaluation methods
    @staticmethod
    def get_dataframe_from_result(model_instance_variable, unstack: list = None):
        """Returns a dataframe with the results from the optimization model."""

        # Create empty dictionary
        variable = {}
        # for all indices in the model_instance_variable put its key-value pair in the dictionary
        for index in model_instance_variable:
            variable[index] = model_instance_variable[index].value
        
        # Create pandas Series from 
        variable = pd.Series(variable)
        if unstack:
            variable = variable.unstack(unstack)

        return variable

    @staticmethod
    def get_dataframe_from_parameter(model_instance_variable, unstack: list = None):
        """Returns a dataframe with the results from the optimization model."""

        # Create empty dictionary
        variable = {}
        # for all indices in the model_instance_variable put its key-value pair in the dictionary
        for index in model_instance_variable:
            variable[index] = model_instance_variable[index]
        
        # Create pandas Series from 
        variable = pd.Series(variable)
        if unstack:
            variable = variable.unstack(unstack)

        return variable

    def get_used_capacity_from_result(self):
        """Return a dictionary of dataframes with the used capacities from the optimization."""

        # Create empty dictionary
        used_capacity_categories = ["production", "storage", "transshipment", "transmission"]
        unstack = {"production": [2], "storage": [2], "transshipment": [2], "transmission": [2]}
        index = {"production": ["node", "process"], "storage": ["node", "process"], "transshipment": ["connection", "process"], "transmission": ["connection", "process"]}
        used_capacity = {}
        for category in used_capacity_categories:
            used_capacity[category] = self.get_dataframe_from_result(getattr(self.model_instance, "used_" + category), unstack=unstack[category])
            used_capacity[category].index.names = index[category]

        return used_capacity

    def get_non_served_demand_from_result(self):
        """Return a dictionary of dataframes with the non_served_demand from the optimization."""

        # Create empty dictionary
        unstack= [2]
        index = ["node", "product", "time slice"]
        non_served_demand = self.get_dataframe_from_result(getattr(self.model_instance, "non_served_demand"), unstack=unstack)
        non_served_demand.index.names = index

        return non_served_demand

    #### CONSTRAINTS 
    ### Product Balance
    @staticmethod
    def constraint_product_balance_rule(model, node: int, product: str, year: int, time_slice: str) -> "pyomo rule":
        """This method is used as rule for the product balance equation.

        This expression represents the product balance equation which is unique for each node, product, year and time slice.
        It can be summarized as::

            Production + (Transshipment) + Storage + (Transmission) + Purchase = Demand + Selling
        
        Therefore it takes the named above as parameters.

        Args:
            model: The equivalent of "self" in pyomo optimization models
            node (int): A node at which the pe.Constraint is active
            product (str): A product at which is constrained
            year (int): A year at which the pe.Constraint is active
            time_slice (str): A time_slice at which the pe.Constraint is active
        """

        # returns the expression which is the product balance equation
        # purchase or sell of some products is later on the next two constraints
        return (
            # Sets the demand for a product (consisting of parametric demand and add_demand sold)
            # at a node in a specific year and time slice equal to
            # the sum of all process contributions to that product balance
            model.demand[product, year, time_slice, node] + model.add_demand[node, product, year, time_slice] == (
            
            # sum of the production of a product by all used production processes
            sum(
                # sum over all construction years to differentiate between the used capacities
                sum(
                    # sum over all vertices and sections, respectively
                    sum(
                        model.technology_matrix_production[vertex, product, process_production, year_construction]
                        * model.xsi_production[node, process_production, year, year_construction, time_slice, vertex]
                        for vertex in range(model.cornerpoints_number[process_production]-1))
                    + sum(
                        model.technology_matrix_production_gradient[vertex, product, process_production, year_construction]
                        * (model.used_production[node, process_production, year, year_construction, time_slice, vertex]
                        - model.xsi_production[node, process_production, year, year_construction, time_slice, vertex]
                        * model.processes_rel_partload_capacity[vertex, process_production, year_construction])
                        for vertex in range(model.cornerpoints_number[process_production]-1))
                    for year_construction in filter(lambda year_construction: (year_construction <= year) and (year - year_construction < model.lifetime_duration_production[process_production, year_construction]), model.construction_years_production))
                for process_production in model.processes_production)

            # sum over all storage processes
            + sum(
                # sum over all construction years to differentiate between the used capacities
                + sum(
                    sum(model.used_storage[node, process_storage, direction_storage, year, year_construction, time_slice]
                        * (- model.storage_level_factor[direction_storage])
                        # sum over both storage direction (deposit and withdraw)
                        for direction_storage in model.directions_storage)
                    for year_construction in filter(lambda year_construction: (year_construction <= year) and (year - year_construction < model.lifetime_duration_storage[process_storage, year_construction]), model.construction_years_storage))
                for process_storage in filter(lambda process_storage: product == model.storage_products[process_storage], model.processes_storage))
            
            # sum of the transshipment of a product by all used transshipment processes
            + sum(
                # sum over all construction years to differentiate between the used capacities
                sum(
                    # Sum of all streams of connections which have this node as first node
                    sum(model.transshipment_efficiency[process_transshipment, connection]
                        * model.used_transshipment[connection, process_transshipment, "backward", year, year_construction, time_slice]
                        - model.used_transshipment[connection, process_transshipment, "forward", year, year_construction, time_slice]
                        for connection in filter(lambda connection: model.connected_nodes[connection, "node1"] == node, model.connections))
                    # Sum of all streams of connections, which have this node as second node, therefore multiplied with the efficiency of the connection
                    + sum(model.transshipment_efficiency[process_transshipment, connection]
                        * model.used_transshipment[connection, process_transshipment, "forward", year, year_construction, time_slice]
                        - model.used_transshipment[connection, process_transshipment, "backward", year, year_construction, time_slice]
                        for connection in filter(lambda connection: model.connected_nodes[connection, "node2"] == node, model.connections))
                    
                    for year_construction in filter(lambda year_construction: (year_construction <= year) and (year - year_construction < model.lifetime_duration_transshipment[process_transshipment, year_construction]), model.construction_years_transshipment))
                # sum over all transshipment processes that use this product
                for process_transshipment in filter(lambda process_transshipment: product == model.transshipment_products[process_transshipment], model.processes_transshipment))
                
            # adding the power from DC load flow transmission, if the product is electricity
            + ( # Sum of the power from all connections which have this node as first node
                sum(
                    model.power_line_properties[connection, year, "susceptance per unit"]
                    * (model.phase_difference[model.connected_nodes[connection, "node2"], year, time_slice]
                        - model.phase_difference[node, year, time_slice])
                    for connection in filter(lambda connection: (model.connected_nodes[connection, "node1"] == node) and (product in model.products_transmission), model.connections))
                # Sum of the power from all connections which have this node as second node
                + sum(
                    model.power_line_properties[connection, year, "susceptance per unit"]
                    * (model.phase_difference[model.connected_nodes[connection, "node1"], year, time_slice]
                        - model.phase_difference[node, year, time_slice])
                    for connection in filter(lambda connection: (model.connected_nodes[connection, "node2"] == node) and (product in model.products_transmission), model.connections))
            ) * model.per_unit_base
            # variable for non-served demand for the purchase of products
            + model.non_served_demand[node, product, year, time_slice]
            )
        )

    # Restrict sell of products
    @staticmethod
    def constraint_restrict_export_rule(model, node: int, product: str, year: int, time_slice: str) -> "pyomo rule":
        """
        Restrict exports of products you dont want to sell/you dont want to allow sell.
        """
        if not product in config.products_exportable:
            return(
                model.add_demand[node,product,year,time_slice] == 0
            )
        else:
            return pe.Constraint.Skip

    # Restrict purchase of products
    @staticmethod
    def constraint_restrict_import_rule(model, node: int, product: str, year: int, time_slice: str) -> "pyomo rule":
        """
        This method is used as rule for preventing import of non-importable products.
        """
        if not product in config.products_importable: 
            return(
                model.non_served_demand[node,product,year,time_slice] == 0
            )
        else:
            return pe.Constraint.Skip

    @staticmethod
    def constraint_usable_production_rule(model, node: int, process_production: str, year: int, year_construction: int, time_slice: str, vertex: int) -> "pyomo rule":
        """
        This method is used to constrain the used production to the available production in each time slice.
        """
        if year_construction < min(model.years):
            capacity_production_bound = model.existing_capacity_production[node, process_production, year, year_construction]
        else:
            capacity_production_bound = model.new_capacity_production[node, process_production, year_construction]
            
        return(
            model.used_production[node, process_production, year, year_construction, time_slice, vertex] <=
            capacity_production_bound
            * model.usable_capacity_factor_timeseries_production[node, process_production, time_slice]
        )

    ### Production
    @staticmethod
    def constraint_production_limit_binaries_rule(model, node: int, process_production: str, year: int, year_construction: int, time_slice: str) -> "pyomo rule":
        """
        This method is used as rule for limitting the number of binaries which are allowed in partload behaviour of components,
        i.e. one partload segment can be chosen at maximum.
        """
        return(
            sum(model.used_production_binary[node, process_production, year, year_construction, time_slice, vertex]
            for vertex in range(model.cornerpoints_number[process_production]-1)) <= 1
        )
    
    @staticmethod
    def constraint_production_limit_by_capacity_upper_rule(model, node: int, process_production: str, year: int, year_construction: int, time_slice: str, vertex: int) -> "pyomo rule":
        """
        This method constrains the used_production to the upper capacity available in a part-load segment by the parameter
        processes_rel_partload_capacity.
        """
        if vertex < model.cornerpoints_number[process_production]-1 and year >= year_construction:
            return(
                model.used_production[node, process_production, year, year_construction, time_slice, vertex] <=
                model.xsi_production[node, process_production, year, year_construction, time_slice, vertex]
                * model.processes_rel_partload_capacity[vertex+1, process_production, year_construction]
                )
        else:
            return(
                model.used_production[node, process_production, year, year_construction, time_slice, vertex] == 0
                )
        
    @staticmethod
    def constraint_production_limit_by_capacity_lower_rule(model, node: int, process_production: str, year: int, year_construction: int, time_slice: str, vertex: int) -> "pyomo rule":
        """
        This method constrains the used_production to the lower capacity available in a part-load segment by the parameter
        processes_rel_partload_capacity.
        """
        if vertex < model.cornerpoints_number[process_production]-1 and year >= year_construction:
            return(
                model.used_production[node, process_production, year, year_construction, time_slice, vertex] >=
                model.xsi_production[node, process_production, year, year_construction, time_slice, vertex]
                * model.processes_rel_partload_capacity[vertex, process_production, year_construction]
                )
        else:
            return(
                model.used_production[node, process_production, year, year_construction, time_slice, vertex] == 0
                )
    
    @staticmethod
    def constraint_production_potential_capacity_by_input_rule(model, node: int, process_production: str, year: int, year_construction: int) -> "pyomo rule":
        """
        This constraint sets a limit for the potential capacity of production processes.
        If existing_capacity_production is >0, this component already exists.
        Thus, new_capacity_production is fixed to existing_capacity_production in this rule.
        """
        if model.existing_capacity_production[node, process_production, year, year_construction] != 0 and year_construction >= min(model.years):
            return(
                model.new_capacity_production[node, process_production, year_construction] == model.existing_capacity_production[node, process_production, year, year_construction]
                )
        else:
            return pe.Constraint.Skip  
        
    ## Glovers equation adapted to SecMOD
    # If binary = 0, then xsi_production = 0
    # If binary = 1, then minimal_capacity_production <= xsi_production <= potential_capacity_production
    @staticmethod
    def constraint_production_glovers_1a_rule(model, node: int, process_production: str, year: int, year_construction: int, time_slice: str, vertex: int) -> "pyomo rule":
        """
        The glover constraints 1a, 1b, 2a, 2b model part-load behavior using the time-dependent auxiliary capacity xsi_production. Here:
        If used_production_binary = 0, then xsi_production = 0 OR
        If used_production_binary = 1, then xsi_production <= potential_capacity_production.
        """
        if year_construction < min(model.years):
            year_construction_for_bound = min(model.years)
        else:
            year_construction_for_bound = year_construction
        if vertex < model.cornerpoints_number[process_production]-1 and year >= year_construction:
            return(
                model.xsi_production[node, process_production, year, year_construction, time_slice, vertex] <=
                model.used_production_binary[node, process_production, year, year_construction, time_slice, vertex]
                * model.potential_capacity_production[node, process_production, year_construction_for_bound]
                )
        else: # if year < year_construction or vertex not in cornerpoints
            return(
                model.used_production_binary[node, process_production, year, year_construction, time_slice, vertex] == 0
                )
        
    @staticmethod
    def constraint_production_glovers_1b_rule(model, node: int, process_production: str, year: int, year_construction: int, time_slice: str, vertex: int) -> "pyomo rule":
        """
        The glover constraints 1a, 1b, 2a, 2b model part-load behavior using the time-dependent auxiliary capacity xsi_production. Here:
        If used_production_binary = 0, then xsi_production = 0 OR
        If used_production_binary = 1, then then minimal_capacity_production <= xsi_production.
        """
        if year_construction < min(model.years):
            year_construction_for_bound = min(model.years)
        else:
            year_construction_for_bound = year_construction
        if vertex < model.cornerpoints_number[process_production]-1 and year >= year_construction:
            return(
                model.xsi_production[node, process_production, year, year_construction, time_slice, vertex] >=
                model.used_production_binary[node, process_production, year, year_construction, time_slice, vertex]
                * model.minimal_capacity_production[node, process_production, year_construction_for_bound]
                )
        else:
            return(
                model.used_production_binary[node, process_production, year, year_construction, time_slice, vertex] == 0
                )
    
    @staticmethod
    # If used_production_binary == 0, then new_capacity_production-potential_capacity_production <= xsi_production <= potential_capacity_production
    # If used_production_binary == 1, then xsi_production=new_capacity_production
    def constraint_production_glovers_2a_rule(model, node: int, process_production: str, year: int, year_construction: int, time_slice: str, vertex: int) -> "pyomo rule":
        """
        The glover constraints 1a, 1b, 2a, 2b model part-load behavior using the time-dependent auxiliary capacity xsi_production. Here:
        If used_production_binary == 0, then new_capacity_production-potential_capacity_production <= xsi_production <= potential_capacity_production
        If used_production_binary == 1, then xsi_production=new_capacity_production
        """
        if year_construction < min(model.years):
            capacity_production_bound = model.existing_capacity_production[node, process_production, year, year_construction]
        else:
            capacity_production_bound = model.new_capacity_production[node, process_production, year_construction]
            
        if vertex < model.cornerpoints_number[process_production]-1 and year >= year_construction: 
            return(
                model.xsi_production[node, process_production, year, year_construction, time_slice, vertex] <=
                capacity_production_bound
                )
        else:
            return(
                model.xsi_production[node, process_production, year, year_construction, time_slice, vertex] == 0
                )
    
    @staticmethod
    def constraint_production_glovers_2b_rule(model, node: int, process_production: str, year: int, year_construction: int, time_slice: str, vertex: int) -> "pyomo rule":
        """
        The glover constraints 1a, 1b, 2a, 2b model part-load behavior using the time-dependent auxiliary capacity xsi_production. Here:
        If used_production_binary == 0, then new_capacity_production-potential_capacity_production <= xsi_production <= potential_capacity_production
        If used_production_binary == 1, then xsi_production=new_capacity_production
        """
        if year_construction < min(model.years):
            year_construction_for_bound = min(model.years)
            capacity_production_bound = model.existing_capacity_production[node, process_production, year, year_construction]
        else:
            year_construction_for_bound = year_construction
            capacity_production_bound = model.new_capacity_production[node, process_production, year_construction]
            
        if vertex < model.cornerpoints_number[process_production]-1 and year >= year_construction: 
            return(
                model.xsi_production[node, process_production, year, year_construction, time_slice, vertex] >=
                capacity_production_bound
                + (model.used_production_binary[node, process_production, year, year_construction, time_slice, vertex]-1)
                * model.potential_capacity_production[node, process_production, year_construction_for_bound]
                )
        else:
            return(
                model.xsi_production[node, process_production, year, year_construction, time_slice, vertex] == 0
                )

    ### Storage 
    @staticmethod
    def constraint_storage_level_limit_by_capacity_rule(model, node: int, process_storage: str, year: int, year_construction: int, time_slice: str) -> "pyomo rule":
        """
        This method is used as rule for limiting the storage_level up to the existing (if exist) or new capacity.
        """

        # if there exists a storage, limit storage_level to existing capacity
        if model.existing_capacity_storage[node, process_storage, year, year_construction] != 0:
            return(
                model.storage_level[node, process_storage, year, year_construction, time_slice]
                <= model.existing_capacity_storage[node, process_storage, year, year_construction]
            )  
        # otherwise a storage can be built if yoc is in years
        else:
            if year_construction not in model.years:
                return model.storage_level[node, process_storage, year, year_construction, time_slice] == 0
            else:
                return(
                    model.storage_level[node, process_storage, year, year_construction, time_slice]
                    <= model.new_capacity_storage[node,process_storage, year_construction]
                )
    
    @staticmethod
    def constraint_storage_level_limit_by_capacity_lower_rule(model, node: int, process_storage: str, year: int, year_construction: int, time_slice: str) -> "pyomo rule":
        """
        This method is used as rule for setting the lower bound for to the existing (if exist) or new capacity * minial relative storage level.
        """        
    
        # if there exists a storage, storage_level has to greater or equal to a minimal load
        if model.existing_capacity_storage[node, process_storage, year, year_construction] != 0:
            return(
                model.storage_level[node, process_storage, year, year_construction, time_slice]
                >= (model.existing_capacity_storage[node, process_storage, year, year_construction]
                * model.min_rel_storage_level_factor[process_storage, year_construction])
            )  
        # otherwise a storage can be built if yoc is in years
        else:
            if year_construction not in model.years:
                return model.storage_level[node, process_storage, year, year_construction, time_slice] == 0
            else:
                return(
                    model.storage_level[node, process_storage, year, year_construction, time_slice]
                    >= (model.new_capacity_storage[node,process_storage, year_construction]
                    * model.min_rel_storage_level_factor[process_storage, year_construction])
                )     
    
    @staticmethod
    def constraint_used_storage_limit_by_capacity_rule(model, node: int, process_storage: str, direction_storage: str, year: int, year_construction: int, time_slice: str) -> "pyomo rule":
        """This abstract method is used as rule for limiting the output and input of storage processes.

        The maximum flow out of or into a storage instance is limited by the available storage capacity
        multiplied with a flow-to-storage-capacity factor:

            storage usage <= flow-to-storage-capacity factor * (existing capacity)

        or::

            storage usage <= flow-to-storage-capacity factor * (new capacity)

        Args:
            model: The equivalent of "self" in pyomo optimization models
            node (int): A node at which the pe.Constraint is active
            process_storage (str): A storage process which is constrained
            direction_storage (str): A direction of "withdraw" or "deposit" a product
            year (int): A year at which the pe.Constraint is active
            year_construction (int): The year of construction of a specific capacity
            time_slice (str): A time_slice at which the pe.Constraint is active
        """

        # if there exists a storage, limit used_storage to existing capacity*factor
        if model.existing_capacity_storage[node, process_storage, year, year_construction] != 0:
            return(
                model.used_storage[node, process_storage, direction_storage, year, year_construction, time_slice]
                <= (model.flow_to_storage_capacity_factor[process_storage, year_construction] 
                * model.existing_capacity_storage[node, process_storage, year, year_construction])
            )
        # otherwise a storage can be built
        else:
            if year_construction not in model.years:
                return model.used_storage[node, process_storage, direction_storage, year, year_construction, time_slice] == 0
            else:
                return(
                    model.used_storage[node, process_storage, direction_storage, year, year_construction, time_slice]
                    <= (model.flow_to_storage_capacity_factor[process_storage, year_construction]
                        *model.new_capacity_storage[node, process_storage, year_construction])
                    )
    
    @staticmethod
    def constraint_storage_potential_capacity_by_input_rule(model, node: int, process_storage: str, year: int, year_construction: int) -> "pyomo rule":
        """
        This method is used as rule for limitting the capacity of the storage processes by the maximum potential,
        if the storage exists.
        """
        # if there exists a storage, no capacity can be built
        if model.existing_capacity_storage[node, process_storage, year, year_construction] != 0:
            return model.new_capacity_storage[node,process_storage, year_construction] == 0
        # otherwise you can build capacity
        else:
            if year_construction not in model.years:
                return model.new_capacity_storage[node,process_storage, year_construction] == 0
            else:
                return(
                    model.new_capacity_storage[node,process_storage, year_construction] 
                    <= model.potential_capacity_storage[node, process_storage, year] 
                    * model.existence_storage[node,process_storage, year_construction]) 
         
    @staticmethod
    def constraint_storage_minimal_capacity_by_input_rule(model, node: int, process_storage: str, year: int, year_construction: int) -> "pyomo rule":
        """This method is used as rule for setting a lower limit to a newly built storage process, if the storage unit is built.

        Args:
            model: The equivalent of "self" in pyomo optimization models
            node (int): A node at which the pe.Constraint is active
            process_storage (str): A storage process which is constrained
            year (int): A year at which the pe.Constraint is active
        """

        # if there exists a storage, no capacity can be built
        if model.existing_capacity_storage[node, process_storage, year, year_construction] != 0:
            return model.new_capacity_storage[node,process_storage, year_construction] == 0
        # otherwise you can build capacity
        else:
            if year_construction not in model.years:
                return model.new_capacity_storage[node,process_storage, year_construction] == 0
            else:
                return(
                    model.new_capacity_storage[node,process_storage, year_construction] 
                    >= model.minimal_capacity_storage[node, process_storage, year] 
                    * model.existence_storage[node,process_storage, year_construction]) 
            
    @staticmethod
    def constraint_storage_deposit_withdraw_decision_rule(model, node: int, process_storage: str, direction_storage: str, year: int, year_construction: int, time_slice: str) -> "pyomo rule":
        """
        This method restricts the storage usage to deposit or withdraw depending on the existing capacity/potential.
        """
        if model.existing_capacity_storage[node, process_storage, year, year_construction] != 0:
            return(model.used_storage[node, process_storage, direction_storage, year, year_construction, time_slice]
            <= (model.flow_to_storage_capacity_factor[process_storage, year_construction] 
            * model.existing_capacity_storage[node, process_storage, year, year_construction])
            * model.storage_deposit_withdraw_decision[node, process_storage, direction_storage, year, year_construction, time_slice])
        else:
            return(model.used_storage[node, process_storage, direction_storage, year, year_construction, time_slice]
            <= (model.flow_to_storage_capacity_factor[process_storage, year_construction] 
            * model.potential_capacity_storage[node, process_storage, year])
            * model.storage_deposit_withdraw_decision[node, process_storage, direction_storage, year, year_construction, time_slice])

    @staticmethod
    def constraint_storage_restrict_storing_per_timeslice_rule(model, node: int, process_storage: str, year: int, year_construction: int, time_slice: str) -> "pyomo rule":
        """This method is used as a rule for deposit/withdrawel decision.
        
        We can either withdraw or deposit in the storage, but never both at the same time in a time step."""
        return(
            sum(
                model.storage_deposit_withdraw_decision[node, process_storage, direction_storage, year, year_construction, time_slice]
                for direction_storage in model.directions_storage) <= 1)
            
    @staticmethod
    def constraint_storage_level_rule(model, node: int, process_storage: str, year: int, year_construction: int, time_slice: str) -> "pyomo rule":
        """This method is used as rule for the calculation of the storage level.

        Since the storage should not act as a source or sink, this method calculates the storage level after
        each time_slice and couples it with the storage level at the beginning of the next time slice.

        Args:
            model: The equivalent of "self" in pyomo optimization models
            node (int): A node at which the pe.Constraint is active
            process_storage (str): A storage process which is constrained
            year (int): A year at which the pe.Constraint is active
            year_construction (int): The year of construction of a specific capacity
            time_slice (str): A time_slice at which the pe.Constraint is active
        """
        return(
            # sum of the current storage level, the deposits and withdrawals
            (model.storage_level[node, process_storage, year, year_construction, time_slice]*(1-model.rel_storage_loss_factor[process_storage,year_construction])**model.time_slice_duration[time_slice]             # multiplication of the duration of the time slice with...
             + (model.time_slice_duration[time_slice]
             # ... the sum of all deposits and withdrawals from the storage
             * sum(model.storage_level_factor[direction_storage] 
                * model.used_storage[node, process_storage, direction_storage, year, year_construction, time_slice]
                * (model.efficiency_matrix_storage[model.storage_products[process_storage], process_storage, direction_storage, year_construction]
                ** model.storage_level_factor[direction_storage])
                for direction_storage in model.directions_storage))
            # the sum above equal the storage level of the next time slice
            ) == model.storage_level[node, process_storage, year, year_construction, model.next_time_slice[time_slice]]
        )    

    ### Transshipment
    @staticmethod
    def constraint_transshipment_limit_by_capacity_rule(model, connection: int, process_transshipment: str, direction_transshipment: str, year: int, year_construction: int, time_slice: str) -> "pyomo rule":
        """This method is used rule for limiting the used transmission up to the sum of existing and new capacity.

        The rule is applied to every year of operation AND every year of construction.
        This means that for every year the capacity of existing infrastructure is used as limit,
        creating a constraint for every year of construction as well. This way the operation can
        differentiate between older and newer capacities which might have different impacts.
        Additionally for the years of construction which also are investment years, 
        the newly build capacities are added to the limit.

        Args:
            model: The equivalent of "self" in pyomo optimization models
            process_transshipment (str): A transshipment process which is constrained
            direction_transshipment (str): A transshipment direction which is constrained
            year (int): A year at which the pe.Constraint is active
            year_construction (int): The year of construction of a specific capacity
            time_slice (str): A time_slice at which the pe.Constraint is active
        """

        if year_construction not in model.years:
            return(
                model.used_transshipment[connection, process_transshipment, direction_transshipment, year, year_construction, time_slice]
                <= model.existing_capacity_transshipment[connection, process_transshipment, year, year_construction]
            )
        else:
            return(
                model.used_transshipment[connection, process_transshipment, direction_transshipment, year, year_construction, time_slice]
                <= model.existing_capacity_transshipment[connection, process_transshipment, year, year_construction]
                + model.new_capacity_transshipment[connection, process_transshipment, year_construction]
            )

    @staticmethod
    def constraint_transshipment_potential_capacity_by_input_rule(model, connection: int, process_transshipment: str, year: int) -> "pyomo rule":
        """This method is used as rule for limitting the capacity of transshipment processes by limits
        from the input e.g. maximum potential.

        Some transshipment technologies like pipelines have local capacity limits based on
        the availability of ressources or regulatory restrictions. These limits are given to the
        optimization as parameter and can change by time.

        Args:
            model: The equivalent of "self" in pyomo optimization models
            connection (int): A connection of the grid
            process_transshipment (str): A transshipment process which is constrained
            year (int): A year at which the pe.Constraint is active
        """

        return(
            # Maximum of either potential capacity for this year or the total existing capacity in this year
            max(model.potential_capacity_transshipment[connection, process_transshipment, year], 
                sum(model.existing_capacity_transshipment[connection,process_transshipment, year, year_construction]
                    for year_construction in filter(lambda year_construction: (year_construction <= year) and (year - year_construction < model.lifetime_duration_transshipment[process_transshipment, year_construction]), model.construction_years_transshipment)))
            # that maximum potential has to be greater or equal to the total existing capacity in this year
            >= sum(model.existing_capacity_transshipment[connection,process_transshipment, year, year_construction]
                for year_construction in filter(lambda year_construction: (year_construction <= year) and (year - year_construction < model.lifetime_duration_transshipment[process_transshipment, year_construction]), model.construction_years_transshipment))
            # adding the new capacity
            + sum(model.new_capacity_transshipment[connection,process_transshipment, earlier_year]
                for earlier_year in filter(lambda earlier_year: (earlier_year <= year) and (year - earlier_year < model.lifetime_duration_transshipment[process_transshipment, earlier_year]), model.years))
        )
                
    ### Transmission
    @staticmethod
    def constraint_transmission_limit_by_capacity_positive_rule(model, connection: int, year: int, time_slice: str) -> "pyomo rule":
        """This method is used as rule for limiting the load flow through a transmission power line.

        The load flow needs to be limited by the available capacity, which is why this abstract rule
        exists and is used for a constraint in the base class for all optimizations.
        Although the available capacity can be defined differently, e.g.::

            load flow <= installed capacity

        or::

            load flow <= installed capacity + capacity from switching + capacity from new power lines

        Because of this, the abstract pe.Constraint rule needs to be defined in a subclass.

        Args:
            model: The equivalent of "self" in pyomo optimization models
            connection (int): A power line which is constrained
            year (int): A year at which the pe.Constraint is active
            time_slice (str): A time_slice at which the pe.Constraint is active
        """
        
        return(
            # Positive power flow from node1 to node2
            + model.power_line_properties[connection, year, "susceptance per unit"]
                * (model.phase_difference[model.connected_nodes[connection, "node1"], year, time_slice] - model.phase_difference[model.connected_nodes[connection, "node2"], year, time_slice])
                * model.per_unit_base
            # has to be smaller or equal than the power limit of the existing capacity of that connection
            <= model.power_line_properties[connection, year, "power limit"]
            # adding the sum of the new power limit capacity from all transmission processes
            + sum(
                # sum over all invest years which are earlier or equal to the current invest years
                sum(
                    # new installed circuits of each transmission technology
                    model.new_capacity_transmission[connection, process_transmission, earlier_year]
                    # multiplied by their power limit per circuit
                    * model.power_limit_per_circuit[process_transmission]
                    # multiplied by 1 minus the safety margin
                    * (1 - model.reliability_margin_transmission[process_transmission])
                    for earlier_year in filter(lambda earlier_year: (earlier_year <= year) and (year - earlier_year < model.lifetime_duration_transmission[process_transmission, earlier_year]), model.years))
                for process_transmission in model.processes_transmission)
        )

    @staticmethod
    def constraint_transmission_limit_by_capacity_negative_rule(model, connection: int, year: int, time_slice: str) -> "pyomo rule":
        """This method is used as rule for limiting the load flow through a transmission power line.

        The load flow needs to be limited by the available capacity, which is why this abstract rule
        exists and is used for a constraint in the base class for all optimizations.
        Although the available capacity can be defined differently, e.g.::

            load flow <= installed capacity

        or::

            load flow <= installed capacity + capacity from switching + capacity from new power lines

        Because of this, the abstract pe.Constraint rule needs to be defined in a subclass.

        Args:
            model: The equivalent of "self" in pyomo optimization models
            power_line (int): A power line which is constrained
            year (int): A year at which the pe.Constraint is active
            time_slice (str): A time_slice at which the pe.Constraint is active
        """
        
        return(
            # Negative power flow from node1 to node2
            - model.power_line_properties[connection, year, "susceptance per unit"]
                * (model.phase_difference[model.connected_nodes[connection, "node1"], year, time_slice] - model.phase_difference[model.connected_nodes[connection, "node2"], year, time_slice])
                * model.per_unit_base
            # has to be smaller or equal than the power limit of the existing capacity of that connection
            <= model.power_line_properties[connection, year, "power limit"]
            # adding the sum of the new power limit capacity from all transmission processes
            + sum(
                # sum over all invest years which are earlier or equal to the current invest years
                sum(
                    # new installed circuits of each transmission technology
                    model.new_capacity_transmission[connection, process_transmission, earlier_year]
                    # multiplied by their power limit per circuit
                    * model.power_limit_per_circuit[process_transmission]
                    # multiplied by 1 minus the safety margin
                    * (1 - model.reliability_margin_transmission[process_transmission])
                    for earlier_year in filter(lambda earlier_year: (earlier_year <= year) and (year - earlier_year < model.lifetime_duration_transmission[process_transmission, earlier_year]), model.years))
                for process_transmission in model.processes_transmission)
        ) 

    @staticmethod
    def constraint_transmission_potential_capacity_by_input_rule_rule(model, connection: int, process_transmission: str, year: int) -> "pyomo rule":
        """This method is used as rule for limitting the capacity of transmission processes by limits
        from the input e.g. maximum potential.

        Some transmission technologies have local capacity limits based on the availability of 
        ressources or regulatory restrictions. These limits are given to the optimization as
        parameter and can change by time.

        Args:
            model: The equivalent of "self" in pyomo optimization models
            connection (int): A connection of the grid
            process_transmission (str): A transsmission process which is constrained
            year (int): A year at which the pe.Constraint is active
        """

        return(
            # Maximum of either potential capacity for this year or the total existing capacity in this year
            max(model.potential_capacity_transmission[connection, process_transmission, year], 
                sum(model.existing_capacity_transmission[connection,process_transmission, year, year_construction]
                    for year_construction in filter(lambda year_construction: (year_construction <= year) and (year - year_construction < model.lifetime_duration_transmission[process_transmission, year_construction]), model.construction_years_transmission)))
            # that maximum potential has to be greater or equal to the total existing capacity in this year
            >= sum(model.existing_capacity_transmission[connection, process_transmission, year, year_construction]
                for year_construction in filter(lambda year_construction: (year_construction <= year) and (year - year_construction < model.lifetime_duration_transmission[process_transmission, year_construction]), model.construction_years_transmission))
            # adding the new capacity
            + sum(model.new_capacity_transmission[connection, process_transmission, earlier_year]
                for earlier_year in filter(lambda earlier_year: (earlier_year <= year) and (year - earlier_year < model.lifetime_duration_transmission[process_transmission, earlier_year]), model.years))
        )             

    ### Operational Impacts 
    @staticmethod
    def constraint_operational_nodal_impact_rule(model, node: int, impact_category: str, year: int) -> "pyomo rule":
        """This method is used as rule for the calculation of the operational impact vector summed over all processes at each node.

        The operational impact of one node over all time slices is calculated in this method.
        By summing over the construction years of all capacities, the corresponding impact matrices from
        the actual construction year can be used. Furthermore the impact calculated for each time slice
        is multiplied with the weight in hours of a year the time slice has.

        The operational impact of production processes equals the sum of the products of process usage and
        the corresponding impact matrix. The same method is used for calculating the impact of transshipment
        processes, but the resulting impact is shared equally between the corresponding nodes.

        The operational impact of storage processes is calculated based on the **withdrawn** product flow.

        For the electric transmission grid it is assumed that the operational impact is negligible.

        Furthermore, a credit is given for sold products.

        Args:
            model: The equivalent of "self" in pyomo optimization models
            node (int): A node at which the pe.Constraint is active
            impact_category (str): An impact category which is assessed
            year (int): A year at which the pe.Constraint is active
        """

        return(
            # calculate the operational nodal impact
            model.operational_nodal_impact[node, impact_category, year] ==
            # sum over all time slices of the year
            sum(
                (
                    # Sum over the impacts of all production processes
                    sum(
                        # sum over the construction years of the production process
                        sum(
                            # sum over the vertices of each production process
                            sum(
                                model.used_production[node, process_production, year, year_construction, time_slice, vertex]
                                *(model.impact_matrix_production[impact_category, 'operation', process_production, year, year_construction])
                                for vertex in range(model.cornerpoints_number[process_production]-1))
                            for year_construction in filter(lambda year_construction: (year_construction <= year) and (year - year_construction < model.lifetime_duration_production[process_production, year_construction]), model.construction_years_production))    
                        for process_production in model.processes_production)
                        
                    # sum over all storage processes
                    + sum(
                        # sum over the construction years of the storage process
                        sum(
                            # impact is assumed to be the impact per withdrawn unit of stored product
                            model.impact_matrix_storage[impact_category, 'operation', process_storage, year, year_construction]
                            * model.used_storage[node, process_storage, "withdraw", year, year_construction, time_slice]
                            for year_construction in filter(lambda year_construction: (year_construction <= year) and (year - year_construction < model.lifetime_duration_storage[process_storage, year_construction]), model.construction_years_storage))
                        for process_storage in model.processes_storage)

                    # sum over all transshipment processes
                    + sum(
                        # sum over the construction years of the transshipment process
                        sum(
                            # impact distributed equally (0.5:0.5) between the two connected nodes
                            0.5 * model.impact_matrix_transshipment[impact_category, 'operation', process_transshipment, year, year_construction]
                            # sum over both transshipment directions
                            * sum(
                                # sum over all connections, which have this node as first node
                                sum(model.used_transshipment[connection, process_transshipment, direction_transshipment, year, year_construction, time_slice]
                                    for connection in filter(lambda connection: model.connected_nodes[connection, "node1"] == node, model.connections))
                                # sum over all connections, which have this node as second node
                                + sum(model.used_transshipment[connection, process_transshipment, direction_transshipment, year, year_construction, time_slice]
                                    for connection in filter(lambda connection: model.connected_nodes[connection, "node2"] == node, model.connections))
                                for direction_transshipment in model.directions_transshipment)
                            for year_construction in filter(lambda year_construction: (year_construction <= year) and (year - year_construction < model.lifetime_duration_transshipment[process_transshipment, year_construction]), model.construction_years_transshipment))
                        for process_transshipment in model.processes_transshipment)

                    # adding the sum of all non-served demands multiplied by the corresponding impacts
                    # sum over all products
                    + sum(model.non_served_demand[node, product, year, time_slice]
                        * model.impact_matrix_non_served_demand[impact_category, product, year, time_slice]
                        for product in model.products)
                    
                    # subtracting the sum of all add-served demands multiplied by the corresponding impacts
                    # add_demand that means sold_products are considered as benefits leaving the system boundaries
                    # sum over all products
                    - sum(model.add_demand[node, product, year, time_slice]
                        * model.impact_matrix_add_demand[impact_category, product, year, time_slice]
                        for product in model.products)              
                )
                # multiply the sum of all impacts with the weight of a time slice regarding the full year
                * model.time_slice_yearly_weight[time_slice]
                for time_slice in model.time_slices)
        )

    @staticmethod
    def constraint_operational_nodal_impact_limits_rule(model, node: int, impact_category: str, year: int) -> "pyomo rule":
        """This method is used as rule for the limitation of the operational impact vector summed over all processes at each node.

        Args:
            model: The equivalent of "self" in pyomo optimization models
            node (int): A node at which the pe.Constraint is active
            impact_category (str): An impact category which is assessed
            year (int): A year at which the pe.Constraint is active
        """

        return(
            # limit nodal operational impact
            model.operational_nodal_impact[node, impact_category, year] <=
            model.operational_nodal_impact_limits[node, impact_category, year]
            # adding an overshoot variable mainly for debugging infeasibility issues
            + model.operational_nodal_impact_overshoot[node, impact_category, year]
        )

    # overall pe.Constraint rules for the operational impact
    @staticmethod
    def constraint_operational_impact_rule(model, impact_category: str, year: int) -> "pyomo rule":
        """
        This method is used as rule for the calculation of the operational impact vector summed over all processes and nodes.

        Args:
            model: The equivalent of "self" in pyomo optimization models
            impact_category (str): An impact category which is assessed
            year (int): A year at which the pe.Constraint is active
        """

        return(
            model.operational_impact[impact_category, year] ==
            sum(model.operational_nodal_impact[node, impact_category, year]
                for node in model.nodes)
        )

    @staticmethod
    def constraint_operational_impact_limits_rule(model, impact_category: str, year: int) -> "pyomo rule":
        """
        This method is used as rule for the limitation of the operational impact vector summed over all processes and nodes.

        Args:
            model: The equivalent of "self" in pyomo optimization models
            impact_category (str): An impact category which is assessed
            year (int): A year at which the pe.Constraint is active
        """

        return(
            # limit overall operational impact
            model.operational_impact[impact_category, year] <=
            model.operational_impact_limits[impact_category, year]
            # adding an overshoot variable mainly for debugging feasibility issues
            + model.operational_impact_overshoot[impact_category, year]
        )

    ### Invest Impact
    @staticmethod
    def constraint_invest_nodal_impact_rule(model, node: int, impact_category: str, year: int) -> "pyomo rule":
        """
        This method is used as rule for the calculation of the invest impact vector summed over all processes at each node.
        The method consider the existing and new capacities.

        Args:
            model: The equivalent of "self" in pyomo optimization models
            impact_category (str): An impact category which is assessed
            year (int): A year at which the pe.Constraint is active
        """

        return(
            # calculate the invest nodal impact
            model.invest_nodal_impact[node, impact_category, year] ==

            # sum over all time slice, while yearly impact is weighted with the share of a year the time slice represents
            sum(# Multiply everything with the yearly weight of the time slice
                model.time_slice_yearly_weight[time_slice] / sum(model.time_slice_yearly_weight[time_slice] for time_slice in model.time_slices) 
                
                # existing infrastructure
                # Sum over the impacts of all production processes
                * (
                    # to prevent double counting of invest impacts, only consider existing_capacities if (year not in model.years)
                    # other invest impacts of years in model.years are considered in new_capacity_production 
                    sum(
                    # sum over the construction years of the production process
                    sum(model.existing_capacity_production[node, process_production, year, year_construction]
                        * (model.impact_matrix_production[impact_category, 'invest', process_production, year, year_construction])
                        for year_construction in filter(lambda year_construction: (year_construction not in model.years) and (year - year_construction < model.lifetime_duration_production[process_production, year_construction]), model.construction_years_production))    
                    for process_production in model.processes_production)

                # Adding the sum over all storage processes
                + sum(
                    # sum over the construction years of the storage process
                    sum(model.existing_capacity_storage[node, process_storage, year, year_construction]
                        * model.impact_matrix_storage[impact_category, 'invest', process_storage, year, year_construction]
                        for year_construction in filter(lambda year_construction: (year_construction not in model.years) and (year - year_construction < model.lifetime_duration_storage[process_storage, year_construction]), model.construction_years_storage))
                    for process_storage in model.processes_storage)
                
                # Adding the sum over all transshipment processes
                + sum(
                    # sum over the construction years of the transshipment process
                    sum(
                        # impact distributed equally (0.5:0.5) between the two connected nodes
                        0.5 * model.impact_matrix_transshipment[impact_category, 'invest', process_transshipment, year, year_construction]
                        # multiplied with the length and capacity of all connections of the node
                        * (
                            # sum over all connections, which have this node as first node
                            sum(model.existing_capacity_transshipment[connection, process_transshipment, year, year_construction]
                                * model.connection_length[connection]
                                for connection in filter(lambda connection: model.connected_nodes[connection, "node1"] == node, model.connections))
                            # sum over all connections, which have this node as second node
                            + sum(model.existing_capacity_transshipment[connection, process_transshipment, year, year_construction]
                                * model.connection_length[connection]
                                for connection in filter(lambda connection: model.connected_nodes[connection, "node2"] == node, model.connections))
                        )
                        for year_construction in filter(lambda year_construction: (year_construction <= year) and (year - year_construction < model.lifetime_duration_transshipment[process_transshipment, year_construction]), model.construction_years_transshipment))
                    for process_transshipment in model.processes_transshipment)
                
                # adding the sum of the impacts of the transmission grid
                + sum(
                    # sum over the construction years of the transmission process
                    sum(
                        # impact distributed equally (0.5:0.5) between the two connected nodes
                        0.5 * model.impact_matrix_transmission[impact_category, 'invest', process_transmission, year, year_construction]
                        # multiplied with the length and power capacity of all connections of the node
                        * (
                            # sum over all connections, which have this node as first node
                            sum(model.existing_capacity_transmission[connection, process_transmission, year, year_construction]
                                * model.power_limit_per_circuit[process_transmission]
                                * model.connection_length[connection]
                                for connection in filter(lambda connection: model.connected_nodes[connection, "node1"] == node, model.connections))
                            # sum over all connections, which have this node as second node
                            + sum(model.existing_capacity_transmission[connection, process_transmission, year, year_construction]
                                * model.power_limit_per_circuit[process_transmission]
                                * model.connection_length[connection]
                                for connection in filter(lambda connection: model.connected_nodes[connection, "node2"] == node, model.connections))
                        )
                        for year_construction in filter(lambda year_construction: (year_construction <= year) and (year - year_construction < model.lifetime_duration_transmission[process_transmission, year_construction]), model.construction_years_transmission))
                    for process_transmission in model.processes_transmission)


                # adding NEW CAPACITIES multiplied with the corresponding impact
                # sum over all invest years until the current year
                # Sum over the impacts of all production processes
                + sum(
                    # sum over the construction years of the production process
                    sum(model.new_capacity_production[node, process_production, earlier_year]
                        * model.impact_matrix_production[impact_category, 'invest', process_production, year, earlier_year]
                        for earlier_year in filter(lambda earlier_year: (earlier_year <= year) and (year - earlier_year < model.lifetime_duration_production[process_production, earlier_year]), model.years))    
                    for process_production in model.processes_production)
                
                # Adding the sum over all storage processes
                + sum(
                    # sum over the construction years of the storage process
                    sum(model.new_capacity_storage[node, process_storage, earlier_year]
                        * model.impact_matrix_storage[impact_category, 'invest', process_storage, year, earlier_year]
                        for earlier_year in filter(lambda earlier_year: (earlier_year <= year) and (year - earlier_year < model.lifetime_duration_storage[process_storage, earlier_year]), model.years))
                    for process_storage in model.processes_storage)

                # Adding the sum over all transshipment processes
                + sum(
                    # sum over the construction years of the transshipment process
                    sum(
                        # impact distributed equally (0.5:0.5) between the two connected nodes
                        0.5 * model.impact_matrix_transshipment[impact_category, 'invest', process_transshipment, year, earlier_year]
                        # multiplied with the length and capacity of all connections of the node
                        * (
                            # sum over all connections, which have this node as first node
                            sum(model.new_capacity_transshipment[connection, process_transshipment, earlier_year]
                                * model.connection_length[connection]
                                for connection in filter(lambda connection: model.connected_nodes[connection, "node1"] == node, model.connections))
                            # sum over all connections, which have this node as second node
                            + sum(model.new_capacity_transshipment[connection, process_transshipment, earlier_year]
                                * model.connection_length[connection]
                                for connection in filter(lambda connection: model.connected_nodes[connection, "node2"] == node, model.connections))
                        )
                        for earlier_year in filter(lambda earlier_year: (earlier_year <= year) and (year - earlier_year < model.lifetime_duration_transshipment[process_transshipment, earlier_year]), model.years))
                    for process_transshipment in model.processes_transshipment)

                # adding the sum of the impacts of the transmission grid
                + sum(
                    # sum over the construction years of the transmission process
                    sum(
                        # impact distributed equally (0.5:0.5) between the two connected nodes
                        0.5 * model.impact_matrix_transmission[impact_category, 'invest', process_transmission, year, earlier_year]
                        # multiplied with the length and power capacity of all connections of the node
                        * (
                            # sum over all connections, which have this node as first node
                            sum(model.new_capacity_transmission[connection, process_transmission, earlier_year]
                                * model.power_limit_per_circuit[process_transmission]
                                * model.connection_length[connection]
                                for connection in filter(lambda connection: model.connected_nodes[connection, "node1"] == node, model.connections))
                            # sum over all connections, which have this node as second node
                            + sum(model.new_capacity_transmission[connection, process_transmission, earlier_year]
                                * model.power_limit_per_circuit[process_transmission]
                                * model.connection_length[connection]
                                for connection in filter(lambda connection: model.connected_nodes[connection, "node2"] == node, model.connections))
                        )
                        for earlier_year in filter(lambda earlier_year: (earlier_year <= year) and (year - earlier_year < model.lifetime_duration_transmission[process_transmission, earlier_year]), model.years))
                    for process_transmission in model.processes_transmission)
            )
            for time_slice in model.time_slices)
        )

    @staticmethod
    def constraint_invest_nodal_impact_limits_rule(model, node: int, impact_category: str, year: int) -> "pyomo rule":
        """
        This method is used as rule for the limitation of the invest impact vector summed over all processes at each node.

        Args:
            model: The equivalent of "self" in pyomo optimization models
            node (int): A node at which the pe.Constraint is active
            impact_category (str): An impact category which is assessed
            year (int): A year at which the pe.Constraint is active
        """

        return(
            # limit nodal invest impact
            model.invest_nodal_impact[node, impact_category, year] <=
            model.invest_nodal_impact_limits[node, impact_category, year]
            # adding an overshoot variable mainly for debugging feasibility issues
            + model.invest_nodal_impact_overshoot[node, impact_category, year]
        )

    # Overall pe.Constraint rules for the invest impact
    @staticmethod
    def constraint_invest_impact_rule(model, impact_category: str, year: int) -> "pyomo rule":
        """This method is used as rule for the calculation of the invest impact vector summed over all processes and nodes

        Args:
            model: The equivalent of "self" in pyomo optimization models
            impact_category (str): An impact category which is assessed
            year (int): A year at which the pe.Constraint is active
        """

        return(
            model.invest_impact[impact_category, year] ==
            sum(model.invest_nodal_impact[node, impact_category, year]
                for node in model.nodes)
        )

    @staticmethod
    def constraint_invest_impact_limits_rule(model, impact_category: str, year: int) -> "pyomo rule":
        """This method is used as rule for the limitation of the invest impact vector summed over all processes and nodes

        Args:
            model: The equivalent of "self" in pyomo optimization models
            impact_category (str): An impact category which is assessed
            year (int): A year at which the pe.Constraint is active
        """

        return(
            # limit overall operational impact
            model.invest_impact[impact_category, year] <=
            model.invest_impact_limits[impact_category, year]
            # adding an overshoot variable mainly for debugging feasibility issues
            + model.invest_impact_overshoot[impact_category, year]
        )

    ### Total Impact 
    @staticmethod
    def constraint_total_nodal_impact_rule(model, node: int, impact_category: str, year: int) -> "pyomo rule":
        """
        This method is used as rule for the calculation of the total impact vector summed over all processes at ecah node.

        Args:
            model: The equivalent of "self" in pyomo optimization models
            node (int): A node at which the pe.Constraint is active
            impact_category (str): An impact category which is assessed
            year (int): A year at which the pe.Constraint is active
        """

        return(
            model.total_nodal_impact[node, impact_category, year] ==
            model.operational_nodal_impact[node, impact_category, year]
            + model.invest_nodal_impact[node, impact_category, year]
        )

    @staticmethod
    def constraint_total_nodal_impact_limits_rule(model, node: int, impact_category: str, year: int) -> "pyomo rule":
        """
        This method is used as rule for the limitation of the total impact vector summed over all processes at each node.

        Args:
            model: The equivalent of "self" in pyomo optimization models
            node (int): A node at which the pe.Constraint is active
            impact_category (str): An impact category which is assessed
            year (int): A year at which the pe.Constraint is active
        """

        return(
            # limit nodal total impact
            model.total_nodal_impact[node, impact_category, year] <=
            model.total_nodal_impact_limits[node, impact_category, year]
            # adding an overshoot variable mainly for debugging feasibility issues
            + model.total_nodal_impact_overshoot[node, impact_category, year]
        )

    # Overall pe.Constraint rules for the total impact
    @staticmethod
    def constraint_total_impact_rule(model, impact_category: str, year: int) -> "pyomo rule":
        """This method is used as rule for the calculation of the total impact vector summed over all processes and nodes

        Args:
            model: The equivalent of "self" in pyomo optimization models
            impact_category (str): An impact category which is assessed
            year (int): A year at which the pe.Constraint is active
        """

        return(
            model.total_impact[impact_category, year] ==
            sum(model.total_nodal_impact[node, impact_category, year]
                for node in model.nodes)
        )

    @staticmethod
    def constraint_total_impact_limits_rule(model, impact_category: str, year: int) -> "pyomo rule":
        """This method is used as rule for the limitation of the total impact vector summed over all processes and nodes

        Args:
            model: The equivalent of "self" in pyomo optimization models
            impact_category (str): An impact category which is assessed
            year (int): A year at which the pe.Constraint is active
        """

        return(
            # limit overall total impact
            model.total_impact[impact_category, year] <=
            model.total_impact_limits[impact_category, year]
            # adding an overshoot variable mainly for debugging feasibility issues
            + model.total_impact_overshoot[impact_category, year]
        )

    #### OBJECTIVE
    # TC = CAPEX + OPEX
    @staticmethod
    def objectiveRule(model):
        """
        This method is used as an pe.Objective for the optimization, using weight factors for all impact categories.

        Args:
            model: The equivalent of "self" in pyomo optimization models
        """

        return (
            # sum over all years
            sum(
                # sum over all impact categories
                sum(
                    # sum of the operational impact over all impact categories multiplied by the corresponding weight factor
                    model.operational_impact[impact_category, year]
                    * model.objective_factor_impacts['operation', impact_category, year]
                    # sum of the invest  impact over all impact categories multiplied by the corresponding weight factor
                    + model.invest_impact[impact_category, year]
                    * model.objective_factor_impacts['invest', impact_category, year]
                    
                    # adding the sum of all impact overshoots multiplied by the corresponding weight factor
                    #sum nodal and overall overshoots
                    + (
                        # nodal limit overshoots
                        sum(model.operational_nodal_impact_overshoot[node, impact_category, year]
                            + model.invest_nodal_impact_overshoot[node, impact_category, year]
                            + model.total_nodal_impact_overshoot[node, impact_category, year]
                            for node in model.nodes)
                        # overall limit overshoots
                        + model.operational_impact_overshoot[impact_category, year]
                        + model.invest_impact_overshoot[impact_category, year]
                        + model.total_impact_overshoot[impact_category, year]
                    ) * model.objective_factor_impact_overshoot[impact_category, year]
                    for impact_category in model.impact_categories)
                for year in model.years)
        )