from pathlib import Path
import logging
import time
import os
import pickle
import numpy as np

# Setup logging
# all handlers from before are removed to avoid double printing
while bool(logging.getLogger().handlers):
    logging.getLogger().removeHandler(logging.getLogger().handlers[0])
log_format = '%(asctime)s %(filename)s: %(levelname)s: %(message)s'
if not os.path.exists("logs"):
            os.mkdir("logs")
logging.basicConfig(filename='logs/secmod.log', level=logging.INFO, format=log_format,
                    datefmt='%Y-%m-%d %H:%M:%S')
logging.propagate = False # prevent double printing

import secmod.data_preprocessing as dataprep
import secmod.setup as setup
import secmod.helpers as helpers
import secmod.data_processing as data_processing
from secmod.classes import ImpactCategory, ProcessImpacts, ProductionProcess, StorageProcess, TransshipmentProcess, TransmissionProcess, units, config

# MILP
from secmod.optimization_MILP import Optimization_MILP
import secmod.evaluation as evaluation

# Get current working directory
working_directory = Path.cwd()
setup.setup(working_directory, reset = False)

def main():
    """This is the main method of SecMOD.

    This method is called when SecMOD is called as a script instead of being imported,
    e.g. using the following command line in the command line::

        python -m secmod

    It is the main entrance point for starting a run of SecMOD.
    """

    if config.optimization_mode == 'MILP':
        helpers.log_heading("SecMOD - Startup")
    else:
        logging.error("You are using the SecMOD MILP framework.")
        logging.error("If you want to use the SecMOD framework for the optimization and life-cycle assessment of linear multi-sector systems, please visit:")
        logging.error("https://git-ce.rwth-aachen.de/ltt/secmod")
        exit()

    computed_input_path = working_directory / "SecMOD" / "00-INPUT" / "01-COMPUTED-INPUT"

    # Setup static variables
    ProcessImpacts.interest_rate = config.interest_rate
    ProcessImpacts.economic_period = units(config.economic_period)

    # Choose LCA framework - "ReCiPe Midpoint (H)", "ILCD 1.0.8 2016 midpoint" or "cumulative energy demand"
    ImpactCategory.FRAMEWORK = config.LCA_framework

    # Override used impact categories to reduce preparation time - "None" if FRAMEWORK should be used
    if hasattr(config, "LCA_manual_impact_categories"):
        ImpactCategory.MANUAL_IMPACT_CATEGORY_SELECTION = config.LCA_manual_impact_categories

    # Load raw data
    if config.load_raw_input:
        data_processing.load_raw_input(working_directory / "SecMOD" / "00-INPUT" / "00-RAW-INPUT")
    
    # Sort invest years in case someone put them in the wrong order
    config.invest_years = list(set(config.invest_years))
    config.invest_years.sort()

    # create directory for results
    if not os.path.exists(working_directory / "SecMOD" / "01-MODEL-RESULTS"):
            os.mkdir(working_directory / "SecMOD" / "01-MODEL-RESULTS")  
            
    ProcessImpacts.invest_years = config.invest_years
    input_dictionary = data_processing.generate_input_dictionary(computed_input_path, config.load_existing_input_dict)

    if config.optimization_mode == 'MILP':
        # initialize Optimization Model
        thisInvestmentOptimization_MILP = Optimization_MILP()
        thisInvestmentOptimization_MILP.run(input_dict=input_dictionary["magnitude"], solver=config.solver, solver_options=config.solver_options, debug=config.debug_optimization)
        with open(working_directory / "SecMOD" / "01-MODEL-RESULTS" / "InvestmentModel_{0}.pickle".format(min(config.invest_years)), "wb") as input_file:
            pickle.dump(thisInvestmentOptimization_MILP.model_instance, input_file)

    helpers.log_heading("Processing results")

    logging.info("Save processes to {0}".format(working_directory / "SecMOD" / "01-MODEL-RESULTS"))
    # Save result in classes:
    with open(working_directory / "SecMOD" / "01-MODEL-RESULTS" / "ProductionProcesses.pickle", "wb") as input_file:
        pickle.dump(ProductionProcess.processes, input_file)
    with open(working_directory / "SecMOD" / "01-MODEL-RESULTS" / "StorageProcesses.pickle", "wb") as input_file:
        pickle.dump(StorageProcess.processes, input_file)
    with open(working_directory / "SecMOD" / "01-MODEL-RESULTS" / "TransshipmentProcesses.pickle", "wb") as input_file:
        pickle.dump(TransshipmentProcess.processes, input_file)
    with open(working_directory / "SecMOD" / "01-MODEL-RESULTS" / "TransmissionProcesses.pickle", "wb") as input_file:
        pickle.dump(TransmissionProcess.processes, input_file)

    logging.info("Save impacts to {0}".format(working_directory / "SecMOD" / "01-MODEL-RESULTS"))
    # Save result in classes:
    with open(working_directory / "SecMOD" / "01-MODEL-RESULTS" / "ImpactCategories.pickle", "wb") as input_file:
        pickle.dump(ImpactCategory.get_list_of_active_impact_categories, input_file)

    logging.info("\n\n")
    logging.info("========================================")
    logging.info("=               THE END!               =")
    logging.info("========================================")
    # Start evaluation
    evaluation.start_evaluation(working_directory)
    
    logging.getLogger().handlers.clear()
    logging.shutdown()

if __name__ == "__main__":
    main()
