import pandas as pd
import pint
import logging
import copy
import numpy as np
import importlib.util
from datetime import datetime

def isInteger(text: str):
    """Checks if string could be an integer."""
    
    try:
        int(text)
    except ValueError:
        return False
    else:
        return True

def unitize_dataframe(dataframe: pd.DataFrame, units: pint.UnitRegistry):
    """Multiplied every row of a dataframe with the corresponding unit and drops the unit column."""

    df_units = dataframe["unit"].apply(lambda row: units(str(row)))
    unitized_dataframe = dataframe.drop("unit", axis=1)
    unitized_dataframe = unitized_dataframe.apply(lambda column: column * df_units)

    return unitized_dataframe

def log_heading(message: str):
    """Takes a message and creates a nicely formatted heading log message."""

    message = "=   {0}   =".format(message)
    length  = len(message)
    logging.info("\n\n\n" + "=" * length + "\n" + message + "\n" + "=" * length + "\n")

def clear_filename_special_character(filename: str):

    filename = filename.replace(":","_")
    filename = filename.replace(" ","_")
    filename = filename.replace("\\","_")
    filename = filename.replace("/","_")
    filename = filename.replace("*","_")
    filename = filename.replace("?","_")
    filename = filename.replace("<","_")
    filename = filename.replace(">","_")
    filename = filename.replace("|","_")
    filename = filename.replace('"',"_")

    return filename

def convert_date(date_old, correct_dateformat, partially_correct_dateformat, wrong_dateformat):
    """ converts date to correct dateformat """
    if type(date_old)!= str:
        raise TypeError("Date format of timeseries indizes needs to be of type 'str'. The type of '{}' is {}".format(date_old,type(date_old)))
    try: # check if date already in right format
        date = datetime.strptime(date_old, correct_dateformat)
        return date_old
    except: # if known wrong format, change format
        try:
            date = datetime.strptime(date_old, partially_correct_dateformat) # if known partially correct format, change format
            date = date.strftime(correct_dateformat)
            return date
        except:
            try:
                date = datetime.strptime(date_old, wrong_dateformat) # if known wrong format, change format
                date = date.strftime(correct_dateformat)
                return date
            except: # unknown wrong format 
                raise TypeError("Date format of timeseries index unknown. {} is not of any known dateformat type".format(date_old))

def correct_time_stamp_of_timeseries(timeseries):
    """ corrects time stamp of timeseries 
    
    format desired, which is given in config (e.g. dd.mm.yyyy HH:MM)"""
    
    # Load config.py
    spec = importlib.util.spec_from_file_location("config", "SecMOD/00-INPUT/00-RAW-INPUT/config.py")
    config = importlib.util.module_from_spec(spec)
    spec.loader.exec_module(config)
    
    if hasattr(config,'time_series_aggregation_config') and config.time_series_aggregation_config["use_time_series_aggregation"]: # if time series aggregation is used
        if not isinstance(timeseries, pd.MultiIndex): # if not a multiindex
            if timeseries.index.name == 'time slice':
                corrected_timeseries = copy.deepcopy(timeseries)
                correct_dateformat = config.time_series_aggregation_config["correct_dateformat"]
                partially_correct_dateformat = config.time_series_aggregation_config["partially_correct_dateformat"]
                wrong_dateformat = config.time_series_aggregation_config["wrong_dateformat"]
                corrected_timeseries['time slice'] = list(map(lambda date: convert_date(date, correct_dateformat, partially_correct_dateformat, wrong_dateformat),timeseries.index.values)) #  get corrected index
                # Convert time slice to time stamp to pd_datetime type to allow for correct chronological sorting of dates.
                # If time stamps are provided as strings, sorting time slices with pd.sort_index() (e.g. in get_combined_demand_time_series) results in incorrect chronological order of time slices
                # Time series need to be correctly sorted for typical period aggregation and the seasonal storage formulation
                corrected_timeseries['time slice']= pd.to_datetime(corrected_timeseries['time slice'],format=correct_dateformat)
                corrected_timeseries.set_index('time slice',drop=True,inplace = True)
        else:
            raise NotImplementedError
        return corrected_timeseries
    else:
        return timeseries

def get_rwth_colors():
    """ return RWTH colors as array """
    rwth_colors = [
        (0,104,180),
        (0,97,101),
        (0,152,161),
        (87,171,39),
        (189,205,0),
        (246,168,0),
        (204,7,30),
        (161,16,53),
        (97,33,88),
        (122,111,172),
    ]
    rwth_colors = np.divide(rwth_colors,256) # percentage of 256 spectrum
    return rwth_colors