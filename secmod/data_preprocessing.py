import os
import logging
import shutil
import requests
from clint.textui import progress
from pathlib import Path

import datapackage as dp


def download_datapackage(url: str, download_path: Path) -> bool:
    """This method is used to download an Open Source Data Package.

    Using the URL to the meta data JSON-file of the data package,
    the complete data package is downloaded into a download folder.
    If the download has been successfull a boolean True is returned.

    Args:
        url (str): URL to the directory of the meta data JSON-file of the data package 
            (e.g. https://data.open-power-system-data.org/renewable_power_plants/latest/datapackage.json)
        download_path (Path): Path of the directory where the data package is to be saved
    """

    # try to open the remote datapackage
    try:
        datapackage = dp.Package(url + "datapackage.json")
    # handle exception, if data package is invalid
    except dp.exceptions.ValidationError as e:
        # show error message
        logging.error("Datapackage is invalid!\n\nPlease check the source:\n{0}\n\nDetails:\n{1}".format(url, str(e)))
        # return False since the download wasn´t successfull
        return False
    # handle exception, if some other data package exception has been raised
    except dp.exceptions.DataPackageException as e:
        # show error message
        logging.error("Datapackage could not be opened!\n\nPlease check the provided URL and your network connection:\n{0}\n\nDetails:\n{1}".format(url, str(e)))
        # return False since the download wasn´t successfull
        return False

    # create download directory, if not existent
    create_directory(download_path)
    # download meta data JSON-file
    download_file(
        url + "datapackage.json", download_path / "datapackage.json")
    # download readme.md, if existing
    try:
        download_file(url + "README.md", download_path / "README.md")
    except Exception:
        pass
    # download all ressources, stated in the meta data JSON-file
    for resource in datapackage.resources:
        try:
            download_file(resource.source, download_path /
                          resource.descriptor['path'], resource.descriptor['bytes'])
        except Exception:
            download_file(resource.source, download_path /
                          resource.descriptor['path'])

    return True


def datapackage_update_available(url: str, download_path: Path) -> bool:
    """This method checks if there is an update available for a remote data package.

    First the methods tries to open the remote data package to check its availability.
    If it is not available, False is returned.

    After that the local data package is opened, if it exists.
    If it does not exist, True is returned.

    If both sources are accessible the version information from the meta data is compared.
    Because the remote source is expected to have the newest version, any difference in the
    version information returns True.

    Args:
        url (str): URL to the directory of the meta data JSON-file of the data package 
            (e.g. https://data.open-power-system-data.org/renewable_power_plants/latest/datapackage.json)
        download_path (Path): Path of the directory where the data package is to be saved
    """

    # try to open the remote data package meta data
    try:
        remote_dp = dp.Package(url + "datapackage.json")
        full_path = download_path / remote_dp.descriptor['name']
    # handle exception, if data package is invalid
    except dp.exceptions.ValidationError as e:
        # show error message
        logging.error("Remote datapackage is invalid!\n\nDetails:\n" + str(e))
        # return False since the download wasn´t successfull
        return False
    # handle exception, if some other data package exception has been raised
    except dp.exceptions.DataPackageException as e:
        # show error message
        logging.error("Remote datapackage could not be opened!\nPlease check the provided URL and your network connection.\n\nDetails:\n" + str(e))
        # return False since the download wasn´t successfull
        return False

    # check if data package exists locally
    if ((full_path / "datapackage.json").exists()):
        local_dp = dp.Package(str(full_path / "datapackage.json"))
        if (local_dp.descriptor['version'] == remote_dp.descriptor['version']):
            return False
        else:
            return True
    else:
        return True


def download_file(url: str, save_to: str, expected_size: int = None):
    """This method downloads files using streaming.

    A get request is used to download the file from the URL.
    The response is streamed into a local file to minimize RAM usage.

    Args:
        url (str): URL to the file
        save_to (str): local path and filename to be saved to
    """
    print("Downloading " + url.split('/')[-1])
    response = requests.get(url, stream=True)

    chunk_size = 8192
    file_handle = open(save_to, "wb")
    if expected_size == None:
        try:
            total_length = int(response.headers.get('content-length'))
        except Exception:
            total_length = 100000000
    else:
        total_length = expected_size
    for chunk in progress.bar(response.iter_content(chunk_size=chunk_size), expected_size=(total_length/chunk_size) + 1):
        if chunk:
            file_handle.write(chunk)


def is_setup(working_directory: Path) -> bool:
    """This method checks if SecMOD has been set up before.

    This is achieved by checking if the folder "SecMOD" exists in
    the working directory and returning a boolean. If it does not exist,
    the method returns False.

    Args:
        working_directory (Path): Path of the working directory
    """

    if (working_directory / "SecMOD").exists():
        return True
    else:
        return False


def create_directory(path: Path):
    """This method creates a new directory, if it does not exist, yet.

    The method uses pathlib to check for the existence of a path.
    If the path does not exist yet, it get created.

    Args:
        path (Path): Path of the directory to be created
    """

    if not path.exists():
        path.mkdir(parents=True)
