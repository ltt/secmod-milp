import logging
import pickle
import os
import platform
from pathlib import Path
from clint.textui import progress
import pandas as pd
import numpy as np
import pyomo.environ as pyo
import pyomo.core.base as pyo_base
import csv
import copy

log_format = '%(asctime)s %(filename)s: %(levelname)s: %(message)s'
if not os.path.exists("logs"):
            os.mkdir("logs")
logging.basicConfig(filename='logs/secmod.log', level=logging.INFO, format=log_format,
                    datefmt='%Y-%m-%d %H:%M:%S')
logging.getLogger().addHandler(logging.StreamHandler())

import secmod.data_preprocessing as dataprep
import secmod.data_processing as dataproc
import secmod.helpers as helpers
from secmod.classes import (
EcoinventImpacts, Grid, ImpactCategory, Process, ProcessImpacts, Product,
ProductionProcess, ProductionProcessEcoinvent, StorageProcess,
StorageProcessEcoinvent, TransmissionProcess, TransmissionProcessEcoinvent,
TransshipmentProcess, TransshipmentProcessEcoinvent, config, units,
NodalProcess, ConnectionProcess)

# MILP 
from secmod.optimization_MILP import (Optimization_MILP)
                                
def extract_results(working_directory, debug=False):
    """ extracts the results of the optimization """
    if debug == True:
        config.invest_years = [2035]

    logging.info("Load processes")
    optimization_results = {'Results': {}, 'Utilities': {'Unit_dict':{},'Index_names':{},'Unit_time_summation': {}, 'Parameters':{},'ProductsOfProcess':{},'Misc':{}}}
    optimization_results['Utilities']['Parameters'] = {'processes':{},'products':{},'impact_categories':[], 
                                                       'lifetime_duration_production': None, 'lifetime_duration_storage': None}

    deunitized_input_dictionary = {'Units':{}}
    index_names = {}  
    # define output balances selected from model instance
    list_output_balances =[
                        'non_served_demand', 
                        'new_capacity_production', 
                        'new_capacity_storage', 
                        'new_capacity_transshipment',
                        'new_capacity_transmission',
                        'existing_capacity_production', 
                        'existing_capacity_storage', 
                        'existing_capacity_transshipment',
                        'existing_capacity_transmission',
                        'demand'
                        ]
    # Load units and Process/Product/Impact names 
    with open(working_directory / "SecMOD" / "00-INPUT" / "01-COMPUTED-INPUT" / "input-{0}.pickle".format(config.invest_years[0]), "rb") as input_file:
        input_dictionary = pickle.load(input_file)
    deunitized_input_dictionary['Units'] = dataproc.deunitize_input_dictionary({None: input_dictionary})["unit"]

    # Load optimization results
    logging.info("Loading optimization results")

    for year in progress.bar(config.invest_years):
    # create abstract optimization model of 'LP' or 'MILP', respectively
        if config.optimization_mode == 'LP':
            # optimization = InvestmentOptimization()
            logging.error("results processing only for MILP")
        elif config.optimization_mode == 'MILP':
            optimization = Optimization_MILP()
            
        #if config.optimization_kind == "RH": # Rolling Horizon
            #year_file = year
        #else:
        year_file = min(config.invest_years)
        
        with open(working_directory / "SecMOD" / "01-MODEL-RESULTS" / "InvestmentModel_{0}.pickle".format(year_file), "rb") as input_file:
        # with open(working_directory / "SecMOD" / "01-MODEL-RESULTS" / .format(year), "rb") as input_file:
            if year == config.invest_years[0]:
                    if platform.system() == 'Windows':
                        date_creation_result_file = os.path.getmtime(input_file.name)
                        # different system
                    else:
                        date_creation_result_file = os.stat(input_file.name).st_mtime
                    # load optimization result instance
                    model_instance = pickle.load(input_file)
        # calculate total duration of time horizon 
        model_instance.total_time_slice_duration = sum(model_instance.time_slice_yearly_weight[time_slice] for time_slice in model_instance.time_slices)
        # iterate through variables in result
        logging.info("Loading optimization variable for year {}".format(str(year)))
        for balance_in_optimization_name in progress.bar(list_output_balances):
            balance_in_optimization = eval('model_instance.' + balance_in_optimization_name)
            # get index of 'year' index and list of index_names
            [index_names[balance_in_optimization_name], idxyear] = get_list_of_index_names(balance_in_optimization)
            if isinstance(balance_in_optimization, pyo_base.var.Var): # if variable
                if balance_in_optimization_name not in optimization_results['Results']:
                    # get results for first year
                    optimization_results['Results'][balance_in_optimization_name] = pd.DataFrame(optimization.get_dataframe_from_result(balance_in_optimization, idxyear)[year])
                else:
                    # append results for subsequent years
                    optimization_results['Results'][balance_in_optimization_name] = optimization_results['Results'][balance_in_optimization_name].join(pd.DataFrame(optimization.get_dataframe_from_result(balance_in_optimization, idxyear)[year]),how='outer')
            elif isinstance(balance_in_optimization, pyo_base.param.Param): # if parameter
                if balance_in_optimization_name not in optimization_results['Results']:
                    # get results for first year
                    optimization_results['Results'][balance_in_optimization_name] = pd.DataFrame(optimization.get_dataframe_from_parameter(balance_in_optimization, idxyear)[year])
                else:
                    # append results for subsequent years
                    optimization_results['Results'][balance_in_optimization_name] = optimization_results['Results'][balance_in_optimization_name].join(pd.DataFrame(optimization.get_dataframe_from_parameter(balance_in_optimization, idxyear)[year]),how='outer')
            optimization_results['Results'][balance_in_optimization_name].fillna(0,inplace=True)
            optimization_results['Results'][balance_in_optimization_name].columns.names = ['invest_year']


    # calculate PRODUCT FLOWS for production, storage, transmission and transshipment and total
        logging.info("Calculate product flows for year {}".format(str(year)))
        # set up Dataframe [products, nodes, time_slices]/[products,product_process, nodes, time_slices]
        if year == config.invest_years[0]:
            # set index of total product flow
            index_names['product_flow'] = ['products','nodes','time_slices']
            # get product, nodes and time slices of product flow
            index_product_flow = [model_instance.products.value_list,model_instance.nodes.value_list,model_instance.time_slices.value_list]
            # create multiindex of total product flow
            index_product_flow = pd.MultiIndex.from_product(index_product_flow, names = index_names['product_flow'])
            # create dataframe of total product flow
            optimization_results['Results']['product_flow'] = pd.DataFrame(index = index_product_flow,columns=config.invest_years)
            optimization_results['Results']['product_flow'].columns.names = ['invest_year']
            # analogously create empty dataframe for process types without transmission
            process_types = ['production','storage','transshipment']
            # set index of product flow
            index_names_product_flow_processes = ['products','product_process','nodes','time_slices']
            for process_type in process_types:
                # get product, process, nodes and time slices of product flow
                index_product_flow = [model_instance.products.value_list,eval('model_instance.processes_'+process_type+'.value_list'), model_instance.nodes.value_list,model_instance.time_slices.value_list]
                # create multiindex of product flow
                index_names['product_flow_'+process_type] = index_names_product_flow_processes
                index_product_flow = pd.MultiIndex.from_product(index_product_flow, names = index_names['product_flow_'+process_type])
                # create dataframe of total product flow
                optimization_results['Results']['product_flow_'+ process_type] = pd.DataFrame(index = index_product_flow,columns=config.invest_years)
                optimization_results['Results']['product_flow_'+ process_type].columns.names = ['invest_year']
            
            # copy of dataframe(product_flow) for transmission
            optimization_results['Results']['product_flow_transmission'] = copy.deepcopy(optimization_results['Results']['product_flow'])
            index_names['product_flow_transmission'] = copy.deepcopy(index_names['product_flow'])

            # copy dataframe for add_demand and non_served demand 
            # copy dataframe of product flow for non_served_demand
            optimization_results['Results']['product_flow_non_served_demand'] = copy.deepcopy(optimization_results['Results']['product_flow'])
            index_names['product_flow_non_served_demand'] = copy.deepcopy(index_names['product_flow'])
            # copy dataframe of product flow for add_demand
            if config.optimization_mode == 'MILP':
                optimization_results['Results']['product_flow_add_demand'] = copy.deepcopy(optimization_results['Results']['product_flow'])
                index_names['product_flow_add_demand'] = copy.deepcopy(index_names['product_flow'])

            # copy dataframe for storage deposit and withdrawal
            optimization_results['Results']['product_flow_deposit_into_storage'] = copy.deepcopy(optimization_results['Results']['product_flow_storage'])
            index_names['product_flow_deposit_into_storage'] = copy.deepcopy(index_names['product_flow_storage'])  

            optimization_results['Results']['product_flow_withdrawal_from_storage'] = copy.deepcopy(optimization_results['Results']['product_flow_storage'])
            index_names['product_flow_withdrawal_from_storage'] = copy.deepcopy(index_names['product_flow_storage'])
            
                    
        # calculate Product flows
        # production processes: product_flow
        optimization_results['Results']['product_flow_production'][year] = list(map(lambda x: get_product_flow_production(model_instance,year,x),
            optimization_results['Results']['product_flow_production'].index.values))
       # calculate add_demand and non_served_demand
        # production processes: non_served_demand
        optimization_results['Results']['product_flow_non_served_demand'][year] = list(map(lambda x: get_product_flow_production_non_served_demand(model_instance,year,x),
            optimization_results['Results']['product_flow_non_served_demand'].index.values))
        
         # production processes:add_demand
        if config.optimization_mode == 'MILP':
            optimization_results['Results']['product_flow_add_demand'][year] = list(map(lambda x: get_product_flow_production_add_demand(model_instance,year,x),
                optimization_results['Results']['product_flow_add_demand'].index.values))
        
        # storage processes deposit
        optimization_results['Results']['product_flow_deposit_into_storage'][year] = list(map(lambda x: get_product_flow_into_storage(model_instance,year,x),
            optimization_results['Results']['product_flow_storage'].index.values))
        
        # storage processes withdrawal
        optimization_results['Results']['product_flow_withdrawal_from_storage'][year] = list(map(lambda x: get_product_flow_out_of_storage(model_instance,year,x),
            optimization_results['Results']['product_flow_storage'].index.values))        
        # storage processes
        optimization_results['Results']['product_flow_storage'][year] = list(map(lambda x: get_product_flow_storage(model_instance,year,x),
            optimization_results['Results']['product_flow_storage'].index.values))
        # transshipment processes
        optimization_results['Results']['product_flow_transshipment'][year] = list(map(lambda x: get_product_flow_transshipment(model_instance,year,x),
            optimization_results['Results']['product_flow_transshipment'].index.values))
        # transmission processes
        optimization_results['Results']['product_flow_transmission'][year] = list(map(lambda x: get_product_flow_transmission(model_instance,year,x),
            optimization_results['Results']['product_flow_transmission'].index.values))
        # total
        optimization_results['Results']['product_flow'][year] = (
            optimization_results['Results']['product_flow_production'][year].sum(level=[0,2,3])
            + optimization_results['Results']['product_flow_storage'][year].sum(level=[0,2,3])
            + optimization_results['Results']['product_flow_transshipment'][year].sum(level=[0,2,3])
            + optimization_results['Results']['product_flow_transmission'][year].fillna(0)
            # w/o add_demand and non_served_demand
        )
        
    # calculate IMPACTS for production, storage, transmission and transshipment and total
        logging.info("Calculate impacts for year {}".format(str(year)))
        # set up Dataframe [impact_category, nodes, time_slices]/[impact_category, impact_process/impact_product, nodes, time_slices]
        if year == config.invest_years[0]:
            # for OVERALL
            # set index of impacts
            index_names['operational_impact'] = ['impact_category','nodes','time_slices']
            index_names['invest_impact'] = ['impact_category','nodes','time_slices']
            # get product, nodes and time slices of impacts
            index_impact = [model_instance.impact_categories.value_list,
                model_instance.nodes.value_list,
                model_instance.time_slices.value_list]
            # create multiindex of impacts
            index_impact = pd.MultiIndex.from_product(index_impact, names = index_names['operational_impact'])
            # create dataframe of impacts
            optimization_results['Results']['operational_impact'] = pd.DataFrame(index = index_impact,columns=config.invest_years)
            optimization_results['Results']['operational_impact'].columns.names = ['invest_year']
            optimization_results['Results']['invest_impact'] = copy.deepcopy(optimization_results['Results']['operational_impact'])

            # for PROCESSES
            # analogously create empty dataframe for process types 
            process_types = ['production','storage','transshipment','transmission']
            # set index of impact
            index_names_impacts_processes = ['impact_category','impact_process','nodes','time_slices']
            for process_type in process_types:
                # get impact category, process, nodes and time slices of product flow
                index_impact = [model_instance.impact_categories.value_list,eval('model_instance.processes_'+process_type+'.value_list'), model_instance.nodes.value_list,model_instance.time_slices.value_list]
                # create multiindex of product flow
                index_names['operational_impact_'+process_type] = index_names_impacts_processes
                index_names['invest_impact_'+process_type] = index_names_impacts_processes
                index_impact = pd.MultiIndex.from_product(index_impact, names = index_names['operational_impact_'+process_type])
                # create dataframe of total product flow
                optimization_results['Results']['operational_impact_'+ process_type] = pd.DataFrame(index = index_impact,columns=config.invest_years)
                optimization_results['Results']['operational_impact_'+ process_type].columns.names = ['invest_year']
                optimization_results['Results']['invest_impact_'+ process_type] = copy.deepcopy(optimization_results['Results']['operational_impact_'+ process_type])

            # for NON_SERVED_DEMAND
            # analogously create empty dataframe for non served demand
            demand_type = 'non_served_demand'
            # set index of impact
            index_names_impacts_demand = ['impact_category','impact_products','nodes','time_slices']
            # get impact category, process, nodes and time slices of product flow
            index_impact = [model_instance.impact_categories.value_list,model_instance.products.value_list, model_instance.nodes.value_list,model_instance.time_slices.value_list]
            # create multiindex of product flow
            index_names['operational_impact_'+demand_type] = index_names_impacts_demand
            index_names['invest_impact_'+demand_type] = index_names_impacts_demand
            index_impact = pd.MultiIndex.from_product(index_impact, names = index_names['operational_impact_'+demand_type])
            # create dataframe of total product flow
            optimization_results['Results']['operational_impact_'+ demand_type] = pd.DataFrame(index = index_impact,columns=config.invest_years)
            optimization_results['Results']['operational_impact_'+ demand_type].columns.names = ['invest_year']
            optimization_results['Results']['invest_impact_'+ demand_type] = copy.deepcopy(optimization_results['Results']['operational_impact_'+ demand_type])

            # for ADD_DEMAND: analogously create empty dataframe for additional demand
            demand_type = 'add_demand'
            # set index of impact
            index_names_impacts_demand = ['impact_category','impact_products','nodes','time_slices']
            # get impact category, process, nodes and time slices of product flow
            index_impact = [model_instance.impact_categories.value_list,model_instance.products.value_list, model_instance.nodes.value_list,model_instance.time_slices.value_list]
            # create multiindex of product flow
            index_names['operational_impact_'+demand_type] = index_names_impacts_demand
            index_names['invest_impact_'+demand_type] = index_names_impacts_demand
            index_impact = pd.MultiIndex.from_product(index_impact, names = index_names['operational_impact_'+demand_type])
            # create dataframe of total product flow
            optimization_results['Results']['operational_impact_'+ demand_type] = pd.DataFrame(index = index_impact,columns=config.invest_years)
            optimization_results['Results']['operational_impact_'+ demand_type].columns.names = ['invest_year']
            optimization_results['Results']['invest_impact_'+ demand_type] = copy.deepcopy(optimization_results['Results']['operational_impact_'+ demand_type])


        # calculate Impacts
        # production processes
        optimization_results['Results']['operational_impact_production'][year] = list(map(lambda x: get_operational_impact_production(model_instance,year,x),
            optimization_results['Results']['operational_impact_production'].index.values))
        optimization_results['Results']['invest_impact_production'][year] = list(map(lambda x: get_invest_impact_production(model_instance,year,x),
            optimization_results['Results']['invest_impact_production'].index.values))
        # storage processes
        optimization_results['Results']['operational_impact_storage'][year] = list(map(lambda x: get_operational_impact_storage(model_instance,year,x),
            optimization_results['Results']['operational_impact_storage'].index.values))
        optimization_results['Results']['invest_impact_storage'][year] = list(map(lambda x: get_invest_impact_storage(model_instance,year,x),
            optimization_results['Results']['invest_impact_storage'].index.values))
        # transshipment processes
        optimization_results['Results']['operational_impact_transshipment'][year] = list(map(lambda x: get_operational_impact_transshipment(model_instance,year,x),
            optimization_results['Results']['operational_impact_transshipment'].index.values))
        optimization_results['Results']['invest_impact_transshipment'][year] = list(map(lambda x: get_invest_impact_transshipment(model_instance,year,x),
           optimization_results['Results']['invest_impact_transshipment'].index.values))
        # transmission processes
        optimization_results['Results']['operational_impact_transmission'][year] = list(map(lambda x: get_operational_impact_transmission(model_instance,year,x),
            optimization_results['Results']['operational_impact_transmission'].index.values))
        optimization_results['Results']['invest_impact_transmission'][year] = list(map(lambda x: get_invest_impact_transmission(model_instance,year,x),
           optimization_results['Results']['invest_impact_transmission'].index.values))
        # non served demand
        optimization_results['Results']['operational_impact_non_served_demand'][year] = list(map(lambda x: get_operational_impact_non_served_demand(model_instance,year,x),
            optimization_results['Results']['operational_impact_non_served_demand'].index.values))
        optimization_results['Results']['invest_impact_non_served_demand'][year] = list(map(lambda x: get_invest_impact_non_served_demand(model_instance,year,x),
            optimization_results['Results']['invest_impact_non_served_demand'].index.values))
        # add demand 
        optimization_results['Results']['operational_impact_add_demand'][year] = list(map(lambda x: get_operational_impact_add_demand(model_instance,year,x),
            optimization_results['Results']['operational_impact_add_demand'].index.values))
        optimization_results['Results']['invest_impact_add_demand'][year] = list(map(lambda x: get_invest_impact_add_demand(model_instance,year,x),
            optimization_results['Results']['invest_impact_add_demand'].index.values))
        
        # overall
        optimization_results['Results']['operational_impact'][year] = (
            optimization_results['Results']['operational_impact_production'][year].sum(level=[0,2,3])
            + optimization_results['Results']['operational_impact_storage'][year].sum(level=[0,2,3])
            + optimization_results['Results']['operational_impact_transshipment'][year].sum(level=[0,2,3])
            + optimization_results['Results']['operational_impact_transmission'][year].sum(level=[0,2,3])
            + optimization_results['Results']['operational_impact_non_served_demand'][year].sum(level=[0,2,3])
            # added add_demand to overall operational impact
            - optimization_results['Results']['operational_impact_add_demand'][year].sum(level=[0,2,3])
        )
        optimization_results['Results']['invest_impact'][year] = (
            optimization_results['Results']['invest_impact_production'][year].sum(level=[0,2,3])
            + optimization_results['Results']['invest_impact_storage'][year].sum(level=[0,2,3])
            + optimization_results['Results']['invest_impact_transshipment'][year].sum(level=[0,2,3])
            + optimization_results['Results']['invest_impact_transmission'][year].sum(level=[0,2,3])
            + optimization_results['Results']['invest_impact_non_served_demand'][year].sum(level=[0,2,3])
            - optimization_results['Results']['invest_impact_add_demand'][year].sum(level=[0,2,3])
        )

    # sum operational impact and invest impact for total impact
    # OVERALL
    index_names['total_impact'] = index_names['operational_impact']
    optimization_results['Results']['total_impact'] = optimization_results['Results']['operational_impact'].add(optimization_results['Results']['invest_impact'], fill_value = 0)
    # PROCESSES
    for process_type in process_types:
        index_names['total_impact_'+process_type] = index_names['operational_impact_'+process_type]
        optimization_results['Results']['total_impact_'+process_type] = optimization_results['Results']['operational_impact_'+process_type].add(optimization_results['Results']['invest_impact_'+process_type], fill_value = 0)
    # NON SERVED DEMAND
    index_names['total_impact_non_served_demand'] = index_names['operational_impact_non_served_demand']
    optimization_results['Results']['total_impact_non_served_demand'] = optimization_results['Results']['operational_impact_non_served_demand'].add(optimization_results['Results']['invest_impact_non_served_demand'], fill_value = 0)
    # ADD DEMAND
    index_names['total_impact_add_demand'] = index_names['operational_impact_add_demand']
    optimization_results['Results']['total_impact_add_demand'] = optimization_results['Results']['operational_impact_add_demand'].add(optimization_results['Results']['invest_impact_add_demand'], fill_value = 0)
    
    # iterate through sets in result for last year to get names of parameters
    for balance_in_optimization in model_instance.component_objects(pyo.Set,active=True):
        # search for prefix "processes" in balance_in_optimization make sure that only process_types are used
        if 'processes' in balance_in_optimization.name and balance_in_optimization.name in ('processes_'+p for p in process_types): 
            optimization_results['Utilities']['Parameters']['processes'][balance_in_optimization.name] = balance_in_optimization.value_list
        if 'products' == balance_in_optimization.name: 
            optimization_results['Utilities']['Parameters'][balance_in_optimization.name] = balance_in_optimization.value_list
        if 'impact_categories' == balance_in_optimization.name: 
            optimization_results['Utilities']['Parameters']['impact_categories'] = balance_in_optimization.value_list
    
    # get lifetime duration 
    for p in ['lifetime_duration_production','lifetime_duration_storage']:
        optimization_results['Utilities']['Parameters'][p] = optimization.get_dataframe_from_parameter(getattr(model_instance,p))

    # get process corresponding to products
    optimization_results['Utilities']['ProductsOfProcess']['production_products'] = {}
    optimization_results['Utilities']['ProductsOfProcess']['storage_products'] = {}
    optimization_results['Utilities']['ProductsOfProcess']['transmission_products'] = {}
    optimization_results['Utilities']['ProductsOfProcess']['transshipment_products'] = {}
    for product in optimization_results['Utilities']['Parameters']['products']:
        optimization_results['Utilities']['ProductsOfProcess']['production_products'][product] = []
        optimization_results['Utilities']['ProductsOfProcess']['storage_products'][product] = []
        optimization_results['Utilities']['ProductsOfProcess']['transmission_products'][product] = []
        optimization_results['Utilities']['ProductsOfProcess']['transshipment_products'][product] = []
        # get production processes
        for production_process in optimization_results['Utilities']['Parameters']['processes']['processes_production']:
            if config.optimization_mode == 'LP':
                if input_dictionary['technology_matrix_production'][(product, production_process, input_dictionary['years'][None][0])].magnitude != 0:
                    optimization_results['Utilities']['ProductsOfProcess']['production_products'][product].append(production_process)
            elif config.optimization_mode == 'MILP':
                # only interestd in names --> thus, not necessary to multiply with xsi
                if sum(input_dictionary['technology_matrix_production'][(vertex, product, production_process, input_dictionary['years'][None][0])].magnitude 
                   for vertex in range(model_instance.cornerpoints_number[production_process]-1)) != 0:
                    optimization_results['Utilities']['ProductsOfProcess']['production_products'][product].append(production_process)

        # get storage processes
        for storage_process in optimization_results['Utilities']['Parameters']['processes']['processes_storage']:
            if input_dictionary['storage_products'][storage_process] == product:
                optimization_results['Utilities']['ProductsOfProcess']['storage_products'][product].append(storage_process)
        # get transmission processes
        for transmission_process in optimization_results['Utilities']['Parameters']['processes']['processes_transmission']:
            if product in input_dictionary['products_transmission'][None]:
                optimization_results['Utilities']['ProductsOfProcess']['transmission_products'][product].append(transmission_process)
        # get transshipment processes
        for transshipment_process in optimization_results['Utilities']['Parameters']['processes']['processes_transshipment']:
            if input_dictionary['transshipment_products'][transshipment_process] == product:
                optimization_results['Utilities']['ProductsOfProcess']['transshipment_products'][product].append(transshipment_process)

    # set index names
    for balance_in_optimization in optimization_results['Results']:
        # if not "transmission" in balance_in_optimization and not "transshipment" in balance_in_optimization:
            optimization_results['Results'][balance_in_optimization].index.names = index_names[balance_in_optimization]

    # add new capacities to existing capacity for transition pathway
    for process_category in progress.bar(optimization_results['Utilities']['Parameters']['processes'].keys()):
        category = process_category.replace('processes','')
        for process in optimization_results['Utilities']['Parameters']['processes'][process_category]:
            for year in config.invest_years:
                for yoc in config.invest_years:
                    if year == yoc:
                        # existing capacity equals new_capacity
                        optimization_results['Results']['existing_capacity'+category].loc[(slice(None),process,yoc),yoc]=optimization_results['Results']['new_capacity'+category][yoc]
                    elif year > yoc and year-yoc < optimization_results['Utilities']['Parameters']['lifetime_duration'+category][process,yoc]:
                        # propagate new capacities to years after years_construction within lifetime duration
                        optimization_results['Results']['existing_capacity'+category].loc[(slice(None),process,yoc),year] = optimization_results['Results']['new_capacity'+category][yoc]
                    else:
                        optimization_results['Results']['existing_capacity'+category].loc[(slice(None),process,yoc),year] = 0
    logging.info('New capacities added to existing capacities')

    # group balance by time slice and nodes/connections/construction years
    # and copy as new balance
    results_temp = {}
    for balance_in_optimization in progress.bar(optimization_results['Results'].keys()):
        # group by time slice and return balance, unit_time_summation appendix and index name
        optimization_results['Results'][balance_in_optimization], optimization_results['Utilities']['Unit_time_summation'][balance_in_optimization], index_names[balance_in_optimization] = \
            group_by_time_slice(optimization_results['Results'][balance_in_optimization], balance_in_optimization, index_names[balance_in_optimization], model_instance.time_slice_yearly_weight)
        # copy balance and group by nosdes/connections and construction year
        # copy index_names and unit_time_summation
        results_temp[balance_in_optimization+'_grouped'],index_names[balance_in_optimization+'_grouped'] = \
            group_by_lower_index(optimization_results['Results'][balance_in_optimization],index_names[balance_in_optimization])
        optimization_results['Utilities']['Unit_time_summation'][balance_in_optimization +'_grouped'] = optimization_results['Utilities']['Unit_time_summation'][balance_in_optimization]
    # update optimization_results['Results']
    optimization_results['Results'].update(results_temp)

    # copy index_names and time slice yearly weights for export 
    optimization_results['Utilities']['Index_names']= index_names
    optimization_results['Utilities']['Misc']['time_slice_yearly_weights'] = model_instance.time_slice_yearly_weight
    ## get units
    # get unit of capacities
    for process_type in optimization_results['Utilities']['Parameters']['processes']:
        for process in optimization_results['Utilities']['Parameters']['processes'][process_type]:
            unit = [val for key, val in deunitized_input_dictionary['Units'][None]["potential_capacity"+process_type.replace('processes','')].items() if process in key][0]
            optimization_results['Utilities']['Unit_dict'][process] = unit
    # get unit of products
    for product in optimization_results['Utilities']['Parameters']['products']:
        unit = [val for key, val in deunitized_input_dictionary['Units'][None]["required_total_secured_capacity"].items() if product in key][0]
        optimization_results['Utilities']['Unit_dict'][product] = unit
    # get unit of impacts
    for impact in optimization_results['Utilities']['Parameters']['impact_categories']:
        unit = [val for key, val in deunitized_input_dictionary['Units'][None]["invest_impact_limits"].items() if impact in key][0]
        optimization_results['Utilities']['Unit_dict'][impact] = unit
    
    logging.info('Results simplified and unitized')
    # create Result folder
    if not os.path.exists(working_directory / "SecMOD" / "01-MODEL-RESULTS" / "Extracted Results"):
                os.mkdir(working_directory / "SecMOD" / "01-MODEL-RESULTS" / "Extracted Results")

    # Write selected optimization results to csv
    # List of results written to csvs set in config
    for i in config.results_list_to_csv:
        optimization_results['Results'][i].to_csv(str(working_directory / "SecMOD" / "01-MODEL-RESULTS" / "Extracted Results" /i) + ".csv")

    # save optimization results
    with open(working_directory / "SecMOD" / "01-MODEL-RESULTS" / "Extracted Results" / "Optimization_Results.pickle", 'wb') as output_file:
        pickle.dump(optimization_results, output_file)
        logging.info('Results saved')

    # save date log file
    with open(working_directory / "SecMOD" / "01-MODEL-RESULTS" / "Extracted Results" / "Date_Log_Raw_Results.pickle", 'wb') as output_file:
        pickle.dump(date_creation_result_file, output_file)

def get_list_of_index_names(balance_in_optimization):
# get index of 'years' in matrix 
    index_names= []
    for [idxcount,index_of_var] in enumerate(balance_in_optimization._index.set_tuple):
        if index_of_var.name == 'years':
            idxyear = idxcount
        else:
            # create list of index names with processes substituted by 'process', and 'construction_years_x' substituted by 'construction_years', in  order to identify level in multiindex
            if index_of_var.name in ['processes_production','processes_storage','processes_transmission','processes_transshipment']:
                index_names.append('process')
            elif 'construction_years' in index_of_var.name:
                index_names.append('construction_years')
            else:
                index_names.append(index_of_var.name)
    return [index_names, idxyear]   

def group_by_time_slice(balance_in_optimization,balance_name, index_name, time_slice_weight):
    """ group balance by time slices. Eliminates time slice index from dataframe """
    unit_time_summation = ''
    if 'time_slices' in index_name and len(index_name)>1:
        # unit of impacts already integrated and multiplied with time slice weights
        if 'impact' not in balance_name:
            # add time slice weight list
            list_time_slice_weight = []
            # get weights of time slices for row in effect
            for row in balance_in_optimization.index.get_level_values('time_slices'):
                list_time_slice_weight.append(time_slice_weight[row])
            # multiply each column with time slice weight
            balance_in_optimization = balance_in_optimization.apply(lambda column: column * list_time_slice_weight)
            # add unit_time_summation
            unit_time_summation = '*hour'
        index_name = [index_of_var for index_of_var in index_name if index_of_var != 'time_slices']
        balance_in_optimization = balance_in_optimization.groupby(index_name).sum() # sum over time slices
    return balance_in_optimization, unit_time_summation, index_name

def group_by_lower_index(balance_in_optimization,index_name):
    """ group balance by nodes/connections and construction years. Eliminates indices from dataframe """
    # sum over nodes, if 'nodes' in index
    if 'nodes' in index_name and len(index_name)>1:
        index_name = [index_of_var for index_of_var in index_name if index_of_var != 'nodes']
        balance_in_optimization = balance_in_optimization.groupby(index_name).sum()
    # sum over connections, if 'connections' in index
    if 'connections' in index_name and len(index_name)>1:
        index_name = [index_of_var for index_of_var in index_name if index_of_var != 'connections']
        balance_in_optimization = balance_in_optimization.groupby(index_name).sum()
    # sum over construction years, if 'construction_years' in index
    if 'construction_years' in index_name and len(index_name)>1:
        index_name = [index_of_var for index_of_var in index_name if index_of_var != 'construction_years']
        balance_in_optimization = balance_in_optimization.groupby(index_name).sum()
    return balance_in_optimization, index_name

def get_product_flow_production(model, year: int, multiindex_entry):
    """  returns the product flow for PRODUCTION at a node in a specific year and time slice equal to the sum of all process contributions to that product balance 
    identical structure as constraint_product_balance_rule in secmod.optimization"""
    product = multiindex_entry[0]
    process_production = multiindex_entry[1]
    node = multiindex_entry[2]
    time_slice = multiindex_entry[3]
    if config.optimization_mode == 'LP':
        return (
            # sum over all construction years to differentiate between the used capacities
            sum(model.technology_matrix_production[product, process_production, year_construction]
                *  model.used_production[node, process_production, year, year_construction, time_slice].value
                for year_construction in filter(lambda year_construction: (year_construction <= year) and (year - year_construction < model.lifetime_duration_production[process_production, year_construction]), model.construction_years_production))
            )
    elif config.optimization_mode == 'MILP':
        return (
            # sum over all construction years to differentiate between the used capacities
                sum(
                    # sum over all vertices and sections, respectively
                    sum(
                        model.technology_matrix_production[vertex, product, process_production, year_construction]
                        * model.xsi_production[node, process_production, year, year_construction, time_slice, vertex].value
                        for vertex in range(model.cornerpoints_number[process_production]-1))
                    + sum(
                        model.technology_matrix_production_gradient[vertex, product, process_production, year_construction]
                        * (model.used_production[node, process_production, year, year_construction, time_slice, vertex].value
                           - model.xsi_production[node, process_production, year, year_construction, time_slice, vertex].value
                           * model.processes_rel_partload_capacity[vertex, process_production, year_construction])
                        for vertex in range(model.cornerpoints_number[process_production]-1))
                    for year_construction in filter(lambda year_construction: (year_construction <= year) and (year - year_construction < model.lifetime_duration_production[process_production, year_construction]), model.construction_years_production))
            )
    
def get_product_flow_production_non_served_demand(model, year: int, multiindex_entry):
    """ returns the product flow for the non_served_demand at a node in a specific year and time slice equal to the sum of all process contributions to that product balance 
    identical structure as constraint_product_balance_rule in secmod.optimization"""
    product = multiindex_entry[0]
    node = multiindex_entry[1]
    time_slice = multiindex_entry[2]
    if config.optimization_mode == 'LP':
        return 0
    elif config.optimization_mode == 'MILP':
        return(
                model.non_served_demand[node, product, year, time_slice].value  
              )      
        
def get_product_flow_production_add_demand(model, year: int, multiindex_entry):
    """ returns the product flow for the additional demand (add_demand) at a node in a specific year and time slice equal to the sum of all process contributions to that product balance 
    identical structure as constraint_product_balance_rule in secmod.optimization"""
    product = multiindex_entry[0]
    node = multiindex_entry[1]
    time_slice = multiindex_entry[2]
    if config.optimization_mode == 'LP':
        return 0
    elif config.optimization_mode == 'MILP':
        return(
                model.add_demand[node, product, year, time_slice].value
              )     

def get_product_flow_storage(model, year: int, multiindex_entry):
    """  returns the product flow for STORAGE at a node in a specific year and time slice equal to the sum of all process contributions to that product balance 
    identical structure as constraint_product_balance_rule in secmod.optimization"""
    product = multiindex_entry[0]
    process_storage = multiindex_entry[1]
    node = multiindex_entry[2]
    time_slice = multiindex_entry[3]
    if product == model.storage_products[process_storage]:
        if config.optimization_mode == 'LP':
            return (
                    # sum over all construction years to differentiate between the used capacities
                    + sum(
                        sum(model.used_storage[node, process_storage, direction_storage, year, year_construction, time_slice].value
                            * (- model.storage_level_factor[direction_storage])
                            # sum over both storage direction (deposit and withdraw)
                            for direction_storage in model.directions_storage)
                        for year_construction in filter(lambda year_construction: (year_construction <= year) and (year - year_construction < model.lifetime_duration_storage[process_storage, year_construction]), model.construction_years_storage))
                   )
        elif config.optimization_mode == 'MILP':
            return (
                # sum over all construction years to differentiate between the used capacities
                + sum(
                    sum(model.used_storage[node, process_storage, direction_storage, year, year_construction, time_slice].value
                        * (- model.storage_level_factor[direction_storage])
                        # sum over both storage direction (deposit and withdraw)
                        for direction_storage in model.directions_storage)
                    for year_construction in filter(lambda year_construction: (year_construction <= year) and (year - year_construction < model.lifetime_duration_storage[process_storage, year_construction]), model.construction_years_storage))  
            )
    else:
        return 0


def get_product_flow_into_storage(model, year: int, multiindex_entry):
    """  returns the product flow for STORAGE at a node in a specific year and time slice equal to the sum of all process contributions to that product balance 
    identical structure as constraint_product_balance_rule in secmod.optimization"""
    product = multiindex_entry[0]
    process_storage = multiindex_entry[1]
    node = multiindex_entry[2]
    time_slice = multiindex_entry[3]
    if product == model.storage_products[process_storage]:
        if config.optimization_mode == 'LP':
            return (
                    # sum over all construction years to differentiate between the used capacities
                    # only consider deposit of stored product
                    + sum(
                        model.used_storage[node, process_storage, 'deposit', year, year_construction, time_slice].value
                        * model.storage_level_factor['deposit']
                        for year_construction in filter(lambda year_construction: (year_construction <= year) and (year - year_construction < model.lifetime_duration_storage[process_storage, year_construction]), model.construction_years_storage))
                   )
        elif config.optimization_mode == 'MILP':
            return (
                # sum over all construction years to differentiate between the used capacities
                # only consider deposit of stored product
                + sum(
                    model.used_storage[node, process_storage, 'deposit', year, year_construction, time_slice].value
                    * model.storage_level_factor['deposit']
                    for year_construction in filter(lambda year_construction: (year_construction <= year) and (year - year_construction < model.lifetime_duration_storage[process_storage, year_construction]), model.construction_years_storage))  
            )
    else:
        return 0


def get_product_flow_out_of_storage(model, year: int, multiindex_entry):
    """  returns the product flow for STORAGE at a node in a specific year and time slice equal to the sum of all process contributions to that product balance 
    identical structure as constraint_product_balance_rule in secmod.optimization"""
    product = multiindex_entry[0]
    process_storage = multiindex_entry[1]
    node = multiindex_entry[2]
    time_slice = multiindex_entry[3]
    if product == model.storage_products[process_storage]:
        if config.optimization_mode == 'LP':
            return (
                    # sum over all construction years to differentiate between the used capacities
                    # only consider withdrawal of stored product
                    + sum(
                        model.used_storage[node, process_storage, 'withdraw', year, year_construction, time_slice].value
                        * (-model.storage_level_factor['withdraw'])
                        for year_construction in filter(lambda year_construction: (year_construction <= year) and (year - year_construction < model.lifetime_duration_storage[process_storage, year_construction]), model.construction_years_storage))
                   )
        elif config.optimization_mode == 'MILP':
            return (
                # sum over all construction years to differentiate between the used capacities
                # only consider withdrawal of stored product
                + sum(
                    model.used_storage[node, process_storage, 'withdraw', year, year_construction, time_slice].value
                    * (-model.storage_level_factor['withdraw'])
                    for year_construction in filter(lambda year_construction: (year_construction <= year) and (year - year_construction < model.lifetime_duration_storage[process_storage, year_construction]), model.construction_years_storage))  
            )
    else:
        return 0


def get_product_flow_transshipment(model, year: int, multiindex_entry):      
    """  returns the product flow for TRANSSHIPMENT at a node in a specific year and time slice equal to the sum of all process contributions to that product balance 
    identical structure as constraint_product_balance_rule in secmod.optimization"""
    product = multiindex_entry[0]
    process_transshipment = multiindex_entry[1]
    node = multiindex_entry[2]
    time_slice = multiindex_entry[3]
    if product == model.transshipment_products[process_transshipment]:
        if config.optimization_mode == 'LP':
            return (
                # sum over all construction years to differentiate between the used capacities
                sum(
                    # Sum of all streams of connections which have this node as first node
                    sum(model.transshipment_efficiency[process_transshipment, connection]
                        * model.used_transshipment[connection, process_transshipment, "backward", year, year_construction, time_slice].value
                        - model.used_transshipment[connection, process_transshipment, "forward", year, year_construction, time_slice].value
                        for connection in filter(lambda connection: model.connected_nodes[connection, "node1"] == node, model.connections))
                    # Sum of all streams of connections, which have this node as second node, therefore multiplied with the efficiency of the connection
                    + sum(model.transshipment_efficiency[process_transshipment, connection]
                        * model.used_transshipment[connection, process_transshipment, "forward", year, year_construction, time_slice].value
                        - model.used_transshipment[connection, process_transshipment, "backward", year, year_construction, time_slice].value
                        for connection in filter(lambda connection: model.connected_nodes[connection, "node2"] == node, model.connections))
                    
                    for year_construction in filter(lambda year_construction: (year_construction <= year) and (year - year_construction < model.lifetime_duration_transshipment[process_transshipment, year_construction]), model.construction_years_transshipment))
                )
        elif config.optimization_mode == 'MILP':
            return(
                # sum over all construction years to differentiate between the used capacities
                sum(
                    # Sum of all streams of connections which have this node as first node
                    sum(model.transshipment_efficiency[process_transshipment, connection]
                        * model.used_transshipment[connection, process_transshipment, "backward", year, year_construction, time_slice].value
                        - model.used_transshipment[connection, process_transshipment, "forward", year, year_construction, time_slice].value
                        for connection in filter(lambda connection: model.connected_nodes[connection, "node1"] == node, model.connections))
                    # Sum of all streams of connections, which have this node as second node, therefore multiplied with the efficiency of the connection
                    + sum(model.transshipment_efficiency[process_transshipment, connection]
                        * model.used_transshipment[connection, process_transshipment, "forward", year, year_construction, time_slice].value
                        - model.used_transshipment[connection, process_transshipment, "backward", year, year_construction, time_slice].value
                        for connection in filter(lambda connection: model.connected_nodes[connection, "node2"] == node, model.connections))
                    
                    for year_construction in filter(lambda year_construction: (year_construction <= year) and (year - year_construction < model.lifetime_duration_transshipment[process_transshipment, year_construction]), model.construction_years_transshipment))
                )
    else:
        return 0

def get_product_flow_transmission(model, year: int, multiindex_entry):      
    """  returns the product flow for TRANSMISSION at a node in a specific year and time slice equal to the sum of all process contributions to that product balance 
    identical structure as constraint_product_balance_rule in secmod.optimization"""
    product = multiindex_entry[0]
    node = multiindex_entry[1]
    time_slice = multiindex_entry[2]
    if config.optimization_mode == 'LP':
        return (
            # adding the power from DC load flow transmission, if the product is electricity
            ( # Sum of the power from all connections which have this node as first node
                sum(
                    model.power_line_properties[connection, year, "susceptance per unit"]
                    * (model.phase_difference[model.connected_nodes[connection, "node2"], year, time_slice].value
                       - model.phase_difference[node, year, time_slice].value)
                    for connection in filter(lambda connection: (model.connected_nodes[connection, "node1"] == node) and (product in model.products_transmission), model.connections))
                # Sum of the power from all connections which have this node as second node
                + sum(
                    model.power_line_properties[connection, year, "susceptance per unit"]
                    * (model.phase_difference[model.connected_nodes[connection, "node1"], year, time_slice].value
                       - model.phase_difference[node, year, time_slice].value)
                    for connection in filter(lambda connection: (model.connected_nodes[connection, "node2"] == node) and (product in model.products_transmission), model.connections))
                ) * model.per_unit_base
            )
    elif config.optimization_mode == 'MILP':
        for connection in filter(lambda connection: (model.connected_nodes[connection, "node1"] == node) and (product in model.products_transmission), model.connections):    
            if model.phase_difference[node, year, time_slice].value == None:
                model.phase_difference[node, year, time_slice].value = 0
            if model.phase_difference[model.connected_nodes[connection, "node2"], year, time_slice].value == None: 
                model.phase_difference[model.connected_nodes[connection, "node2"], year, time_slice].value = 0
            return (
                # adding the power from DC load flow transmission, if the product is electricity
            + ( # Sum of the power from all connections which have this node as first node
                sum(
                    model.power_line_properties[connection, year, "susceptance per unit"]
                    * (model.phase_difference[model.connected_nodes[connection, "node2"], year, time_slice].value
                        - model.phase_difference[node, year, time_slice].value)
                    for connection in filter(lambda connection: (model.connected_nodes[connection, "node1"] == node) and (product in model.products_transmission), model.connections))
                # Sum of the power from all connections which have this node as second node
                + sum(
                    model.power_line_properties[connection, year, "susceptance per unit"]
                    * (model.phase_difference[model.connected_nodes[connection, "node1"], year, time_slice].value
                        - model.phase_difference[node, year, time_slice].value)
                    for connection in filter(lambda connection: (model.connected_nodes[connection, "node2"] == node) and (product in model.products_transmission), model.connections))
            )*model.per_unit_base
                )

def get_operational_impact_production(model, year: int, multiindex_entry):
    """  returns the oprerational impact for PRODUCTION at a node in a specific year and time slice equal to the sum of all process contributions to that impact 
    identical structure as constraint_operational_nodal_impact_rule in secmod.optimization"""
    impact_category = multiindex_entry[0]
    process_production = multiindex_entry[1]
    node = multiindex_entry[2]
    time_slice = multiindex_entry[3] 
    annuity_correction = 1
    if impact_category == 'cost':
        # if the impact category is cost, we divide by the annuity factor of the investment period:
        # In the optimization we annualize the total cost by determining cost factors in the impact matrix that
        # represent the full investment period (i.e., the present value  of the cost in the full
        # investment period, e.g., the present value of the cost for the investment period 2020-2025). This is necessary
        # for the rolling horizon optimization if we consider foresight with multiple investment periods of different
        # lengths.
        # see `classes.py`: `_get_cost_invest` and `_get_cost_operation` in `ProcessImpacts` class
        # However, in the results we want to know the cost per year (e.g., the total annualized cost in 2020).
        # Therefore, we need to divide the cost by the `investment_period_annuity_present_value_factor`.
        investment_period_duration = Process._get_investment_period_duration(year)
        annuity_correction =    ((1 + Process.interest_rate) ** (investment_period_duration / units.year) - 1) / \
                                ((1 + Process.interest_rate) ** (investment_period_duration / units.year) * Process.interest_rate)                         
    if config.optimization_mode == 'LP':
        return(
            # sum over the construction years of the production process
            sum(model.used_production[node, process_production, year, year_construction, time_slice].value
                * model.impact_matrix_production[impact_category, 'operation', process_production, year, year_construction]
                for year_construction in filter(lambda year_construction: (year_construction <= year) and (year - year_construction < model.lifetime_duration_production[process_production, year_construction]), model.construction_years_production))    
            # multiply the impact with the weight of the current time slice
            * model.time_slice_yearly_weight[time_slice] / annuity_correction
            )
    elif config.optimization_mode == 'MILP':
        return (
            # sum over the construction years of the production process
            sum(
            # sum over the vertices of each production process
                sum(
                    model.used_production[node, process_production, year, year_construction, time_slice, vertex].value
                    * model.impact_matrix_production[impact_category, 'operation', process_production, year, year_construction]
                    for vertex in range(model.cornerpoints_number[process_production]-1))
                for year_construction in filter(lambda year_construction: (year_construction <= year) and (year - year_construction < model.lifetime_duration_production[process_production, year_construction]), model.construction_years_production))
            # multiply the impact with the weight of the current time slice
            * model.time_slice_yearly_weight[time_slice] / annuity_correction
            )

def get_operational_impact_storage(model, year: int, multiindex_entry):
    """  returns the oprerational impact for STORAGE at a node in a specific year and time slice equal to the sum of all process contributions to that impact 
    identical structure as constraint_operational_nodal_impact_rule in secmod.optimization"""
    impact_category = multiindex_entry[0]
    process_storage = multiindex_entry[1]
    node = multiindex_entry[2]
    time_slice = multiindex_entry[3]   
    annuity_correction = 1
    if impact_category == 'cost':
        # if the impact category is cost, we divide by the annuity factor of the investment period:
        # In the optimization we annualize the total cost by determining cost factors in the impact matrix that
        # represent the full investment period (i.e., the present value  of the cost in the full
        # investment period, e.g., the present value of the cost for the investment period 2020-2025). This is necessary
        # for the rolling horizon optimization if we consider foresight with multiple investment periods of different
        # lengths.
        # see `classes.py`: `_get_cost_invest` and `_get_cost_operation` in `ProcessImpacts` class
        # However, in the results we want to know the cost per year (e.g., the total annualized cost in 2020).
        # Therefore, we need to divide the cost by the `investment_period_annuity_present_value_factor`.
        investment_period_duration = Process._get_investment_period_duration(year)
        annuity_correction =    ((1 + Process.interest_rate) ** (investment_period_duration / units.year) - 1) / \
                                ((1 + Process.interest_rate) ** (investment_period_duration / units.year) * Process.interest_rate)
    # sum over the construction years of the storage process
    return(
        sum(
        # impact is assumed to be the impact per withdrawn unit of stored product
        model.impact_matrix_storage[impact_category, 'operation', process_storage, year, year_construction]
        * model.used_storage[node, process_storage, "withdraw", year, year_construction, time_slice].value
        for year_construction in filter(lambda year_construction: (year_construction <= year) and (year - year_construction < model.lifetime_duration_storage[process_storage, year_construction]), model.construction_years_storage))
        # multiply the impact with the weight of the current time slice
        * model.time_slice_yearly_weight[time_slice] / annuity_correction)

def get_operational_impact_transshipment(model, year: int, multiindex_entry):
    """  returns the oprerational impact for TRANSSHIPMENT at a node in a specific year and time slice equal to the sum of all process contributions to that impact 
    identical structure as constraint_operational_nodal_impact_rule in secmod.optimization"""
    impact_category = multiindex_entry[0]
    process_transshipment = multiindex_entry[1]
    node = multiindex_entry[2]
    time_slice = multiindex_entry[3]
    annuity_correction = 1
    if impact_category == 'cost':
        # if the impact category is cost, we divide by the annuity factor of the investment period:
        # In the optimization we annualize the total cost by determining cost factors in the impact matrix that
        # represent the full investment period (i.e., the present value  of the cost in the full
        # investment period, e.g., the present value of the cost for the investment period 2020-2025). This is necessary
        # for the rolling horizon optimization if we consider foresight with multiple investment periods of different
        # lengths.
        # see `classes.py`: `_get_cost_invest` and `_get_cost_operation` in `ProcessImpacts` class
        # However, in the results we want to know the cost per year (e.g., the total annualized cost in 2020).
        # Therefore, we need to divide the cost by the `investment_period_annuity_present_value_factor`.
        investment_period_duration = Process._get_investment_period_duration(year)
        annuity_correction =    ((1 + Process.interest_rate) ** (investment_period_duration / units.year) - 1) / \
                                ((1 + Process.interest_rate) ** (investment_period_duration / units.year) * Process.interest_rate)
    return(
        # sum over the construction years of the transshipment process
        sum(
            # impact distributed equally (0.5:0.5) between the two connected nodes
            0.5 * model.impact_matrix_transshipment[impact_category, 'operation', process_transshipment, year, year_construction] 
            # sum over both transshipment directions
            * sum(
                # sum over all connections, which have this node as first node
                sum(model.used_transshipment[connection, process_transshipment, direction_transshipment, year, year_construction, time_slice].value
                    for connection in filter(lambda connection: model.connected_nodes[connection, "node1"] == node, model.connections))
                # sum over all connections, which have this node as second node
                + sum(model.used_transshipment[connection, process_transshipment, direction_transshipment, year, year_construction, time_slice].value
                    for connection in filter(lambda connection: model.connected_nodes[connection, "node2"] == node, model.connections))
                for direction_transshipment in model.directions_transshipment)
            for year_construction in filter(lambda year_construction: (year_construction <= year) and (year - year_construction < model.lifetime_duration_transshipment[process_transshipment, year_construction]), model.construction_years_transshipment))
        * model.time_slice_yearly_weight[time_slice] / annuity_correction)

def get_operational_impact_transmission(model, year: int, multiindex_entry):
    """  returns the oprerational impact for TRANSMISSION at a node in a specific year and time slice equal to the sum of all process contributions to that impact 
    identical structure as constraint_operational_nodal_impact_rule in secmod.optimization
    Assumed to be negligible
    """
    return 0

def get_operational_impact_non_served_demand(model, year: int, multiindex_entry):
    """  returns the oprerational impact for NON SERVED DEMAND at a node in a specific year and time slice equal to the sum of all process contributions to that impact 
    identical structure as constraint_operational_nodal_impact_rule in secmod.optimization
    """
    impact_category = multiindex_entry[0]
    product = multiindex_entry[1]
    node = multiindex_entry[2]
    time_slice = multiindex_entry[3]
    annuity_correction = 1
    if impact_category == 'cost':
        # if the impact category is cost, we divide by the annuity factor of the investment period:
        # In the optimization we annualize the total cost by determining cost factors in the impact matrix that
        # represent the full investment period (i.e., the present value  of the cost in the full
        # investment period, e.g., the present value of the cost for the investment period 2020-2025). This is necessary
        # for the rolling horizon optimization if we consider foresight with multiple investment periods of different
        # lengths.
        # see `classes.py`: `_get_cost_invest` and `_get_cost_operation` in `ProcessImpacts` class
        # However, in the results we want to know the cost per year (e.g., the total annualized cost in 2020).
        # Therefore, we need to divide the cost by the `investment_period_annuity_present_value_factor`.
        investment_period_duration = Process._get_investment_period_duration(year)
        annuity_correction =    ((1 + Process.interest_rate) ** (investment_period_duration / units.year) - 1) / \
                                ((1 + Process.interest_rate) ** (investment_period_duration / units.year) * Process.interest_rate)              
    if config.optimization_mode == 'LP':
            return(
                model.non_served_demand[node, product, year, time_slice].value
                * model.impact_matrix_non_served_demand[impact_category, product, year, time_slice]
                * model.time_slice_yearly_weight[time_slice] / annuity_correction)
    elif config.optimization_mode =='MILP':
            return(
                model.non_served_demand[node, product, year, time_slice].value
                * model.impact_matrix_non_served_demand[impact_category, product, year, time_slice]
                * model.time_slice_yearly_weight[time_slice] / annuity_correction)
        
def get_operational_impact_add_demand(model, year: int, multiindex_entry):
    """  returns the oprerational impact for ADD  DEMAND at a node in a specific year and time slice equal to the sum of all process contributions to that impact 
    identical structure as constraint_operational_nodal_impact_rule in secmod.optimization
    """
    impact_category = multiindex_entry[0]
    product = multiindex_entry[1]
    node = multiindex_entry[2]
    time_slice = multiindex_entry[3]
    annuity_correction = 1
    if impact_category == 'cost':
        # if the impact category is cost, we divide by the annuity factor of the investment period:
        # In the optimization we annualize the total cost by determining cost factors in the impact matrix that
        # represent the full investment period (i.e., the present value  of the cost in the full
        # investment period, e.g., the present value of the cost for the investment period 2020-2025). This is necessary
        # for the rolling horizon optimization if we consider foresight with multiple investment periods of different
        # lengths.
        # see `classes.py`: `_get_cost_invest` and `_get_cost_operation` in `ProcessImpacts` class
        # However, in the results we want to know the cost per year (e.g., the total annualized cost in 2020).
        # Therefore, we need to divide the cost by the `investment_period_annuity_present_value_factor`.
        investment_period_duration = Process._get_investment_period_duration(year)
        annuity_correction =    ((1 + Process.interest_rate) ** (investment_period_duration / units.year) - 1) / \
                                ((1 + Process.interest_rate) ** (investment_period_duration / units.year) * Process.interest_rate)                
    if config.optimization_mode == 'LP':
        return 0
    elif config.optimization_mode == 'MILP':
        return(
            model.add_demand[node, product, year, time_slice].value
            * model.impact_matrix_add_demand[impact_category, product, year, time_slice]
            * model.time_slice_yearly_weight[time_slice] / annuity_correction)
    
def get_invest_impact_production(model, year: int, multiindex_entry):
    """  returns the invest impact for PRODUCTION at a node in a specific year and time slice equal to the sum of all process contributions to that impact 
    identical structure as constraint_operational_nodal_impact_rule in secmod.optimization"""
    impact_category = multiindex_entry[0]
    process_production = multiindex_entry[1]
    node = multiindex_entry[2]
    time_slice = multiindex_entry[3]
    annuity_correction = 1
    if impact_category == 'cost':
        # if the impact category is cost, we divide by the annuity factor of the investment period:
        # In the optimization we annualize the total cost by determining cost factors in the impact matrix that
        # represent the full investment period (i.e., the present value  of the cost in the full
        # investment period, e.g., the present value of the cost for the investment period 2020-2025). This is necessary
        # for the rolling horizon optimization if we consider foresight with multiple investment periods of different
        # lengths.
        # see `classes.py`: `_get_cost_invest` and `_get_cost_operation` in `ProcessImpacts` class
        # However, in the results we want to know the cost per year (e.g., the total annualized cost in 2020).
        # Therefore, we need to divide the cost by the `investment_period_annuity_present_value_factor`.
        investment_period_duration = Process._get_investment_period_duration(year)
        annuity_correction =    ((1 + Process.interest_rate) ** (investment_period_duration / units.year) - 1) / \
                                ((1 + Process.interest_rate) ** (investment_period_duration / units.year) * Process.interest_rate)                        
    return(
        # EXISTING CAPACITY
        # sum over the construction years of the production process
        # only consider existing capacities if:
        # 1. year_construction is not in model.years --> if year in model.years then the impact is already considered in new_capacity_production
        # 2. year_construction >= years --> impact only counts if the capacity is already constructed
        # 3. year - year_construction < model.lifetime_duration_production[process_production, year_construction]) --> impact does not count after lifetime duration
        (sum(model.existing_capacity_production[node, process_production, year, year_construction]
            * model.impact_matrix_production[impact_category, 'invest', process_production, year, year_construction]
            for year_construction in filter(lambda year_construction: (year_construction not in model.years) and (year_construction <= year) and (year - year_construction < model.lifetime_duration_production[process_production, year_construction]), model.construction_years_production))    
        +
        # NEW CAPACITY
        # sum over the construction years of the production process
        sum(model.new_capacity_production[node, process_production, earlier_year].value
            * model.impact_matrix_production[impact_category, 'invest', process_production, year, earlier_year]
            for earlier_year in filter(lambda earlier_year: (earlier_year <= year) and (year - earlier_year < model.lifetime_duration_production[process_production, earlier_year]), model.years)))    
        * model.time_slice_yearly_weight[time_slice] / (model.total_time_slice_duration) / annuity_correction)

def get_invest_impact_storage(model, year: int, multiindex_entry):
    """  returns the invest impact for STORAGE at a node in a specific year and time slice equal to the sum of all process contributions to that impact 
    identical structure as constraint_operational_nodal_impact_rule in secmod.optimization"""
    impact_category = multiindex_entry[0]
    process_storage = multiindex_entry[1]
    node = multiindex_entry[2]
    time_slice = multiindex_entry[3]
    annuity_correction = 1
    if impact_category == 'cost':
        # if the impact category is cost, we divide by the annuity factor of the investment period:
        # In the optimization we annualize the total cost by determining cost factors in the impact matrix that
        # represent the full investment period (i.e., the present value  of the cost in the full
        # investment period, e.g., the present value of the cost for the investment period 2020-2025). This is necessary
        # for the rolling horizon optimization if we consider foresight with multiple investment periods of different
        # lengths.
        # see `classes.py`: `_get_cost_invest` and `_get_cost_operation` in `ProcessImpacts` class
        # However, in the results we want to know the cost per year (e.g., the total annualized cost in 2020).
        # Therefore, we need to divide the cost by the `investment_period_annuity_present_value_factor`.
        investment_period_duration = Process._get_investment_period_duration(year)
        annuity_correction =    ((1 + Process.interest_rate) ** (investment_period_duration / units.year) - 1) / \
                                ((1 + Process.interest_rate) ** (investment_period_duration / units.year) * Process.interest_rate)         
    return(
        # EXISTING CAPACITY
        # sum over the construction years of the storage process
        (sum(model.existing_capacity_storage[node, process_storage, year, year_construction]
            * model.impact_matrix_storage[impact_category, 'invest', process_storage, year, year_construction]
            for year_construction in filter(lambda year_construction: (year_construction not in model.years) and (year_construction <= year) and (year - year_construction < model.lifetime_duration_storage[process_storage, year_construction]), model.construction_years_storage))
        + 
        # NEW CAPACITY
        # sum over the construction years of the storage process
        sum(model.new_capacity_storage[node, process_storage, earlier_year].value
            * model.impact_matrix_storage[impact_category, 'invest', process_storage, year, earlier_year]
            for earlier_year in filter(lambda earlier_year: (earlier_year <= year) and (year - earlier_year < model.lifetime_duration_storage[process_storage, earlier_year]), model.years)))
        * model.time_slice_yearly_weight[time_slice] / (model.total_time_slice_duration) / annuity_correction)

def get_invest_impact_transshipment(model, year: int, multiindex_entry):
    """  returns the invest impact for TRANSSHIPMENT at a node in a specific year and time slice equal to the sum of all process contributions to that impact 
    identical structure as constraint_operational_nodal_impact_rule in secmod.optimization"""
    impact_category = multiindex_entry[0]
    process_transshipment = multiindex_entry[1]
    node = multiindex_entry[2]
    time_slice = multiindex_entry[3]
    annuity_correction = 1
    if impact_category == 'cost':
        # if the impact category is cost, we divide by the annuity factor of the investment period:
        # In the optimization we annualize the total cost by determining cost factors in the impact matrix that
        # represent the full investment period (i.e., the present value  of the cost in the full
        # investment period, e.g., the present value of the cost for the investment period 2020-2025). This is necessary
        # for the rolling horizon optimization if we consider foresight with multiple investment periods of different
        # lengths.
        # see `classes.py`: `_get_cost_invest` and `_get_cost_operation` in `ProcessImpacts` class
        # However, in the results we want to know the cost per year (e.g., the total annualized cost in 2020).
        # Therefore, we need to divide the cost by the `investment_period_annuity_present_value_factor`.
        investment_period_duration = Process._get_investment_period_duration(year)
        annuity_correction =    ((1 + Process.interest_rate) ** (investment_period_duration / units.year) - 1) / \
                                ((1 + Process.interest_rate) ** (investment_period_duration / units.year) * Process.interest_rate)                        
    return(
        # EXISTING CAPACITY
        # sum over the construction years of the transshipment process
        (sum(
            # impact distributed equally (0.5:0.5) between the two connected nodes
            0.5 * model.impact_matrix_transshipment[impact_category, 'invest', process_transshipment, year, year_construction] 
            # multiplied with the length and capacity of all connections of the node
            * (
                # sum over all connections, which have this node as first node
                sum(model.existing_capacity_transshipment[connection, process_transshipment, year, year_construction]
                    * model.connection_length[connection]
                    for connection in filter(lambda connection: model.connected_nodes[connection, "node1"] == node, model.connections))
                # sum over all connections, which have this node as second node
                + sum(model.existing_capacity_transshipment[connection, process_transshipment, year, year_construction]
                    * model.connection_length[connection]
                    for connection in filter(lambda connection: model.connected_nodes[connection, "node2"] == node, model.connections))
            )
            for year_construction in filter(lambda year_construction: (year_construction not in model.years) and (year_construction <= year) and (year - year_construction < model.lifetime_duration_transshipment[process_transshipment, year_construction]), model.construction_years_transshipment))
        +
        # NEW CAPACITY
        # sum over the construction years of the transshipment process
        sum(
            # impact distributed equally (0.5:0.5) between the two connected nodes
            0.5 * model.impact_matrix_transshipment[impact_category, 'invest', process_transshipment, year, earlier_year] 
            # multiplied with the length and capacity of all connections of the node
            * (
                # sum over all connections, which have this node as first node
                sum(model.new_capacity_transshipment[connection, process_transshipment, earlier_year].value
                    * model.connection_length[connection]
                    for connection in filter(lambda connection: model.connected_nodes[connection, "node1"] == node, model.connections))
                # sum over all connections, which have this node as second node
                + sum(model.new_capacity_transshipment[connection, process_transshipment, earlier_year].value
                    * model.connection_length[connection]
                    for connection in filter(lambda connection: model.connected_nodes[connection, "node2"] == node, model.connections))
            )
            for earlier_year in filter(lambda earlier_year: (earlier_year <= year) and (year - earlier_year < model.lifetime_duration_transshipment[process_transshipment, earlier_year]), model.years)))
        * model.time_slice_yearly_weight[time_slice] / (model.total_time_slice_duration) / annuity_correction)

def get_invest_impact_transmission(model, year: int, multiindex_entry):
    """  returns the invest impact for TRANSMISSION at a node in a specific year and time slice equal to the sum of all process contributions to that impact 
    identical structure as constraint_operational_nodal_impact_rule in secmod.optimization"""
    impact_category = multiindex_entry[0]
    process_transmission = multiindex_entry[1]
    node = multiindex_entry[2]
    time_slice = multiindex_entry[3]
    annuity_correction = 1
    if impact_category == 'cost':
        # if the impact category is cost, we divide by the annuity factor of the investment period:
        # In the optimization we annualize the total cost by determining cost factors in the impact matrix that
        # represent the full investment period (i.e., the present value  of the cost in the full
        # investment period, e.g., the present value of the cost for the investment period 2020-2025). This is necessary
        # for the rolling horizon optimization if we consider foresight with multiple investment periods of different
        # lengths.
        # see `classes.py`: `_get_cost_invest` and `_get_cost_operation` in `ProcessImpacts` class
        # However, in the results we want to know the cost per year (e.g., the total annualized cost in 2020).
        # Therefore, we need to divide the cost by the `investment_period_annuity_present_value_factor`.
        investment_period_duration = Process._get_investment_period_duration(year)
        annuity_correction =    ((1 + Process.interest_rate) ** (investment_period_duration / units.year) - 1) / \
                                ((1 + Process.interest_rate) ** (investment_period_duration / units.year) * Process.interest_rate)
    return(
        # EXISTING CAPACITY
        # sum over the construction years of the transmission process
        (sum(
            # impact distributed equally (0.5:0.5) between the two connected nodes
            0.5 * model.impact_matrix_transmission[impact_category, 'invest', process_transmission, year, year_construction] 
            # multiplied with the length and power capacity of all connections of the node
            * (
                # sum over all connections, which have this node as first node
                sum(model.existing_capacity_transmission[connection, process_transmission, year, year_construction]
                    * model.power_limit_per_circuit[process_transmission]
                    * model.connection_length[connection]
                    for connection in filter(lambda connection: model.connected_nodes[connection, "node1"] == node, model.connections))
                # sum over all connections, which have this node as second node
                + sum(model.existing_capacity_transmission[connection, process_transmission, year, year_construction]
                    * model.power_limit_per_circuit[process_transmission]
                    * model.connection_length[connection]
                    for connection in filter(lambda connection: model.connected_nodes[connection, "node2"] == node, model.connections))
            )
            for year_construction in filter(lambda year_construction: (year_construction not in model.years) and (year_construction <= year) and (year - year_construction < model.lifetime_duration_transmission[process_transmission, year_construction]), model.construction_years_transmission))
        +
        # NEW CAPACITY
        # sum over the construction years of the transmission process
        sum(
            # impact distributed equally (0.5:0.5) between the two connected nodes
            0.5 * model.impact_matrix_transmission[impact_category, 'invest', process_transmission, year, earlier_year] 
            # multiplied with the length and power capacity of all connections of the node
            * (
                # sum over all connections, which have this node as first node
                sum(model.new_capacity_transmission[connection, process_transmission, earlier_year].value
                    * model.power_limit_per_circuit[process_transmission]
                    * model.connection_length[connection]
                    for connection in filter(lambda connection: model.connected_nodes[connection, "node1"] == node, model.connections))
                # sum over all connections, which have this node as second node
                + sum(model.new_capacity_transmission[connection, process_transmission, earlier_year].value
                    * model.power_limit_per_circuit[process_transmission]
                    * model.connection_length[connection]
                    for connection in filter(lambda connection: model.connected_nodes[connection, "node2"] == node, model.connections))
            )
            for earlier_year in filter(lambda earlier_year: (earlier_year <= year) and (year - earlier_year < model.lifetime_duration_transmission[process_transmission, earlier_year]), model.years)))
        * model.time_slice_yearly_weight[time_slice] / (model.total_time_slice_duration) / annuity_correction)

def get_invest_impact_non_served_demand(model, year: int, multiindex_entry):
    """  returns the oprerational impact for NON SERVED DEMAND at a node in a specific year and time slice equal to the sum of all process contributions to that impact 
    identical structure as constraint_invest_nodal_impact_rule in secmod.optimization
    Obviously 0, since no invest necessary for non served demand
    """         
    return 0

def get_invest_impact_add_demand(model, year: int, multiindex_entry):
    """  returns the oprerational impact for ADD DEMAND at a node in a specific year and time slice equal to the sum of all process contributions to that impact 
    identical structure as constraint_invest_nodal_impact_rule in secmod.optimization
    Obviously 0, since no invest necessary for non served demand
    """         
    return 0

if __name__ == "__main__":
    log_format = '%(asctime)s %(filename)s: %(levelname)s: %(message)s'
    if not os.path.exists("logs"):
                os.mkdir("logs")
    logging.basicConfig(filename='logs/secmod.log', level=logging.INFO, format=log_format,
                        datefmt='%Y-%m-%d %H:%M:%S')
    logging.getLogger().addHandler(logging.StreamHandler())
    working_directory = Path().cwd()
    extract_results(working_directory, debug=False)

    logging.getLogger().handlers.clear()
    logging.shutdown()