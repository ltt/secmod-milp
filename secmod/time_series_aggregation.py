import pandas as pd
import logging
import numpy as np
import copy as copy
import tsam.timeseriesaggregation as tsam
from secmod.classes import config, Product, ProductionProcess
import secmod.helpers as helpers

def get_all_timeseries():
    """returns a dictionary of timeseries dataframes with the timeseries data provided for each product and process

    Returns:
	full_time_series (dict)

    """

    logging.info("Get all time series")
    full_time_series = {}
    full_time_series["demand"] = Product.get_combined_demand_time_series()
    full_time_series["impact_non_served_demand"] = Product.get_combined_impact_non_served_demand()
    full_time_series["technologymatrix"] = ProductionProcess.get_combined_technology_matrix_timeseries()
    full_time_series["availability"] = ProductionProcess.get_combined_availability_timeseries()
    
    if config.optimization_mode == 'MILP':
        full_time_series["impact_add_served_demand"] = Product.get_combined_impact_add_served_demand()

    for series in full_time_series:
        # convert date string in index
        full_time_series[series] = format_as_timeseries(full_time_series[series], series)

    return full_time_series

def format_as_timeseries(timeseries: pd.Series, series: str):
    """ Formats a parametrized time series obtained from classes as columns of individual time series.

    """

    # make sure that column has no identifier, transforming DataFrames to Series
    if isinstance(timeseries, pd.DataFrame):
        timeseries = timeseries[list(timeseries.columns)[0]]
    # Get the names of all indices of the provided series
    index_names = list(timeseries.index.names)

    # Remove the level "time slice" from the list of indices
    try:
        index_names.remove("time slice")
    except ValueError as e:
        print(e)
        raise ValueError("Index of the provided Series '{0}' does not include the level 'time slice'!".format(series))
    
    # Use the list of indices different from "time slice" to unstack all of them
    unstacked_timeseries = timeseries.unstack(index_names)

    # return the unstacked time series
    return unstacked_timeseries

def get_columns_for_all_timeseries(full_time_series: list):
    """ Return simple indices for all indices of all time series which are about to be joined. """

    simple_columns = {}
    num_of_columns = 0
    for series in full_time_series:
        columns = list(full_time_series[series].columns)
        simple_columns[series] = {}
        for column in columns:
            simple_columns[series][column] = num_of_columns
            num_of_columns += 1

    return simple_columns

def get_combined_timeseries_table(full_time_series: dict):
    """ Returns a dataframe with all timeseries as columns. """

    logging.info("Combine timeseries tables")
    # Get unique identifiers for every column of the combined time series
    column_identifiers = get_columns_for_all_timeseries(full_time_series)
    # Initialize a new dictionary to take in all used time series
    combined_timeseries = {}
    # initialize dictionary to save column multiindex
    column_index_timeseries = {}
    # For all time series in the input data...
    for timeseries in full_time_series:
        # Copy the dataframe in order to not manipulate the input data
        combined_timeseries[timeseries] = full_time_series[timeseries].copy(deep=True)
        # Overwrite the columns index with the unique integer column identifiers in order to have a simple joinable column index
        combined_timeseries[timeseries].columns = [column_identifiers[timeseries][index] for index in column_identifiers[timeseries]]
        # save column multiindex for later reconstruction of index
        column_index_timeseries[timeseries] = full_time_series[timeseries].columns
    list_of_timeseries = [combined_timeseries[index] for index in combined_timeseries]
    combined_timeseries_data = list_of_timeseries.pop(0)
    combined_timeseries_data = combined_timeseries_data.join(list_of_timeseries)

    combined_timeseries = {}
    combined_timeseries["timeseries"] = combined_timeseries_data
    combined_timeseries["column_identifiers"] = column_identifiers 
    combined_timeseries["column_index_timeseries"] = column_index_timeseries

    return combined_timeseries

def deunitize_timeseries(timeseries: pd.DataFrame):
    """ Determines a unit for every time series and splits unit and magnitude.

    This method takes in a DataFrame with columns of time series and determines
    a specific unit for every time series by determining the unit of a value 
    of the time series. Afterwards, every value in the time series is converted
    to the same unit to ensure correct scaling of the values of the time series.
    Finally, the units for every column are stored separately from the magnitudes
    and both are returned in a dictionary.

    """

    logging.info("Deunitize time series data")
    # Initialize dictionary for split units and magnitudes of time series
    split_timeseries = {}

    # Get units for every column
    split_timeseries["units"]       = get_unit_of_columns(timeseries)
    # Get copy of the handed timeseries to prohibit manipulation of the original object
    split_timeseries["timeseries"]  = timeseries.copy(deep=True)
    # Transform every value of a column to the determined unit of the 
    for column in split_timeseries["timeseries"]:
        split_timeseries["timeseries"][column] = split_timeseries["timeseries"][column].apply(lambda value: value.to(split_timeseries["units"][column]).magnitude if (split_timeseries["units"][column] != 1) else value)
        
    return split_timeseries

def unitize_timeseries(timeseries: pd.DataFrame, unit_dict: dict):
    """ Adds the previously determined target unit of each column to every value of the aggregated time series. """

    logging.info("Unitize time series")
    unitized_timeseries = timeseries.copy(deep=True)

    for column in unitized_timeseries.columns:
        unitized_timeseries[column] = unitized_timeseries[column].apply(lambda row: row * unit_dict[column])

    return unitized_timeseries

def transform_timeseries_value(value, target_unit):
    """ Takes quantities and non-quantities and transforms them to the respective target unit. 
    
    If it is a quantity which can be transformed to the target unit, it is simply transformed.
    However, if the value is no quantity, but merely a number an error will be raised,
    if the number is not 0.
    """

    try:
        transformed_value = value.to(target_unit).magnitude
    except AttributeError as e:
        print(e)
        if value == 0:
            transformed_value = 0
        else:
            raise ValueError("""The provided value '{0}' is neither a quantity nor zero. Therefore, it can't be \\
                converted to the target unit '{1}'""".format(value, target_unit))
    
    return transformed_value

def get_unit_of_columns(timeseries: pd.DataFrame):
    """ Determines the unit of a combined time series and returns it. """

    # Initialize dictionary for units
    column_unit = {}

    # Determine for every column a single unit
    for column in timeseries.columns:
        # Set unit to 1 in case no element of the column is a quantity
        column_unit[column] = 1
        # by going through all elements of a columns
        for element in timeseries[column]:
            # trying to get the unit of the value, if the value is a quantity
            try:
                column_unit[column] = element.units
                # break the loop as soon as an element with a unit is found in the column
                break
            # If it's no quantity set unit to 1
            except AttributeError:
                pass
    
    # return dictionary of units
    return column_unit

def separate_timeseries(combined_timeseries: pd.DataFrame, column_identifiers: dict, column_index: dict, time_slices: dict):
    """ Method to separate a combined time series DataFrame into multiple DataFrames.

    The parameter column_identifiers contains a dictionary of dictionaries for every
    targeted DataFrame. These dictionaries contain the information about all columns
    of the combined time series which belong to the targeted DataFrame.
    """

    logging.info("Separate combined time series")
    # Create an empty dictionary for the separated time series
    separated_timeseries = {}
    combined_timeseries.index = time_slices["time_slice"]
    combined_timeseries.index.set_names("time_slice",inplace = True)
    for element in column_identifiers:
        separated_timeseries[element] = pd.DataFrame()
        for index in column_identifiers[element]:
            separated_timeseries[element][index] = combined_timeseries[column_identifiers[element][index]]
        separated_timeseries[element].columns = column_index[element]
        
    return separated_timeseries

def reorder_timeseries(aggregated_time_series: dict):
    """ Reorder indizes of timeseries in order to match format in generate_input_dictionary (data_processing) """
    for timeseries in aggregated_time_series:
        # get names of columns
        column_names = list(aggregated_time_series[timeseries].columns.names)
        desired_order = copy.deepcopy(column_names)
        if timeseries != 'demand': # if not demand, append 'time_slice'
            desired_order.append('time_slice')
        else: # if demand
            desired_order.insert(2, 'time_slice')
        # stack and reorder
        aggregated_time_series[timeseries] = aggregated_time_series[timeseries].stack(column_names).reorder_levels(desired_order)

    return aggregated_time_series
def get_all_weights():
    """ Get all weights for the time series aggregation
        Returns: None; Right now implemented in get_time_slices
    """

    return None

def get_time_slices(aggregation):
    """ return time slices of aggregation. Corresponding to format of time slices without time series aggregation """

    aggregated_time_slices = {"time_slice": [], "next_time_slice": [], "time_slice_duration":[],"time_slice_yearly_weights": []}
    # create array of time slices
    for cluster in aggregation.clusterPeriodIdx:

        # list of time slices for each cluster
        if not aggregation.segmentation:
            time_slices = list(range(1+cluster*int(aggregation.hoursPerPeriod/aggregation.resolution),
                                     1+int(aggregation.hoursPerPeriod/aggregation.resolution)*(1+cluster)))
            aggregated_time_slices["time_slice"].extend(time_slices)
            # shift index of time slices to get next time slice
            time_slices.append(time_slices.pop(0))
            aggregated_time_slices["next_time_slice"].extend(time_slices)
            
            # get duration of time slice
            # this duration is supposed to be the "real" duration within a cluster
            aggregated_time_slices["time_slice_duration"].extend(list(np.ones(int(aggregation.hoursPerPeriod))*aggregation.resolution))
            
            # get weight of time slice
            # how many hours of the year are represented by this time_slice
            aggregated_time_slices["time_slice_yearly_weights"].extend(list(np.ones(aggregation.hoursPerPeriod)*\
                                                                                  aggregation.clusterPeriodNoOccur[cluster]*\
                                                                                  aggregation.resolution))

        else:
            time_slices = list(range(1+cluster*aggregation.noSegments,1+aggregation.noSegments*(1+cluster)))
            aggregated_time_slices["time_slice"].extend(time_slices)
            # shift index of time slices to get next time slice
            time_slices.append(time_slices.pop(0))
            aggregated_time_slices["next_time_slice"].extend(time_slices)
            # get duration of time slice
            segment_durations = []
            for segment in range(aggregation.noSegments):
                segment_durations.append(aggregation.segmentDurationDict['Segment Duration'][cluster,segment])
            aggregated_time_slices["time_slice_duration"].extend(segment_durations)
            # get weight of time slice --> not yet fully implemented
            aggregated_time_slices["time_slice_yearly_weights"].extend(list(aggregation.clusterPeriodNoOccur[cluster]*\
                                                                      np.array(segment_durations)))

    # create dicts
    aggregated_time_slices["next_time_slice"] = dict(zip(aggregated_time_slices["time_slice"],aggregated_time_slices["next_time_slice"]))
    aggregated_time_slices["time_slice_duration"] = dict(zip(aggregated_time_slices["time_slice"],aggregated_time_slices["time_slice_duration"]))
    aggregated_time_slices["time_slice_yearly_weights"] = dict(zip(aggregated_time_slices["time_slice"],aggregated_time_slices["time_slice_yearly_weights"]))

    return aggregated_time_slices

def get_aggregated_time_series(deunitized_timeseries, combined_timeseries, aggregation):
    """ Returns all aggregated time_series. """

    # Create typical periods
    aggregated_time_series = aggregation.createTypicalPeriods()
    
    # get time slices
    aggregated_time_slices = get_time_slices(aggregation)
       
    # Unitize the aggregated time series again
    aggregated_time_series = unitize_timeseries(aggregated_time_series, deunitized_timeseries["units"])
    # Create the individual time series DataFrames
    aggregated_time_series = separate_timeseries(aggregated_time_series, combined_timeseries["column_identifiers"],combined_timeseries["column_index_timeseries"],aggregated_time_slices)
    # Reorder time series
    aggregated_time_series = reorder_timeseries(aggregated_time_series)

    return aggregated_time_series, aggregated_time_slices

def get_add_Peak_max(column_identifiers):
    """Returns the column number of the time_series you want to add the peak_demand
    to your aggregation. """
    
    peak_demand_columns = None
    
    # check if specified
    if config.time_series_aggregation_config["addPeakMax"] != None:
        peak_demand_columns = list()
        for dict_key in config.time_series_aggregation_config["addPeakMax"].keys():
            for max_key in config.time_series_aggregation_config["addPeakMax"][dict_key]:
                for key, value in column_identifiers[dict_key].items():
                    if max_key in key:
                        peak_demand_columns.append(value)
                    
    return peak_demand_columns

def get_time_series_data():
    """ Return the aggregated time series data """

    helpers.log_heading("Time Series Aggregation")
    aggregated_time_series = {}

    # Get all time series
    aggregated_time_series["full_time_series"] = get_all_timeseries()
    # Combine time series to a single dataframe
    aggregated_time_series["combined_timeseries"] = get_combined_timeseries_table(aggregated_time_series["full_time_series"])
    # Get weight of every single time series
    weights = get_all_weights()

    # Split units of all timeseries
    aggregated_time_series["deunitized_timeseries"] = deunitize_timeseries(aggregated_time_series["combined_timeseries"]["timeseries"])

    # get peak demands
    peakDemand_2add2_timeSeries = get_add_Peak_max(aggregated_time_series["combined_timeseries"]["column_identifiers"])
                        
    logging.info("Aggregate time series")
    # Create aggregation object
    
### SEGMENTATION + SEGMENT
    aggregated_time_series["aggregation"] = tsam.TimeSeriesAggregation(
        timeSeries                  = aggregated_time_series["deunitized_timeseries"]["timeseries"],
        resolution                  = config.time_series_aggregation_config["resolution"],
        noTypicalPeriods            = config.time_series_aggregation_config["noTypicalPeriods"],
        hoursPerPeriod              = config.time_series_aggregation_config["hoursPerPeriod"],
        segmentation                = config.time_series_aggregation_config["segmentation"],
        noSegments                  = config.time_series_aggregation_config["noSegments"],
        clusterMethod               = config.time_series_aggregation_config["clusterMethod"],
        evalSumPeriods              = config.time_series_aggregation_config["evalSumPeriods"],
        sortValues                  = config.time_series_aggregation_config["sortValues"],
        sameMean                    = config.time_series_aggregation_config["sameMean"],
        rescaleClusterPeriods       = config.time_series_aggregation_config["rescaleClusterPeriods"],
        weightDict                  = weights,
        extremePeriodMethod         = config.time_series_aggregation_config["extremePeriodMethod"],
        predefClusterOrder          = config.time_series_aggregation_config["predefClusterOrder"],
        predefClusterCenterIndices  = config.time_series_aggregation_config["predefClusterCenterIndices"],
        solver                      = config.time_series_aggregation_config["solver_tsa"],
        roundOutput                 = config.time_series_aggregation_config["roundOutput"],
        addPeakMin                  = config.time_series_aggregation_config["addPeakMin"],
        addPeakMax                  = peakDemand_2add2_timeSeries, #config.time_series_aggregation_config["addPeakMax"],
        addMeanMin                  = config.time_series_aggregation_config["addMeanMin"],
        addMeanMax                  = config.time_series_aggregation_config["addMeanMax"]
        )

    aggregated_time_series["aggregated_time_series"],aggregated_time_series["aggregated_time_slices"] = \
        get_aggregated_time_series(aggregated_time_series["deunitized_timeseries"], 
                                   aggregated_time_series["combined_timeseries"], 
                                   aggregated_time_series["aggregation"])

    return aggregated_time_series
